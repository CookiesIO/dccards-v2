var browserify = require('browserify'),
	babelify = require('babelify'),
	source = require('vinyl-source-stream'),
	buffer = require('vinyl-buffer'),
	connect = require('gulp-connect-php'),
	browserSync = require('browser-sync'),
	open = require('gulp-open'),
	gulp = require('gulp'),
	gulpif = require('gulp-if'),
	uglify = require('gulp-uglify'),
	minifyCss = require('gulp-minify-css'),
	runSequence = require('run-sequence');

process.env.NODE_ENV = process.env.NODE_ENV ? process.env.NODE_ENV.trim() : 'development';
var production = process.env.NODE_ENV === 'production';

// performs magic
gulp.task('build-js', function() {
	return browserify('src/js/app.js', {
		paths: ['./src/js/lib'],
	})
		.transform(babelify, {optional: ['runtime', 'es7.objectRestSpread']})
	.bundle()
		.pipe(source('app.js'))
		.pipe(buffer())
		.pipe(gulpif(production, uglify()))
		.pipe(gulp.dest('dist/public_html/assets/js'));
});

// moves source files to dist
gulp.task('copy-public', function() {
	return gulp.src('src/public/**/*.*')
		.pipe(gulp.dest('dist/public_html'));
});

gulp.task('copy-css', function() {
	return gulp.src('src/css/**/*.css')
		.pipe(gulpif(production, minifyCss()))
		.pipe(gulp.dest('dist/public_html/assets/css'));
})
 
// local development server
gulp.task('connect', function(){
	connect.server({
		base: './dist/public_html',
		port: '8010',
		keepalive: true
	});
});


gulp.task('browser', ['connect'], function() {
	browserSync.init({
		proxy: '127.0.0.1:8010',
		port: 8080,
		open: true,
		notify: false
	});
});

gulp.task('reload', function() {
	browserSync.reload();
});

// build the application
gulp.task('build-scripts', ['copy-css', 'build-js']);
gulp.task('build', ['copy-public', 'build-scripts']);
gulp.task('default', ['build', 'browser']);
	
// watch for source changes
gulp.task('watch', ['default'], function() {
	gulp.watch('src/public/**/*.*', function() {
		runSequence('copy-public', 'reload');
	});
	gulp.watch('src/css/**/*.*', function() {
		runSequence('copy-css', 'reload');
	});
	gulp.watch('src/js/**/*.*', function() {
		runSequence('build-js', 'reload');
	});
	gulp.watch('dist/laravel/**/*.*', ['reload']);
});