<?php

use Illuminate\Database\Seeder;
use App\Type;

class TypeTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Type::truncate();

		Type::create(['name' => 'Average']);
		Type::create(['name' => 'Fresh']);
		Type::create(['name' => 'Mental']);
		Type::create(['name' => 'Strong']);
		Type::create(['name' => 'Tough']);
		Type::create(['name' => 'Quick']);
		Type::create(['name' => 'Smart']);
		Type::create(['name' => 'Perfect']);
	}
}
