<?php

use Illuminate\Database\Seeder;
use App\Shape;

class ShapeTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Shape::truncate();

		Shape::create(['name' => 'Unknown']);
		Shape::create(['name' => 'None']);
		Shape::create(['name' => 'Chick']);
		Shape::create(['name' => 'Beast']);
		Shape::create(['name' => 'Torso']);
		Shape::create(['name' => 'Bird']);
		Shape::create(['name' => 'Male']);
		Shape::create(['name' => 'Female']);
		Shape::create(['name' => 'Mutant']);
		Shape::create(['name' => 'Giant']);
		Shape::create(['name' => 'Female (Bunny)']);
		Shape::create(['name' => 'Male (Suit)']);
		Shape::create(['name' => 'Male (Pilot)']);
		Shape::create(['name' => 'Female (Cheerleader)']);
		Shape::create(['name' => 'Male (Batter)']);
		Shape::create(['name' => 'Male (Prisoner)']);
		Shape::create(['name' => 'Easter Egg']);
		Shape::create(['name' => 'Female (Beastling)']);
		Shape::create(['name' => 'Male (Beastling)']);
	}
}
