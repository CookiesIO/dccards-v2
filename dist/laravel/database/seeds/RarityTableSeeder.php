<?php

use Illuminate\Database\Seeder;
use App\Rarity;

class RarityTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Rarity::truncate();
		
		Rarity::create([
			'name' => 'Common',
			'max_level' => 40,
			'redeathable' => false,
			'redeath_max_level' => null
		]);
		Rarity::create([
			'name' => 'Uncommon',
			'max_level' => 45,
			'redeathable' => false,
			'redeath_max_level' => null
		]);
		Rarity::create([
			'name' => 'Rare',
			'max_level' => 50,
			'redeathable' => false,
			'redeath_max_level' => null
		]);
		Rarity::create([
			'name' => 'Epic',
			'max_level' => 55,
			'redeathable' => true,
			'redeath_max_level' => 60
		]);
		Rarity::create([
			'name' => 'Epic Plus',
			'max_level' => 55,
			'redeathable' => true,
			'redeath_max_level' => 60
		]);
		Rarity::create([
			'name' => 'Legendary',
			'max_level' => 60,
			'redeathable' => true,
			'redeath_max_level' => 70
		]);
		Rarity::create([
			'name' => 'Virulent',
			'max_level' => 60,
			'redeathable' => true,
			'redeath_max_level' => 70
		]);
		Rarity::create([
			'name' => 'Pestilent',
			'max_level' => 60,
			'redeathable' => true,
			'redeath_max_level' => 70
		]);
	}
}
