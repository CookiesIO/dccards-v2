<?php

use Illuminate\Database\Seeder;
use App\Ability;
use App\Effect;

class AbilityTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Effect::truncate();
		Ability::truncate();

		$abilities = json_decode(file_get_contents('http://api.dccards.io/GetAllAbilities'));
		$abilities = $abilities->info;
		$effects = [];
		foreach ($abilities as $ability) {
			if (isset($effects[$ability->effect])) {
				$effect = $effects[$ability->effect];
			}
			else {
				$effect = new Effect;
				$effect->effect = $ability->effect;
				$effect->cost = $ability->psyche_cost;
				$effect->cost_stat = 'psyche';
				$effect->event = 'unknown';
				$effect->priority = 0;
				$effect->save();
				$effects[$ability->effect] = $effect;
			}

			$newAbility = new Ability;
			$newAbility->name = $ability->name;
			$newAbility->effect()->associate($effect);
			$newAbility->save();
		}
	}
}
