<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('TypeTableSeeder');
		$this->call('StrainTableSeeder');
		$this->call('RarityTableSeeder');
		$this->call('SpawnAreaTableSeeder');
		$this->call('ShapeTableSeeder');
		$this->call('AbilityTableSeeder');
		$this->call('CardTableSeeder');
	}

}
