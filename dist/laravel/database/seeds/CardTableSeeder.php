<?php

use Illuminate\Database\Seeder;
use App\Card;
use App\Stats;
use App\Build;
use App\Skillset;
use App\Type;
use App\Shape;
use App\Strain;
use App\Rarity;
use App\SpawnArea;
use App\Ability;

class CardTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Build::truncate();
		Stats::truncate();
		Skillset::truncate();
		Card::truncate();
		DB::table('card_type')->truncate();

		DB::transaction(function() {
			$cards = json_decode(file_get_contents('http://api.dccards.io/GetAllCards'));
			$cards = $cards->info;

			$types = Type::all()->keyBy('name');
			$shapes = Shape::all()->keyBy('name');
			$strains = Strain::all()->keyBy('name');
			$rarities = Rarity::all()->keyBy('name');
			$spawnAreas = SpawnArea::all()->keyBy('name');
			$abilities = Ability::all()->keyBy('name');
			
			foreach ($cards as $card) {
				$strain = $strains->get($card->Strain);
				$rarity = $rarities->get($card->Rarity); 
				$spawnArea = $spawnAreas->get($card->SpawnArea);
				$shape = $shapes->get($card->Shape);

				// Card
				$newCard = new Card;

				$newCard->strain()->associate($strain);
				$newCard->rarity()->associate($rarity);
				if (isset($shape)) {
					$newCard->shape()->associate($shape);
				}
				if (isset($spawnArea)) {
					$newCard->spawnArea()->associate($spawnArea);
				}

				$newCard->name = $card->Name;
				$newCard->description = $card->Description;
				$newCard->catalog = $card->Catalog;
				$newCard->card_number = $card->CardNumber;
				$newCard->max_level = $card->MaxLevel;
				$newCard->redeathable = $rarity->redeathable && $card->MaxLevel == $rarity->max_level;
				$newCard->tradable = true;
				$newCard->extinct = false;

				$newCard->save();

				if ($card->AverageOnly) {
					$newCard->types()->attach($types->get('Average')->id);
				}
				else {
					foreach ($types as $type) {
						$newCard->types()->attach($type->id);
					}
				}

				// Stats
				$stats = new Stats;

				$stats->card()->associate($newCard);

				$stats->health_min = $card->AverageStats->Health->Min;
				$stats->health_max = $card->AverageStats->Health->Max;
				$stats->psyche_min = $card->AverageStats->Psyche->Min;
				$stats->psyche_max = $card->AverageStats->Psyche->Max;
				$stats->attack_min = $card->AverageStats->Attack->Min;
				$stats->attack_max = $card->AverageStats->Attack->Max;
				$stats->defense_min = $card->AverageStats->Defense->Min;
				$stats->defense_max = $card->AverageStats->Defense->Max;
				$stats->speed_min = $card->AverageStats->Speed->Min;
				$stats->speed_max = $card->AverageStats->Speed->Max;
				$stats->intelligence_min = $card->AverageStats->Intelligence->Min;
				$stats->intelligence_max = $card->AverageStats->Intelligence->Max;

				$stats->save();

				// Skillset
				$skillset = new Skillset;

				$skillset->card()->associate($newCard);

				$this->associateAbility($skillset->level01Ability(), $abilities->get($card->Abilities->Ability1));
				$this->associateAbility($skillset->level15Ability(), $abilities->get($card->Abilities->Ability2));
				$this->associateAbility($skillset->level30Ability(), $abilities->get($card->Abilities->Ability3));
				$this->associateAbility($skillset->level40Ability(), $abilities->get($card->Abilities->Ability4));
				$this->associateAbility($skillset->level50Ability(), $abilities->get($card->Abilities->Ability5));
				$this->associateAbility($skillset->redeathAbility(), $abilities->get($card->Abilities->Ability6));

				$skillset->save();

				// Normal Build
				$normalBuild = new Build;

				$normalBuild->redeathed = false;
				$normalBuild->card()->associate($newCard);
				$normalBuild->type()->associate($types->get('Average'));
				$this->associateAbility($normalBuild->ability1(), $abilities->get($card->RecommendedAbilities[0]));
				$this->associateAbility($normalBuild->ability2(), $abilities->get($card->RecommendedAbilities[1]));
				$this->associateAbility($normalBuild->ability3(), $abilities->get($card->RecommendedAbilities[2]));

				$normalBuild->save();

				// Redeath Build
				if ($rarity->redeathable)
				{
					$redeathBuild = new Build;

					$redeathBuild->redeathed = true;
					$redeathBuild->card()->associate($newCard);
					$redeathBuild->type()->associate($types->get('Average'));
					$this->associateAbility($redeathBuild->ability1(), $abilities->get($card->RecommendedRedeathAbilities[0]));
					$this->associateAbility($redeathBuild->ability2(), $abilities->get($card->RecommendedRedeathAbilities[1]));
					$this->associateAbility($redeathBuild->ability3(), $abilities->get($card->RecommendedRedeathAbilities[2]));

					$redeathBuild->save();
				}
			}
		});
	}

	function associateAbility($relation, $ability)
	{
		if (isset($ability)) {
			$relation->associate($ability);
		}
	}
}
