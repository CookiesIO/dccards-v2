<?php

use Illuminate\Database\Seeder;
use App\Strain;

class StrainTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Strain::truncate();

		Strain::create(['name' => 'Voider']);
		Strain::create(['name' => 'Burner']);
		Strain::create(['name' => 'Chiller']);
		Strain::create(['name' => 'Slasher']);
		Strain::create(['name' => 'Shocker']);
		Strain::create(['name' => 'Screamer']);
		Strain::create(['name' => 'Spitter']);
		Strain::create(['name' => 'Charmer']);
		Strain::create(['name' => 'Leecher']);
	}
}
