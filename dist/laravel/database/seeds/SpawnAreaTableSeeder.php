<?php

use Illuminate\Database\Seeder;
use App\SpawnArea;

class SpawnAreaTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		SpawnArea::truncate();

		SpawnArea::create(['name' => 'Gould Square']);
		SpawnArea::create(['name' => 'Clayton Cemetary']);
		SpawnArea::create(['name' => 'Tumbleweed Casino']);
		SpawnArea::create(['name' => 'USS Claypool']);
		SpawnArea::create(['name' => 'Raccoon City']);
		SpawnArea::create(['name' => 'Boneyard']);
		SpawnArea::create(['name' => 'Clan']);
		SpawnArea::create(['name' => 'Lottery']);
		SpawnArea::create(['name' => 'Login Bonus']);
		SpawnArea::create(['name' => 'Job Reward']);
		SpawnArea::create(['name' => '1st Invite-a-Friend']);
		SpawnArea::create(['name' => '2nd Invite-a-Friend']);
		SpawnArea::create(['name' => 'ZenoTek Stadium']);
		SpawnArea::create(['name' => 'Halloween Event']);
		SpawnArea::create(['name' => 'Morris Penitentiary']);
		SpawnArea::create(['name' => 'Christmas Event']);
		SpawnArea::create(['name' => 'Gould Square (alt)']);
		SpawnArea::create(['name' => 'Valentine\'s Event']);
		SpawnArea::create(['name' => 'Deserted Ghost Town']);
		SpawnArea::create(['name' => 'Easter Event']);
		SpawnArea::create(['name' => 'Beastling Event']);
		SpawnArea::create(['name' => 'Gould Square (Dusk)']);
	}
}
