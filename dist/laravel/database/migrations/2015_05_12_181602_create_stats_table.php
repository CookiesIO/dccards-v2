<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('stats', function(Blueprint $table)
		{
			$table->integer('card_id')->unsigned()->primary();
			$table->integer('health_min');
			$table->integer('health_max');
			$table->integer('psyche_min');
			$table->integer('psyche_max');
			$table->integer('attack_min');
			$table->integer('attack_max');
			$table->integer('defense_min');
			$table->integer('defense_max');
			$table->integer('speed_min');
			$table->integer('speed_max');
			$table->integer('intelligence_min');
			$table->integer('intelligence_max');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('stats');
	}

}
