<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cards', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('name');
			$table->string('description')->nullable();
			$table->string('catalog')->nullable();
			$table->string('card_number')->nullable();

			$table->integer('strain_id')->unsigned();
			$table->integer('rarity_id')->unsigned();
			$table->integer('spawn_area_id')->nullable()->unsigned();
			$table->integer('shape_id')->nullable()->unsigned();
			$table->integer('max_level');

			$table->boolean('redeathable');
			$table->boolean('tradable');
			$table->boolean('extinct');
			$table->boolean('locked')->default(false);

			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cards');
	}

}
