<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Many-to-many relations
		Schema::create('card_type', function(Blueprint $table)
		{
			$table->integer('card_id')->unsigned();
			$table->integer('type_id')->unsigned();

			$table->primary(['card_id', 'type_id']);
			$table->foreign('card_id')->references('id')->on('cards');
			$table->foreign('type_id')->references('id')->on('types');
		});

		// One-to-one/one-to-many relations
		Schema::table('cards', function(Blueprint $table)
		{
			$table->foreign('shape_id')->references('id')->on('shapes');
			$table->foreign('strain_id')->references('id')->on('strains');
			$table->foreign('rarity_id')->references('id')->on('rarities');
			$table->foreign('spawn_area_id')->references('id')->on('spawn_areas');
		});

		Schema::table('abilities', function(Blueprint $table)
		{
			$table->foreign('effect_id')->references('id')->on('effects');
		});

		Schema::table('stats', function(Blueprint $table)
		{
			$table->foreign('card_id')->references('id')->on('cards');
		});

		Schema::table('skillsets', function(Blueprint $table)
		{
			$table->foreign('card_id')->references('id')->on('cards');
			$table->foreign('level01_ability_id')->references('id')->on('abilities');
			$table->foreign('level15_ability_id')->references('id')->on('abilities');
			$table->foreign('level30_ability_id')->references('id')->on('abilities');
			$table->foreign('level40_ability_id')->references('id')->on('abilities');
			$table->foreign('level50_ability_id')->references('id')->on('abilities');
			$table->foreign('redeath_ability_id')->references('id')->on('abilities');
		});

		Schema::table('builds', function(Blueprint $table)
		{
			$table->foreign('card_id')->references('id')->on('cards');
			$table->foreign('type_id')->references('id')->on('types');
			$table->foreign('ability1_id')->references('id')->on('abilities');
			$table->foreign('ability2_id')->references('id')->on('abilities');
			$table->foreign('ability3_id')->references('id')->on('abilities');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Many-to-many relations
		Schema::drop('card_type');

		// One-to-one/One-to-many relations
		Schema::table('cards', function(Blueprint $table)
		{
			$table->dropForeign('cards_shape_id_foreign');
			$table->dropForeign('cards_strain_id_foreign');
			$table->dropForeign('cards_rarity_id_foreign');
			$table->dropForeign('cards_spawn_area_id_foreign');
		});

		Schema::table('abilities', function(Blueprint $table)
		{
			$table->dropForeign('abilities_effect_id_foreign');
		});

		Schema::table('stats', function(Blueprint $table)
		{
			$table->dropForeign('stats_card_id_foreign');
		});

		Schema::table('skillsets', function(Blueprint $table)
		{
			$table->dropForeign('skillsets_card_id_foreign');
		});
		
		Schema::table('builds', function(Blueprint $table)
		{
			$table->dropForeign('builds_card_id_foreign');
			$table->dropForeign('builds_type_id_foreign');
			$table->dropForeign('builds_ability1_id_foreign');
			$table->dropForeign('builds_ability2_id_foreign');
			$table->dropForeign('builds_ability3_id_foreign');
		});
	}
}
