<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuildsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('builds', function(Blueprint $table)
		{
			$table->increments('id');
			$table->boolean('redeathed');
			$table->integer('card_id')->unsigned();
			$table->integer('type_id')->unsigned();
			$table->integer('ability1_id')->nullable()->unsigned();
			$table->integer('ability2_id')->nullable()->unsigned();
			$table->integer('ability3_id')->nullable()->unsigned();

			$table->unique(['redeathed', 'card_id', 'type_id']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('builds');
	}

}
