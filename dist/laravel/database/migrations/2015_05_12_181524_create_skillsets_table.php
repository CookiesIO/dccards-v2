<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkillsetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('skillsets', function(Blueprint $table)
		{
			$table->integer('card_id')->unsigned()->primary();
			$table->integer('level01_ability_id')->nullable()->unsigned();
			$table->integer('level15_ability_id')->nullable()->unsigned();
			$table->integer('level30_ability_id')->nullable()->unsigned();
			$table->integer('level40_ability_id')->nullable()->unsigned();
			$table->integer('level50_ability_id')->nullable()->unsigned();
			$table->integer('redeath_ability_id')->nullable()->unsigned();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('skillsets');
	}

}
