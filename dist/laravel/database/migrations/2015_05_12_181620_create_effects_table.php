<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEffectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('effects', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('effect')->unique();
			$table->string('event');
			$table->integer('cost');
			$table->string('cost_stat');
			$table->integer('priority');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('effects');
	}

}
