<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\ScopeByNameTrait;

class Card extends Model {
	use SoftDeletes;
	use ScopeByNameTrait;
	protected $dates = ['deleted_at'];

	protected $casts = [
		'id' => 'integer',
		'max_level' => 'integer',
		'redeathable' => 'boolean',
		'tradable' => 'boolean',
		'extinct' => 'boolean',
		'locked' => 'boolean'
	];

	public function stats()
	{
		return $this->hasOne('App\Stats');
	}

	public function skillset()
	{
		return $this->hasOne('App\Skillset');
	}

	public function builds()
	{
		return $this->hasMany('App\Build');
	}

	public function types()
	{
		return $this->belongsToMany('App\Type');
	}

	public function shape()
	{
		return $this->belongsTo('App\Shape');
	}

	public function strain()
	{
		return $this->belongsTo('App\Strain');
	}

	public function rarity()
	{
		return $this->belongsTo('App\Rarity');
	}

	public function spawnArea()
	{
		return $this->belongsTo('App\SpawnArea');
	}

	public function toArray()
	{
		$builds = $this->builds;
		if (count($builds) > 0) {
			$normal = [];
			$redeath = [];

			foreach ($builds as $build) {
				$typeName = strtolower($build->type->name);
				if ($build->redeathed) {
					$redeath[$typeName] = $build;
				}
				else {
					$normal[$typeName] = $build;
				}
			}

			if (count($redeath) == 0) {
				$redeath = null;
			}
			if (count($normal) == 0) {
				$normal = null;
			}
			else {
				$normal['redeath'] = $redeath;
			}
			$builds = $normal;
		}
		else {
			$builds = null;
		}

		return [
			'id' => $this->id,
			'locked' => $this->locked,

			'name' => $this->name,
			'description' => $this->description,
			'catalog' => $this->catalog,
			'cardNumber' => $this->card_number,

			'strain' => $this->strain->name,
			'rarity' => $this->rarity->name,
			'spawnArea' => isset($this->spawnArea) ? $this->spawnArea->name : null,
			'shape' => isset($this->shape) ? $this->shape->name : null,

			'maxLevel' => $this->max_level,
			'redeathable' => $this->redeathable,
			'extinct' => $this->extinct,
			'tradable' => $this->tradable,

			'types' => $this->types->map(function($type) { return $type->name; }),
			'stats' => $this->stats,
			'skillset' => $this->skillset,
			'builds' => $builds
		];
	}
}
