<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Effect extends Model {
	public $timestamps = false;
	protected $casts = [
		'id' => 'integer',
		'cost' => 'integer',
		'priority' => 'integer'
	];

	public function abilities()
	{
		return $this->hasMany('App\Ability');
	}
}
