<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Stats extends Model {
	public $timestamps = false;

	protected $casts = [
		'card_id' => 'integer',
		'health_min' => 'integer',
		'health_max' => 'integer',
		'psyche_min' => 'integer',
		'psyche_max' => 'integer',
		'attack_min' => 'integer',
		'attack_max' => 'integer',
		'defense_min' => 'integer',
		'defense_max' => 'integer',
		'speed_min' => 'integer',
		'speed_max' => 'integer',
		'intelligence_min' => 'integer',
		'intelligence_max' => 'integer'
	];

	public function card()
	{
		return $this->belongsTo('App\Card');
	}

	public function toArray()
	{
		return [
			'healthMin' => $this->health_min,
			'healthMax' => $this->health_max,
			'psycheMin' => $this->psyche_min,
			'psycheMax' => $this->psyche_max,
			'attackMin' => $this->attack_min,
			'attackMax' => $this->attack_max,
			'defenseMin' => $this->defense_min,
			'defenseMax' => $this->defense_max,
			'speedMin' => $this->speed_min,
			'speedMax' => $this->speed_max,
			'intelligenceMin' => $this->intelligence_min,
			'intelligenceMax' => $this->intelligence_max
		];
	}
}
