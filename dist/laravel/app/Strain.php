<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ScopeByNameTrait;

class Strain extends Model {
	use ScopeByNameTrait;
	public $timestamps = false;

	protected $casts = [
		'id' => 'integer'
	];

	public function cards()
	{
		return $this->hasMany('App\Card');
	}
}
