<?php namespace App\Http\Controllers;

class DCCardsController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	}

	/**
	 * Show the application screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('dccards');
	}

}
