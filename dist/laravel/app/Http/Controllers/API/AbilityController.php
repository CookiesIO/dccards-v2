<?php namespace App\Http\Controllers\API;

use Cache;
use App\Http\Controllers\Controller;
use App\Ability;

class AbilityController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	}

	/**
	 * Show the application screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return Cache::remember('abilities', 1440, function() {
			return Ability::with('effect')->get();
		});
	}

	public function show($name)
	{
		return Ability::byName($name)->first();
	}
}
