<?php namespace App\Http\Controllers\API;

use App\Http\Requests\StoreCardRequest;

use Cache;
use App\Http\Controllers\Controller;
use App\Card;
use App\Stats;
use App\Build;
use App\Skillset;
use App\Type;
use App\Shape;
use App\Strain;
use App\Rarity;
use App\SpawnArea;
use App\Ability;

class CardController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	}

	/**
	 * Show the application screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		// Remember cards for a day
		return Cache::remember('cards', 1440, function() {
			return Card::with(
				'types',
				'stats',
				'shape',
				'strain',
				'rarity',
				'spawnArea',
				'builds.type',
				'builds.ability1',
				'builds.ability2',
				'builds.ability3',
				'skillset.level01Ability',
				'skillset.level15Ability',
				'skillset.level30Ability',
				'skillset.level40Ability',
				'skillset.level50Ability',
				'skillset.redeathAbility'
			)->get();
		});
	}

	public function show($name)
	{
		return Card::byName($name)->first();
	}

	public function store(StoreCardRequest $request)
	{
		//$tellsUsOur = env('API_SECRET');
		//$secret = $request->input('secret');
		//if ($tellsUsOur == $secret) {
			Cache::forget('cards');

			$card = $request->input('card');

			$strain = Strain::byName($card['strain'])->firstOrFail();
			$rarity = Rarity::byName($card['rarity'])->firstOrFail();
			if (isset($card['spawnArea'])) {
				$spawnArea = SpawnArea::byName($card['spawnArea'])->first();
			}
			if (isset($card['shape'])) {
				$shape = Shape::byName($card['shape'])->first();
			}

			// Card
			$newCard = new Card;

			$newCard->strain()->associate($strain);
			$newCard->rarity()->associate($rarity);
			if (isset($shape)) {
				$newCard->shape()->associate($shape);
			}
			if (isset($spawnArea)) {
				$newCard->spawnArea()->associate($spawnArea);
			}

			$newCard->name = $card['name'];
			$newCard->description = isset($card['description']) ? $card['description'] : null;
			$newCard->catalog =     isset($card['catalog'])     ? $card['catalog'] : null;
			$newCard->card_number = isset($card['cardNumber'])  ? $card['cardNumber'] : null;
			$newCard->max_level =   isset($card['maxLevel'])    ? $card['maxLevel'] : $rarity->max_level;
			$newCard->redeathable = isset($card['redeathable']) ? $card['redeathable'] : $rarity->redeathable;
			$newCard->tradable =    isset($card['tradable'])    ? $card['tradable'] : true;
			$newCard->extinct =     isset($card['extinct'])     ? $card['extinct'] : false;
			$newCard->locked = 	    isset($card['locked'])      ? $card['locked'] : false;

			$newCard->save();

			$types = Type::in($card['types'])->get();
			if ($types->count() > 0) {
				$newCard->types()->attach(
					$types->map(function($type) {
						return $type->id;
					})->all()
				);
			}

			// Stats
			if (isset($card['stats'])) {
				$stats = new Stats;

				$stats->card()->associate($newCard);

				$stats->health_min = $card['stats']['healthMin'];
				$stats->health_max = $card['stats']['healthMax'];
				$stats->psyche_min = $card['stats']['psycheMin'];
				$stats->psyche_max = $card['stats']['psycheMax'];
				$stats->attack_min = $card['stats']['attackMin'];
				$stats->attack_max = $card['stats']['attackMax'];
				$stats->defense_min = $card['stats']['defenseMin'];
				$stats->defense_max = $card['stats']['defenseMax'];
				$stats->speed_min = $card['stats']['speedMin'];
				$stats->speed_max = $card['stats']['speedMax'];
				$stats->intelligence_min = $card['stats']['intelligenceMin'];
				$stats->intelligence_max = $card['stats']['intelligenceMax'];

				$stats->save();
			}


			// Skillset
			if (isset($card['skillset'])) {
				$skillset = new Skillset;

				$skillset->card()->associate($newCard);

				$this->associateSkillsetAbility($skillset->level01Ability(), $card['skillset'], 'level01');
				$this->associateSkillsetAbility($skillset->level15Ability(), $card['skillset'], 'level15');
				$this->associateSkillsetAbility($skillset->level30Ability(), $card['skillset'], 'level30');
				$this->associateSkillsetAbility($skillset->level40Ability(), $card['skillset'], 'level40');
				$this->associateSkillsetAbility($skillset->level50Ability(), $card['skillset'], 'level50');
				$this->associateSkillsetAbility($skillset->redeathAbility(), $card['skillset'], 'redeath');

				$skillset->save();
			}

			if (isset($card['builds'])) {
				$this->createBuilds($newCard, $card['builds'], false);
				if (isset($card['builds']['redeath'])) {
					$this->createBuilds($newCard, $card['builds']['redeath'], true);
				}
			}

			return response()->json('{"success": true}');
		//}
		//return response()->json('{"secret"="'.$secret.'"}', 401);
		//abort(401, $secret);
	}
		
	// Associate skillset ability helper
	function associateSkillsetAbility($relation, $skillset, $ability)
	{
		if (isset($skillset[$ability])) {
			$ability = Ability::byName($skillset[$ability])->first();
			$this->associateAbility($relation, $ability);
		}
	}

	function createBuilds($card, $builds, $redeathed)
	{
		if (isset($builds['average'])) {
			$this->createBuild($card, Type::byName('Average')->firstOrFail(), $builds['average'], $redeathed);
		}
		if (isset($builds['fresh'])) {
			$this->createBuild($card, Type::byName('Fresh')->firstOrFail(), $builds['fresh'], $redeathed);
		}
		if (isset($builds['mental'])) {
			$this->createBuild($card, Type::byName('Mental')->firstOrFail(), $builds['mental'], $redeathed);
		}
		if (isset($builds['strong'])) {
			$this->createBuild($card, Type::byName('Strong')->firstOrFail(), $builds['strong'], $redeathed);
		}
		if (isset($builds['tough'])) {
			$this->createBuild($card, Type::byName('Tough')->firstOrFail(), $builds['tough'], $redeathed);
		}
		if (isset($builds['quick'])) {
			$this->createBuild($card, Type::byName('Quick')->firstOrFail(), $builds['quick'], $redeathed);
		}
		if (isset($builds['smart'])) {
			$this->createBuild($card, Type::byName('Smart')->firstOrFail(), $builds['smart'], $redeathed);
		}
		if (isset($builds['perfect'])) {
			$this->createBuild($card, Type::byName('Perfect')->firstOrFail(), $builds['perfect'], $redeathed);
		}
	}
		
	// Create build helper
	function createBuild($card, $type, $build, $redeathed)
	{
		$newBuild = new Build;

		$newBuild->redeathed = $redeathed;
		$newBuild->card()->associate($card);
		$newBuild->type()->associate($type);
		if (isset($build[0])) {
			$this->associateAbility($newBuild->ability1(), Ability::byName($build[0])->first());
		}
		if (isset($build[1])) {
			$this->associateAbility($newBuild->ability2(), Ability::byName($build[1])->first());
		}
		if (isset($build[2])) {
			$this->associateAbility($newBuild->ability3(), Ability::byName($build[2])->first());
		}

		$newBuild->save();
	}
		
	// Associate ability helper
	function associateAbility($relation, $ability)
	{
		if (isset($ability)) {
			$relation->associate($ability);
		}
	}
}
