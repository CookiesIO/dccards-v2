<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreCardRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		if ($this->has('secret')) 
		{
			$tellsUsOur = env('API_SECRET');
			$secret = $this->input('secret');
			return $tellsUsOur === $secret;
		}
		return false;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$rules = [
			'card' => 'required',
			'card.strain' => 'required|exists:strains,name',
			'card.rarity' => 'required|exists:rarities,name',
			'card.spawnArea' => 'sometimes|exists:spawn_areas,name',
			'card.shape' => 'sometimes|exists:shapes,name',

			'card.name' => 'required|string|unique:cards,name',
			'card.description' => 'sometimes|string',
			'card.catalog' => 'sometimes|string',
			'card.cardNumber' => 'sometimes|string',

			'card.maxLevel' => 'sometimes|integer',

			'card.redeathable' => 'sometimes|boolean',
			'card.tradable' => 'sometimes|boolean',
			'card.extinct' => 'sometimes|boolean',
			'card.locked' => 'sometimes|boolean',

			'card.stats.healthMin' => 'required_with:card.stats|integer',
			'card.stats.healthMax' => 'required_with:card.stats|integer',
			'card.stats.psycheMin' => 'required_with:card.stats|integer',
			'card.stats.psycheMax' => 'required_with:card.stats|integer',
			'card.stats.attackMin' => 'required_with:card.stats|integer',
			'card.stats.attackMax' => 'required_with:card.stats|integer',
			'card.stats.defenseMin' => 'required_with:card.stats|integer',
			'card.stats.defenseMax' => 'required_with:card.stats|integer',
			'card.stats.speedMin' => 'required_with:card.stats|integer',
			'card.stats.speedMax' => 'required_with:card.stats|integer',
			'card.stats.intelligenceMin' => 'required_with:card.stats|integer',
			'card.stats.intelligenceMax' => 'required_with:card.stats|integer',

			'card.skillset.level01' => 'sometimes|string|exists:abilities,name',
			'card.skillset.level15' => 'sometimes|string|exists:abilities,name',
			'card.skillset.level30' => 'sometimes|string|exists:abilities,name',
			'card.skillset.level40' => 'sometimes|string|exists:abilities,name',
			'card.skillset.level50' => 'sometimes|string|exists:abilities,name',
			'card.skillset.redeath' => 'sometimes|string|exists:abilities,name',
		];

		foreach ($this->input('card.types') as $key => $value) {
			$rule = 'required|string|exists:types,name';
			foreach ($this->input('card.types') as $key2 => $value2) {
				if ($key !== $key2) {
					$rule .= '|different:card.types.'.$key2;
				}
			}
			$rules['card.types.'.$key] = $rule;
		}

		if ($this->has('card.builds')) {
			$this->storeBuildRules($rules, 'average');
			$this->storeBuildRules($rules, 'fresh');
			$this->storeBuildRules($rules, 'mental');
			$this->storeBuildRules($rules, 'strong');
			$this->storeBuildRules($rules, 'tough');
			$this->storeBuildRules($rules, 'quick');
			$this->storeBuildRules($rules, 'smart');
			$this->storeBuildRules($rules, 'perfect');
			if ($this->has('card.builds.redeath')) {
				$this->storeBuildRules($rules, 'redeath.average');
				$this->storeBuildRules($rules, 'redeath.fresh');
				$this->storeBuildRules($rules, 'redeath.mental');
				$this->storeBuildRules($rules, 'redeath.strong');
				$this->storeBuildRules($rules, 'redeath.tough');
				$this->storeBuildRules($rules, 'redeath.quick');
				$this->storeBuildRules($rules, 'redeath.smart');
				$this->storeBuildRules($rules, 'redeath.perfect');
			}
		}
		return $rules;
	}

	private function storeBuildRules(&$rules, $type)
	{
		if ($this->has('card.builds.'.$type)) {
			foreach ($this->input('card.builds.'.$type) as $key => $value) {
				$rule = 'sometimes|string|exists:abilities,name';
				$rules['card.builds.'.$type.'.'.$key] = $rule;
			}
		}
	}
}
