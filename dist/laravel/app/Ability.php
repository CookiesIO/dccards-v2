<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ScopeByNameTrait;

class Ability extends Model {
	use ScopeByNameTrait;
	public $timestamps = false;

	protected $casts = [
		'id' => 'integer'
	];

	public function effect()
	{
		return $this->belongsTo('App\Effect');
	}

	public function toArray()
	{
		$effect = $this->effect;
		return [
			'name' => $this->name,
			'effect' => $effect->effect,
			'costStat' => $effect->cost_stat,
			'cost' => $effect->cost,
			'event' => $effect->event,
			'priority' => $effect->priority,
		];
	}
}
