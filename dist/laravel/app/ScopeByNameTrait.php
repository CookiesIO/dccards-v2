<?php namespace App;

trait ScopeByNameTrait {
	public function scopeByName($query, $name)
	{
		return $query->whereName($name);
	}
}