<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Build extends Model {
	public $timestamps = false;

	protected $casts = [
		'id' => 'integer',
		'redeathed' => 'boolean'	
	];

	public function card()
	{
		return $this->belongsTo('App\Card');
	}

	public function type()
	{
		return $this->belongsTo('App\Type');
	}

	public function ability1()
	{
		return $this->belongsTo('App\Ability', 'ability1_id');
	}

	public function ability2()
	{
		return $this->belongsTo('App\Ability', 'ability2_id');
	}

	public function ability3()
	{
		return $this->belongsTo('App\Ability', 'ability3_id');
	}

	public function toArray()
	{
		return [
			$this->getAbilityName($this->ability1),
			$this->getAbilityName($this->ability2),
			$this->getAbilityName($this->ability3)
		];
	}

	function getAbilityName($ability) {
		if (isset($ability)) {
			return $ability->name;
		}
		return null;
	}
}
