<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Skillset extends Model {
	public $timestamps = false;

	protected $casts = [
		'card_id' => 'integer'
	];

	public function card()
	{
		return $this->belongsTo('App\Card');
	}

	public function level01Ability()
	{
		return $this->belongsTo('App\Ability', 'level01_ability_id');
	}

	public function level15Ability()
	{
		return $this->belongsTo('App\Ability', 'level15_ability_id');
	}

	public function level30Ability()
	{
		return $this->belongsTo('App\Ability', 'level30_ability_id');
	}

	public function level40Ability()
	{
		return $this->belongsTo('App\Ability', 'level40_ability_id');
	}

	public function level50Ability()
	{
		return $this->belongsTo('App\Ability', 'level50_ability_id');
	}

	public function redeathAbility()
	{
		return $this->belongsTo('App\Ability', 'redeath_ability_id');
	}

	public function toArray()
	{
		return [
			'level01' => $this->getAbilityName($this->level01Ability),
			'level15' => $this->getAbilityName($this->level15Ability),
			'level30' => $this->getAbilityName($this->level30Ability),
			'level40' => $this->getAbilityName($this->level40Ability),
			'level50' => $this->getAbilityName($this->level50Ability),
			'redeath' => $this->getAbilityName($this->redeathAbility)
		];
	}

	function getAbilityName($ability) {
		if (isset($ability)) {
			return $ability->name;
		}
		return null;
	}
}
