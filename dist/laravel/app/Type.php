<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ScopeByNameTrait;

class Type extends Model {
	use ScopeByNameTrait;
	public $timestamps = false;

	protected $casts = [
		'id' => 'integer'
	];

	public function scopeIn($query, $types)
	{
		return $query->whereIn('name', $types);
	}
}
