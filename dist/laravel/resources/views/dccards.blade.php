<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>Deadmans Cross Cards</title>
	<link rel="shortcut icon" href="/assets/images/favicon.ico">
	<link rel="stylesheet" type="text/css" href="/assets/css/app.css">
	<link rel="stylesheet" type="text/css" href="{{ cached_asset('/assets/css/bootstrap_extensions.css') }}">
	<script type="text/javascript" src="{{ cached_asset('/assets/js/app.js') }}" defer></script>
</head>
<body>
	<div class="text-center">
		<h1><span class="glyphicon glyphicon-refresh glyphicon-spinning"></span> Loading DCCards, please wait...</h1>
	</div>
</body>
</html>