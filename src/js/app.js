import React from "react/addons"
import Router, {Route, DefaultRoute, Redirect} from "react-router"
import {App, Home, Calculator, CalculatorCard, CalculatorHome/*, Wiki, Contribute, About*/} from "ui/views"
import DCCards from "dccards"


import "calculators"


DCCards.load();

var routes = (
	<Route name="app" path="/" handler={App}>
		<Route name="home" handler={Home} />
		<Route name="calculator" handler={Calculator}>
			<Route name="calculatorCard" path=":cardName" handler={CalculatorCard}>
			{/*
				<Route name="calculatorCardRedeath" path="Redeath"/>
				<Route name="calculatorCardType" path=":typeName">
					<Route name="calculatorCardTypeRedeath" path="Redeath"/>
				</Route>
			*/}
			</Route>
			<Redirect from=":cardName/" to="calculatorCard" />
			<DefaultRoute handler={CalculatorHome} />
		</Route>
		<Redirect from="calculator/" to="calculator" />
		{/*
		<Route name="wiki" handler={Wiki}>
			<Route name="wikiCard" path="card/:cardName" handler={Wiki.Card}/>
			<Route name="wikiAbility" path="ability/:abilityName" handler={Wiki.Ability}/>
			<DefaultRoute handler={Wiki.Home}/>
		</Route>
		<Route name="contribute" handler={Contribute}/>
		<Route name="about" handler={About}/>
		*/}
		<Redirect from="/" to="home" />
	</Route>
);

Router.run(routes, Router.HistoryLocation, (Handler, state) => {
	React.render(<Handler/>, document.body);
});
