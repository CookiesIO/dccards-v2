import Promise from "bluebird"

export default class Calculator {
	constructor(name) {
		this.name = name;
		this.cardData = null;
		this.calculationPromise = null;
	}

	getName() {
		return this.name;
	}

	getOptions() {
		throw new Error(`Implement getOptions please: ${this}`);
	}

	setOption(section, option, value) {
		throw new Error(`Implement setOption please: ${this}`);
	}

	setCard(card, type, redeathed, level, boosts, build) {
		this.cardData = {
			card, type, redeathed, level, boosts, build
		};
	}

	calculate(opponents) {
		if (this.calculationPromise !== null) {
			this.calculationPromise.cancel();
		}

		var state = {
			cancelled: false,
			state: this.setupCalculationState()
		};

		return state.promise = this.calculationPromise = new Promise((resolve, reject) => {
			var calculationGenerator = this.internalCalculate(opponents, state);
			var step = () => {
				return Promise.delay(0).then(() => {
					if (state.cancelled === true) {
						reject();
						return;
					}
					if (calculationGenerator.next().done) {
						var presentation = this.presentCalculation(state.state);
						if (state.promise === this.calculationPromise) {
							this.calculationPromise = null;	
						}
						resolve(presentation);
						return;
					}
					return step();
				});
			}
			step();
		})
		.cancellable()
		.caught(Promise.CancellationError, (e) => {
			// Only clear if another promise didn't override the internal promise
			if (this.calculationPromise === state.promise) {
				this.calculationPromise = null;
			}
			state.cancelled = true;
			throw e;
		});
	}

	*internalCalculate(opponents, state) {
		var step = 1;
		var index = 0;

		while (true) {
			if (state.cancelled === true) {
				return;
			}

			var end = index + step;
			if (end > opponents.length) {
				end = opponents.length;
			}

			this.calculateCards(opponents.slice(index, end), state.state);      

			index = end;
			if (index === opponents.length) {
				return;
			}
			yield;
		}
	}

	setupCalculationState() {
		throw new Error(`Implement setupCalculationState please: ${this}`);
	}

	calculateCards(cards, state) {
		throw new Error(`Implement calculateCards please: ${this}`);
	}

	presentCalculation(state) {
		throw new Error(`Implement presentCalculation please: ${this}`);
	}
}