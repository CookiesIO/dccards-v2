import Calculator from "calculators/Calculator"

import Options from "simulation/Options"
import Simulation from "simulation/Simulation"
import AbilityStore from "stores/dccards/AbilityStore"
import CalculatorActions from "actions/calculator"

class OHKOCalculator extends Calculator {
	constructor() {
		super("OHKO Calculator");

		this.cardData = null;

		this.calculatorOptions = new Options();
		this.opponentOptions = new Options();
		this.attackerOptions = new Options();
		this.defenderOptions = new Options();

		this.cardEntryOption = this.calculatorOptions.add({
			type: "Select",
			name: "entry",
			display: "Card entry",
			tooltip: "Choose which card enters the battle, Attacker is this card and Defender is the opponent card.",
			relevantFor: [],
			defaultValue: "attackerEnters",
			values: [
				{
					value: "attackerEnters",
					display: "Selected card enters"
				},
				{
					value: "defenderEnters",
					display: "Opponent card enters"
				}
			]
		});

		this.opponentBoostedOption = this.opponentOptions.add({
			type: "Boolean",
			name: "boosted",
			display: "Boosted",
			tooltip: "Toggle whether or not the opponent card is boosted.",
			relevantFor: [],
			defaultValue: true
		});

		this.opponentRedeathedOption = this.opponentOptions.add({
			type: "Boolean",
			name: "redeathed",
			display: "Redeathed",
			tooltip: "Toggle whether or not the opponent card is redethed.",
			relevantFor: [],
			defaultValue: false
		});

		AbilityStore.listen(this.updateAbilities, this);
	}

	updateAbilities(abilities) {
		this.defenderOptions = abilities.getOptions();
	}

	setCard(card, type, redeathed, level, boosts, build) {
		super.setCard(card, type, redeathed, level, boosts, build);

		this.attackerOptions.setAllInactive();
		if (build !== null) {
			for (var i = 0; i < build.length; i++) {
				build[i].addOptions(this.attackerOptions);
			}
		}
	}

	getOptions() {
		return [
			{
				group: "Opponent Card",
				options: this.opponentOptions.toUIDefinitions()
			},
			{
				group: "Calculator State",
				options: this.calculatorOptions.toUIDefinitions()
			},
			{
				group: "Selected Card Abilities",
				options: this.attackerOptions.toUIDefinitions("Attacker")
			},
			{
				group: "Opponent Card Abilities",
				options: this.defenderOptions.toUIDefinitions("Defender")
			}
		];
	}

	setOption(group, option, value) {
		switch (group) {
			case "Opponent Card":
				this.opponentOptions.set(option, value);
				break;
			case "Calculator State":
				this.calculatorOptions.set(option, value);
				break;
			case "Selected Card Abilities":
				this.attackerOptions.set(option, value);
				break;
			case "Opponent Card Abilities":
				this.defenderOptions.set(option, value);
				break;
		}
	}

	setupCalculationState() {
		var state = {};
		state.simulation = new Simulation();
		state.attackerEnters = this.cardEntryOption.value === "attackerEnters";

		if (state.attackerEnters) {
			state.dummy = state.simulation.createAttackerDummyCard([]);
		}
		else {
			state.dummy = state.simulation.createDefenderDummyCard([]);
		}

		state.attacker = state.simulation.createAttackerCard(
			this.cardData.card,
			this.cardData.type,
			this.cardData.redeathed,
			this.cardData.level,
			this.cardData.boosts,
			this.cardData.build,
			this.attackerOptions
		);

		state.opponentRedeathed = this.opponentRedeathedOption.value;
		state.opponentBoosted = this.opponentBoostedOption.value;

		state.ableToOHKOTotalTypes = 0;
		state.unableToOHKOTotalTypes = 0;

		state.ableToOHKO = [];
		state.unableToOHKO = [];
		return state;
	}

	calculateCards(cards, state) {
		var simulation = state.simulation;
		var attackerEnters = state.attackerEnters;

		var dummy = state.dummy;
		var attacker = state.attacker;

		var opponentRedeathed = state.opponentRedeathed;
		var opponentBoosted = state.opponentBoosted;

		var ableToOHKO = state.ableToOHKO;
		var unableToOHKO = state.unableToOHKO;

		for (var i = 0; i < cards.length; i++) {
			var opponent = cards[i];
			var types = opponent.getTypes();

			var ableToOHKOTypes = [];
			var unableToOHKOTypes = [];

			for (var j = 0; j < types.length; j++) {
				var simulationOpponent = simulation.createDefenderCard(
					opponent,
					types[j],
					opponentRedeathed,
					opponent.getMaxLevel(opponentRedeathed),
					opponentBoosted,
					undefined, // default build
					this.defenderOptions
				);

				simulation.start();

				if (attackerEnters) {
					simulation.forceDefenderFirstAttack();
					simulationOpponent.onAttack((event) => {
						event.unregister();
						dummy.onTakeDamage((event) => {
							event.damage = dummy.getHealth();
							event.unregister();
							event.stop();
						}, -Infinity);
					}, -Infinity);
				}
				else {
					simulation.forceAttackerFirstAttack();
					attacker.onAttack((event) => {
						event.unregister();
						dummy.onTakeDamage((event) => {
							event.damage = dummy.getHealth();
							event.unregister();
							event.stop();
						}, -Infinity);
					}, -Infinity);
				}
				simulation.simulateRound();

				if (attackerEnters) {
					if (simulation.getAttacker() === dummy) {
						simulation.nextAttackerCard();
					}
				}
				else {
					if (simulation.getDefender() === dummy) {
						simulation.nextDefenderCard();
					}
				}

				simulation.forceAttackerFirstAttack();
				simulation.simulateRound();

				//console.log(simulation.getLogText());

				if (simulation.didDecideWinner() && simulation.didAttackerWin()) {
					ableToOHKOTypes.push(types[j]);
					state.ableToOHKOTotalTypes++;
				}
				else {
					simulation.setWin(false);
					unableToOHKOTypes.push(types[j]);
					state.unableToOHKOTotalTypes++;
				}

				simulation.removeDefender(simulationOpponent);
			}

			if (ableToOHKOTypes.length > 0) {
				ableToOHKO.push({
					card: opponent,
					types: ableToOHKOTypes
				});
			}

			if (unableToOHKOTypes.length > 0) {
				unableToOHKO.push({
					card: opponent,
					types: unableToOHKOTypes
				});
			}
		}
	}

	presentCalculation(state) {
		return [
			{
				section: `Unable to OHKO`,
				typeCount: state.unableToOHKOTotalTypes,
				cards: state.unableToOHKO
			},
			{
				section: `Able to OHKO`,
				typeCount: state.ableToOHKOTotalTypes,
				cards: state.ableToOHKO
			}
		];
	}
}

CalculatorActions.addCalculator(new OHKOCalculator());