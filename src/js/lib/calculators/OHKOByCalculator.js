import Calculator from "calculators/Calculator"

import Options from "simulation/Options"
import Simulation from "simulation/Simulation"
import AbilityStore from "stores/dccards/AbilityStore"
import CalculatorActions from "actions/calculator"

export default class OHKOBYCalculator extends Calculator {
	constructor() {
		super("OHKO By Calculator");

		this.cardData = null;

		this.calculatorOptions = new Options();
		this.opponentOptions = new Options();
		this.attackerOptions = new Options();
		this.defenderOptions = new Options();

		this.cardEntryOption = this.calculatorOptions.add({
			type: "Select",
			name: "entry",
			display: "Card entry",
			tooltip: "Choose which card enters the battle, Defender is this card and Attacker is the opponent card.",
			relevantFor: [],
			defaultValue: "attackerEnters",
			values: [
				{
					value: "attackerEnters",
					display: "Opponent card enters"
				},
				{
					value: "defenderEnters",
					display: "Selected card enters"
				}
			]
		});

		this.opponentBoostedOption = this.opponentOptions.add({
			type: "Boolean",
			name: "boosted",
			display: "Boosted",
			tooltip: "Toggle whether or not the opponent card is boosted.",
			relevantFor: [],
			defaultValue: true
		});

		this.opponentRedeathedOption = this.opponentOptions.add({
			type: "Boolean",
			name: "redeathed",
			display: "Redeathed",
			tooltip: "Toggle whether or not the opponent card is redethed.",
			relevantFor: [],
			defaultValue: false
		});

		AbilityStore.listen(this.updateAbilities, this);
	}

	updateAbilities(abilities) {
		this.attackerOptions = abilities.getOptions();
	}

	setCard(card, type, redeathed, level, boosts, build) {
		super.setCard(card, type, redeathed, level, boosts, build);

		this.defenderOptions.setAllInactive();
		if (build !== null) {
			for (var i = 0; i < build.length; i++) {
				build[i].addOptions(this.defenderOptions);
			}
		}
	}

	getOptions() {
		return [
			{
				group: "Opponent Card",
				options: this.opponentOptions.toUIDefinitions()
			},
			{
				group: "Calculator State",
				options: this.calculatorOptions.toUIDefinitions()
			},
			{
				group: "Selected Card Abilities",
				options: this.defenderOptions.toUIDefinitions("Defender")
			},
			{
				group: "Opponent Card Abilities",
				options: this.attackerOptions.toUIDefinitions("Attacker")
			}
		];
	}

	setOption(group, option, value) {
		switch (group) {
			case "Opponent Card":
				this.opponentOptions.set(option, value);
				break;
			case "Calculator State":
				this.calculatorOptions.set(option, value);
				break;
			case "Opponent Card Abilities":
				this.attackerOptions.set(option, value);
				break;
			case "Selected Card Abilities":
				this.defenderOptions.set(option, value);
				break;
		}
	}

	setupCalculationState() {
		var state = {};
		state.simulation = new Simulation();
		state.attackerEnters = this.cardEntryOption.value === "attackerEnters";

		if (state.attackerEnters) {
			state.dummy = state.simulation.createAttackerDummyCard([]);
		}
		else {
			state.dummy = state.simulation.createDefenderDummyCard([]);
		}

		state.defender = state.simulation.createDefenderCard(
			this.cardData.card,
			this.cardData.type,
			this.cardData.redeathed,
			this.cardData.level,
			this.cardData.boosts,
			this.cardData.build,
			this.defenerOptions
		);

		state.opponentRedeathed = this.opponentRedeathedOption.value;
		state.opponentBoosted = this.opponentBoostedOption.value;

		state.ohkoByTotalTypes = 0;
		state.notOHKOByTotalTypes = 0;

		state.ohkoBy = [];
		state.notOHKOBy = [];
		return state;
	}

	calculateCards(cards, state) {
		var simulation = state.simulation;
		var attackerEnters = state.attackerEnters;

		var dummy = state.dummy;
		var defender = state.defender;

		var opponentRedeathed = state.opponentRedeathed;
		var opponentBoosted = state.opponentBoosted;

		var ohkoBy = state.ohkoBy;
		var notOHKOBy = state.notOHKOBy;

		for (var i = 0; i < cards.length; i++) {
			var opponent = cards[i];
			var types = opponent.getTypes();

			var ohkoByTypes = [];
			var notOHKOByTypes = [];

			for (var j = 0; j < types.length; j++) {
				var simulationOpponent = simulation.createAttackerCard(
					opponent,
					types[j],
					opponentRedeathed,
					opponent.getMaxLevel(opponentRedeathed),
					opponentBoosted,
					undefined, // default build
					this.attackerOptions
				);

				simulation.start();

				if (attackerEnters) {
					simulation.forceDefenderFirstAttack();
					simulationOpponent.onAttack((event) => {
						event.unregister();
						dummy.onTakeDamage((event) => {
							event.damage = dummy.getHealth();
							event.unregister();
							event.stop();
						}, -Infinity);
					}, -Infinity);
				}
				else {
					simulation.forceAttackerFirstAttack();
					defender.onAttack((event) => {
						event.unregister();
						dummy.onTakeDamage((event) => {
							event.damage = dummy.getHealth();
							event.unregister();
							event.stop();
						}, -Infinity);
					}, -Infinity);
				}
				simulation.simulateRound();

				if (attackerEnters) {
					if (simulation.getAttacker() === dummy) {
						simulation.nextAttackerCard();
					}
				}
				else {
					if (simulation.getDefender() === dummy) {
						simulation.nextDefenderCard();
					}
				}

				simulation.forceAttackerFirstAttack();
				simulation.simulateRound();

				//console.log(simulation.getLogText());

				if (simulation.didDecideWinner() && simulation.didAttackerWin()) {
					ohkoByTypes.push(types[j]);
					state.ohkoByTotalTypes++;
				}
				else {
					simulation.setWin(false);
					notOHKOByTypes.push(types[j]);
					state.notOHKOByTotalTypes++;
				}

				simulation.removeAttacker(simulationOpponent);
			}

			if (ohkoByTypes.length > 0) {
				ohkoBy.push({
					card: opponent,
					types: ohkoByTypes
				});
			}

			if (notOHKOByTypes.length > 0) {
				notOHKOBy.push({
					card: opponent,
					types: notOHKOByTypes
				});
			}
		}
	}

	presentCalculation(state) {
		return [
			{
				section: `OHKO By`,
				typeCount: state.ohkoByTotalTypes,
				cards: state.ohkoBy
			},
			{
				section: `Not OHKO By`,
				typeCount: state.notOHKOByTotalTypes,
				cards: state.notOHKOBy
			}
		];
	}
}

CalculatorActions.addCalculator(new OHKOBYCalculator());