import Strains from "dccards/Strains"
import Rarities from "dccards/Rarities"
import Types from "dccards/Types"
import Events from "simulation/Events"

function processBuild(build, abilities) {
	if (build === null || build === undefined) {
		return null;
	}
	return [
		abilities.findByName(build[0]),
		abilities.findByName(build[1]),
		abilities.findByName(build[2]),
	];
}

function calculateStat(min, max, level, maxLevel, modifier) {
	var stat = Math.floor(min + ((max - min) / (maxLevel - 1)) * (level - 1));
	stat = Math.floor(stat * modifier);
	return stat;
}

function clampNumber(min, max, value) {
	if (value < min) return min;
	if (value > max) return max;
	return value;
}

function correctBoostAmount(max, value) {
	if (value <= -1) {
		return max;
	}
	return clampNumber(0, max, value);
}

export default class Card {
	static parse(rawCard, abilities) {
		var card = {};

		card.id = rawCard.id;
		card.locked = rawCard.locked;

		card.name = rawCard.name;
		card.description = rawCard.description;
		card.catalog = rawCard.catalog;
		card.cardNumber = rawCard.cardNumber;
		card.spawnArea = rawCard.spawnArea;
		card.shape = rawCard.shape;

		card.strain = Strains.findByName(rawCard.strain);
		card.rarity = Rarities.findByName(rawCard.rarity);

		card.maxLevel = rawCard.maxLevel;
		card.redeathable = rawCard.redeathable;
		card.tradeable = rawCard.tradeable;
		card.extinct = rawCard.extinct;
		card.stats = rawCard.stats;

		card.types = [ ];
		for (var i = 0 ; i < rawCard.types.length; i++) {
			card.types.push(Types.findByName(rawCard.types[i]));
		}

		card.skillset = rawCard.skillset === null ? null : {
			level01: abilities.findByName(rawCard.skillset.level01),
			level15: abilities.findByName(rawCard.skillset.level15),
			level30: abilities.findByName(rawCard.skillset.level30),
			level40: abilities.findByName(rawCard.skillset.level40),
			level50: abilities.findByName(rawCard.skillset.level50),
			redeath: abilities.findByName(rawCard.skillset.redeath),
		};

		card.builds = rawCard.builds === null ? null : {
			average: processBuild(rawCard.builds.average, abilities),
			fresh: processBuild(rawCard.builds.fresh, abilities),
			mental: processBuild(rawCard.builds.mental, abilities),
			strong: processBuild(rawCard.builds.strong, abilities),
			tough: processBuild(rawCard.builds.tough, abilities),
			quick: processBuild(rawCard.builds.quick, abilities),
			smart: processBuild(rawCard.builds.smart, abilities),
			perfect: processBuild(rawCard.builds.perfect, abilities),
			redeath: rawCard.builds.redeath === null ? null : {
				average: processBuild(rawCard.builds.redeath.average, abilities),
				fresh: processBuild(rawCard.builds.redeath.fresh, abilities),
				mental: processBuild(rawCard.builds.redeath.mental, abilities),
				strong: processBuild(rawCard.builds.redeath.strong, abilities),
				tough: processBuild(rawCard.builds.redeath.tough, abilities),
				quick: processBuild(rawCard.builds.redeath.quick, abilities),
				smart: processBuild(rawCard.builds.redeath.smart, abilities),
				perfect: processBuild(rawCard.builds.redeath.perfect, abilities),
			}
		};

		return new Card(card);
	}

	constructor(card) {
		this.id = card.id;
		this.locked = card.locked;

		this.name = card.name;
		this.description = card.description;
		this.catalog = card.catalog;
		this.cardNumber = card.cardNumber;
		this.spawnArea = card.spawnArea;
		this.shape = card.shape;

		this.strain = card.strain;
		this.rarity = card.rarity;

		this.maxLevel = card.maxLevel;
		this.redeathable = card.redeathable;
		this.tradeable = card.tradeable;
		this.extinct = card.extinct;
		this.stats = card.stats;

		this.types = card.types;

		this.skillset = card.skillset === null ? null : {
			level01: card.skillset.level01,
			level15: card.skillset.level15,
			level30: card.skillset.level30,
			level40: card.skillset.level40,
			level50: card.skillset.level50,
			redeath: card.skillset.redeath,
		}

		this.builds = card.builds === null ? null : {
			average: card.builds.average,
			fresh: card.builds.fresh,
			mental: card.builds.mental,
			strong: card.builds.strong,
			tough: card.builds.tough,
			quick: card.builds.quick,
			smart: card.builds.smart,
			perfect: card.builds.perfect,
			redeath: card.builds.redeath === null ? null : {
				average: card.builds.redeath.average,
				fresh: card.builds.redeath.fresh,
				mental: card.builds.redeath.mental,
				strong: card.builds.redeath.strong,
				tough: card.builds.redeath.tough,
				quick: card.builds.redeath.quick,
				smart: card.builds.redeath.smart,
				perfect: card.builds.redeath.perfect,
			}
		}
	}

	getID() {
		return this.id;
	}

	getLocked() {
		return this.locked;
	}

	getName(borderStripped=false) {
		if (borderStripped) {
			var charPosition = 0;
			var cardName = this.name;
			var rarity = this.getRarity();

			if (rarity === Rarities.pestilent) {
				if (cardName.indexOf('Pestilent') === 0) {
					charPosition = 10;
				}
				if (cardName.indexOf('Pest.') === 0) {
					charPosition = 6;
				}
			}
			else if (rarity === Rarities.virulent) {
				if (cardName.indexOf('Virulent') === 0) {
					charPosition = 9;
				}
				if (cardName.indexOf('Vir.') === 0) {
					charPosition = 5;
				}
			}

			if (charPosition !== 0) {
				return cardName.substr(charPosition);
			}
		}
		return this.name;
	}

	getDescription() {
		return this.description;
	}

	getCatalog() {
		return this.catalog;
	}

	getCardNumber() {
		return this.cardNumber;
	}

	getSpawnArea() {
		return this.spawnArea;
	}

	getShape() {
		return this.shape;
	}

	getStrain() {
		return this.strain;
	}

	getRarity() {
		return this.rarity;
	}

	getTypes() {
		return this.types.slice();
	}

	getMaxLevel(redeathed=false) {
		if (redeathed) {
			if (this.getRedeathable()) {
				return this.getRarity().getMaxRedeathLevel();
			}
		}
		return this.maxLevel;
	}

	getRedeathable() {
		return this.redeathable;
	}

	getTradeable() {
		return this.tradeable;
	}

	getExtinct() {
		return this.extinct;
	}

	getSkillset() {
		return this.skillset;
	}

	getBuild(type, redeathed, level=this.getMaxLevel(redeathed)) {
		if (this.isSimulatable() === false) {
			return [];
		}
		redeathed = redeathed && this.getRedeathable();
		var typeName = type.getName().toLowerCase();

		var build = null;
		var normalBuild = null;
		var builds = this.builds;
		if (builds !== null) {
			var redeathBuilds = builds.redeath;

			if (redeathed && redeathBuilds !== null) {
				build = redeathBuilds[typeName];
				if (build === null) {
					build = redeathBuilds.average;
				}
			}

			normalBuild = builds[typeName];
			if (normalBuild === null) {
				normalBuild = builds.average;
			}

			if (build === null) {
				build = normalBuild;
			}
		}

		var maxLevel = this.getMaxLevel(redeathed);
		if (build === null || level !== maxLevel) {
			if (redeathed && build !== null && normalBuild !== null && build !== normalBuild) {
				var equalAbilityCount = 0;
				for (var i = 0; i < build.length; i++) {
					if (normalBuild.indexOf(build[i]) !== -1) {
						equalAbilityCount++;
					}
				}
				if (equalAbilityCount >= 2) {
					build = normalBuild;
				}
			}

			var skillset = this.getSkillset();
			if (level < 50) {
				if (level < 40 || (level === 40 && this.getRarity() === Rarities.common)) {
					build = [
						skillset.level01
					];
					if (level >= 15) {
						build.push(skillset.level15);
					}
					if (level >= 30) {
						build.push(skillset.level30);
					}
				}
				else {
					if (build === null || build.indexOf(skillset.level01) === -1) {
						build = [
							skillset.level15,
							skillset.level30,
							skillset.level40
						];
					}
					else {
						var availableAbilities = [
							skillset.level01,
							skillset.level15,
							skillset.level30,
							skillset.level40
						];
						var newBuild = [];
						var remainingAbilities = [];

						var hasAttack = false;
						var attackInRemaining = false;
						for (var i = 0; i < availableAbilities.length; i++) {
							var ability = availableAbilities[i];
							if (build.indexOf(ability) !== -1) {
								newBuild.push(ability);
								if (ability.getEvent() === Events.attack) {
									hasAttack = true;
								}
							}
							else {
								remainingAbilities.push(ability);
								if (ability.getEvent() === Events.attack) {
									attackInRemaining = true;
								}
							}
						}

						if (newBuild.length < 3) {
							if (!hasAttack && attackInRemaining) {
								for (var i = remainingAbilities.length-1; i >= 0 ; i--) {
									if (remainingAbilities[i].getEvent() === Events.attack) {
										newBuild.push(remainingAbilities.splice(i, 1)[0]);
										break;
									}
								}
							}
							while (newBuild.length < 3) {
								newBuild.push(remainingAbilities.pop());
							}
						}
						build = newBuild;
					}
				}
			} // if (level < 50)
			else if (build === null) {
				if (redeathed && level === maxLevel) {
					build = [
						skillset.level40,
						skillset.level50,
						skillset.redeath
					];
				}
				else {
					build = [
						skillset.level30,
						skillset.level40,
						skillset.level50
					];
				}
			}
		}

		return build.slice();
	}

	getStats(type, redeathed, level, boosts) {
		if (this.isSimulatable() === false) {
			return {health:0, psyche:0, attack:0, defense:0, speed:0, intelligence:0};
		}
		var result = {};
		var maxLevel = this.getRarity().getMaxLevel();
		var modifiers = type.getStatModifiers();

		result.health = calculateStat(this.stats.healthMin, this.stats.healthMax, level, maxLevel, modifiers.health);
		result.psyche = calculateStat(this.stats.psycheMin, this.stats.psycheMax, level, maxLevel, modifiers.psyche);
		result.attack = calculateStat(this.stats.attackMin, this.stats.attackMax, level, maxLevel, modifiers.attack);
		result.defense = calculateStat(this.stats.defenseMin, this.stats.defenseMax, level, maxLevel, modifiers.defense);
		result.speed = calculateStat(this.stats.speedMin, this.stats.speedMax, level, maxLevel, modifiers.speed);
		result.intelligence = calculateStat(this.stats.intelligenceMin, this.stats.intelligenceMax, level, maxLevel, modifiers.intelligence);

		if (boosts === false) {
			return result;
		}
		boosts = this.correctBoosts(boosts, redeathed);

		result.health += boosts.health;
		result.psyche += boosts.psyche;
		result.attack += boosts.attack;
		result.defense += boosts.defense;
		result.speed += boosts.speed;
		result.intelligence += boosts.intelligence;
		return result;
	}

	correctBoosts(boosts, redeathed) {
		if (boosts === false) {
			return {health:0, psyche:0, attack:0, defense:0, speed:0, intelligence:0};
		}

		var result = this.getBoosts(redeathed);
		if (boosts === true) {
			return result;
		}

		result.health = correctBoostAmount(result.health, boosts.health);
		result.psyche = correctBoostAmount(result.psyche, boosts.psyche);
		result.attack = correctBoostAmount(result.attack, boosts.attack);
		result.defense = correctBoostAmount(result.defense, boosts.defense);
		result.speed = correctBoostAmount(result.speed, boosts.speed);
		result.intelligence = correctBoostAmount(result.intelligence, boosts.intelligence);
		return result;
	}

	getBoosts(redeathed) {
		if (this.isSimulatable() === false) {
			return {health:0, psyche:0, attack:0, defense:0, speed:0, intelligence:0};
		}
		var boosts = {};

		// Use the levels from Rarity to get correct results for chicks/cards with a max level of 1
		var level;
		if (redeathed && this.getRedeathable()) {
			level = this.getRarity().getMaxRedeathLevel();
		}
		else {
			level = this.getRarity().getMaxLevel();
		}
		var maxLevel = this.getRarity().getMaxLevel();

		boosts.health = calculateStat(this.stats.healthMin, this.stats.healthMax, level, maxLevel, 0.2);
		boosts.psyche = calculateStat(this.stats.psycheMin, this.stats.psycheMax, level, maxLevel, 0.2);
		boosts.attack = calculateStat(this.stats.attackMin, this.stats.attackMax, level, maxLevel, 0.2);
		boosts.defense = calculateStat(this.stats.defenseMin, this.stats.defenseMax, level, maxLevel, 0.2);
		boosts.speed = calculateStat(this.stats.speedMin, this.stats.speedMax, level, maxLevel, 0.2);
		boosts.intelligence = calculateStat(this.stats.intelligenceMin, this.stats.intelligenceMax, level, maxLevel, 0.2);
		return boosts;
	}

	isSimulatable() {
		if (this.stats === null) {
			return false;
		}
		if (this.skillset === null) {
			return false;
		}
		if (this.types.length === 0) {
			return false;
		}
		return true;
	}

	compareTo(card, nameOnly=false, borderStripped=false) {
		if (nameOnly === false) {
			var result = this.getRarity().compareTo(card.getRarity());
			if (result !== 0) {
				return -result;
			}
			result = this.getStrain().compareTo(card.getStrain());
			if (result !== 0) {
				return result;
			}
		}
		return this.getName(borderStripped).localeCompare(card.getName(borderStripped));
	}
}