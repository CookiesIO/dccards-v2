export class Type {
	constructor(index, name, statModifiers) {
		this.index = index;
		this.name = name;
		this.statModifiers = statModifiers;
	}

	getName() {
		return this.name;
	}

	getStatModifiers() {
		return {
			health:       this.statModifiers.health,
			psyche:       this.statModifiers.psyche,
			attack:       this.statModifiers.attack,
			defense:      this.statModifiers.defense,
			speed:        this.statModifiers.speed,
			intelligence: this.statModifiers.intelligence
		};
	}

	compareTo(rarity) {
		return this.index - rarity.index;
	}
}

var types = {
	average: new Type(0, "Average", {
		health:       1.00,
		psyche:       1.00,
		attack:       1.00,
		defense:      1.00,
		speed:        1.00,
		intelligence: 1.00
	}),
	fresh: new Type(1, "Fresh",   {
		health:       1.10,
		psyche:       1.00,
		attack:       1.00,
		defense:      1.00,
		speed:        0.95,
		intelligence: 1.00
	}),
	mental: new Type(2, "Mental",  {
		health:       1.00,
		psyche:       1.10,
		attack:       0.95,
		defense:      1.00,
		speed:        1.00,
		intelligence: 1.00
	}),
	strong: new Type(3, "Strong",  {
		health:       1.00,
		psyche:       1.00,
		attack:       1.10,
		defense:      1.00,
		speed:        1.00,
		intelligence: 0.95
	}),
	tough: new Type(4, "Tough",   {
		health:       1.00,
		psyche:       0.95,
		attack:       1.00,
		defense:      1.10,
		speed:        1.00,
		intelligence: 1.00
	}),
	quick: new Type(5, "Quick",   {
		health:       0.95,
		psyche:       1.00,
		attack:       1.00,
		defense:      1.00,
		speed:        1.10,
		intelligence: 1.00
	}),
	smart: new Type(6, "Smart",   {
		health:       1.00,
		psyche:       1.00,
		attack:       1.00,
		defense:      0.95,
		speed:        1.00,
		intelligence: 1.10
	}),
	perfect: new Type(7, "Perfect", {
		health:       1.10,
		psyche:       1.10,
		attack:       1.10,
		defense:      1.10,
		speed:        1.10,
		intelligence: 1.10
	}),
}

export default {
	findByName(name) {
		switch (name.toLowerCase()) {
			case "average": return types.average;
			case "fresh":   return types.fresh;
			case "mental":  return types.mental;
			case "strong":  return types.strong;
			case "tough":   return types.tough;
			case "quick":   return types.quick;
			case "smart":   return types.smart;
			case "perfect": return types.perfect;
		}
		return null;
	},

	getAll() {
		return [
			types.average,
			types.fresh,
			types.mental,
			types.strong,
			types.tough,
			types.quick,
			types.smart,
			types.perfect,
		];
	},

	average: types.average,
	fresh:   types.fresh,
	mental:  types.mental,
	strong:  types.strong,
	tough:   types.tough,
	quick:   types.quick,
	smart:   types.smart,
	perfect: types.perfect,
}