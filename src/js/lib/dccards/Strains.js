export class Strain {
	constructor(index, name, critting, blockedBy) {
		this.index = index;
		this.name = name;
		this.critting = critting;
		this.blockedBy = blockedBy;
	}

	getName() {
		return this.name;
	}

	isBlockedBy(strain) {
		if (strain instanceof Strain) {
			strain = strain.getName();
		}
		if (typeof strain === "string") {
			for (var i = 0; i < this.blockedBy.length; i++) {
				if (this.blockedBy[i] === strain) {
					return true;
				}
			}
		}
		return false;
	}

	isCritting(strain) {
		if (strain instanceof Strain) {
			strain = strain.getName();
		}
		if (typeof strain === "string") {
			for (var i = 0; i < this.critting.length; i++) {
				if (this.critting[i] === strain) {
					return true;
				}
			}
		}
		return false;
	}

	getElementalModifier(strain) {
		if (this.isBlockedBy(strain)) {
			return 0.85;
		}
		if (this.isCritting(strain)) {
			return 1.15;
		}
		return 1;
	}

	compareTo(strain) {
		return this.index - strain.index;
	}
}

var strains = {
	voider:   new Strain(0, "Voider",   [ ], [ ]),
	burner:   new Strain(1, "Burner",   ["Screamer", "Charmer"], ["Burner" , "Chiller" , "Spitter"]),
	chiller:  new Strain(2, "Chiller",  ["Burner"  , "Leecher"], ["Chiller", "Shocker" , "Spitter"]),
	slasher:  new Strain(3, "Slasher",  ["Shocker" , "Spitter"], ["Slasher", "Screamer", "Leecher"]),
	shocker:  new Strain(4, "Shocker",  ["Chiller" , "Leecher"], ["Slasher", "Shocker" , "Screamer"]),
	screamer: new Strain(5, "Screamer", ["Slasher" , "Shocker"], ["Burner" , "Screamer", "Charmer"]),
	spitter:  new Strain(6, "Spitter",  ["Burner"  , "Chiller"], ["Slasher", "Spitter" , "Charmer"]),
	charmer:  new Strain(7, "Charmer",  ["Screamer", "Spitter"], ["Burner" , "Charmer" , "Leecher"]),
	leecher:  new Strain(8, "Leecher",  ["Slasher" , "Charmer"], ["Chiller", "Shocker" , "Leecher"]),
}

export default {
	findByName(name) {
		switch (name.toLowerCase()) {
			case "voider":   return strains.voider;
			case "burner":   return strains.burner;
			case "chiller":  return strains.chiller;
			case "slasher":  return strains.slasher;
			case "shocker":  return strains.shocker;
			case "screamer": return strains.screamer;
			case "spitter":  return strains.spitter;
			case "charmer":  return strains.charmer;
			case "leecher":  return strains.leecher;
		}
		return null;
	},

	getAll() {
		return [
			strains.voider,
			strains.burner,
			strains.chiller,
			strains.slasher,
			strains.shocker,
			strains.screamer,
			strains.spitter,
			strains.charmer,
			strains.leecher,
		];
	},

	voider:   strains.voider,
	burner:   strains.burner,
	chiller:  strains.chiller,
	slasher:  strains.slasher,
	shocker:  strains.shocker,
	screamer: strains.screamer,
	spitter:  strains.spitter,
	charmer:  strains.charmer,
	leecher:  strains.leecher,
}