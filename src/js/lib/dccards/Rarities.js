export class Rarity {
	constructor(index, name, maxLevel, maxRedeathLevel=null) {
		this.index = index;
		this.name = name;
		this.maxLevel = maxLevel;
		this.maxRedeathLevel = maxRedeathLevel;
	}

	getName() {
		return this.name;
	}

	getMaxLevel() {
		return this.maxLevel;
	}

	getMaxRedeathLevel() {
		return this.maxRedeathLevel;
	}

	getRedeathable() {
		return this.maxRedeathLevel !== null;
	}

	compareTo(rarity) {
		return this.index - rarity.index;
	}
}

var rarities = {
	common:    new Rarity(0, "Common",    40),
	uncommon:  new Rarity(1, "Uncommon",  45),
	rare:      new Rarity(2, "Rare",      50),
	epic:      new Rarity(3, "Epic",      55, 60),
	epicPlus:  new Rarity(4, "Epic Plus", 55, 60),
	legendary: new Rarity(5, "Legendary", 60, 70),
	virulent:  new Rarity(6, "Virulent",  60, 70),
	pestilent: new Rarity(7, "Pestilent", 60, 70),
}

export default {
	findByName(name) {
		switch (name.toLowerCase()) {
			case "common":    return rarities.common;
			case "uncommon":  return rarities.uncommon;
			case "rare":      return rarities.rare;
			case "epic":      return rarities.epic;
			case "epic plus": return rarities.epicPlus;
			case "legendary": return rarities.legendary;
			case "virulent":  return rarities.virulent;
			case "pestilent": return rarities.pestilent;
		}
		return null;
	},

	getAll() {
		return [
			rarities.common,
			rarities.uncommon,
			rarities.rare,
			rarities.epic,
			rarities.epicPlus,
			rarities.legendary,
			rarities.virulent,
			rarities.pestilent,
		];
	},

	common:    rarities.common,
	uncommon:  rarities.uncommon,
	rare:      rarities.rare,
	epic:      rarities.epic,
	epicPlus:  rarities.epicPlus,
	legendary: rarities.legendary,
	virulent:  rarities.virulent,
	pestilent: rarities.pestilent,
}