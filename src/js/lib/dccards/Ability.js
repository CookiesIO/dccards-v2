export default class Ability {
	static parse(rawAbility) {
		var ability = {};
		ability.name = rawAbility.name;
		ability.effect = rawAbility.effect;
		ability.cost = rawAbility.cost;
		ability.costStat = rawAbility.costStat;
		ability.event = rawAbility.event;
		ability.priority = rawAbility.priority;
		return new Ability(ability);
	}

	constructor(ability) {
		this.name = ability.name;
		this.effect = ability.effect;
		this.cost = ability.cost;
		this.costStat = ability.costStat;
		this.event = ability.event;
		this.priority = ability.priority;
	}

	getName() {
		return this.name;
	}

	getEffect() {
		return this.effect;
	}

	getEvent() {
		return this.event;
	}

	getPriority() {
		return this.priority;
	}

	getCost() {
		return this.cost;
	}

	getCostStat() {
		return this.costStat;
	}
}