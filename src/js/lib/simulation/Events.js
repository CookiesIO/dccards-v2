export default {
	entry: "Entry",
	buff: "Buff",
	clearBuff: "Clear Buff",
	initialAttack: "Initial Attack",
	attack: "Attack",
	struck: "Struck",
	takeDamage: "Take Damage",
	damageTaken: "Damage Taken",
	death: "Death",
	enemyUsedAbility: "Enemy Used Ability",
	endTurn: "End Turn"
}