import Dictionary from "collections/dict"

class Option {
	static create(option) {
		switch (option.type) {
			case "Boolean": return BooleanOption.create(option);
			case "Select": return SelectOption.create(option);
		}
		return null;
	}

	constructor(option) {
		this.type = option.type;
		this.name = option.name;
		this.display = option.display;
		this.tooltip = option.tooltip;
		this.value = option.defaultValue;
		this.defaultValue = option.defaultValue;
		this.relevantFor = option.relevantFor;

		this.isActive = true;
	}

	isRelevantFor(target) {
		for (var i = 0; i < this.relevantFor.length; i++) {
			if (this.relevantFor[i] === target) {
				return true;
			}
		}
		return false;
	}

	simulate(card, simulation) {
		throw new Error(`Implement simulate in: ${this}`);
	}

	toUIDefinition(relevancy) {
		return {
			type: this.type,
			name: this.name,
			display: this.display,
			tooltip: this.tooltip,
			value: this.value
		};
	}
}

class BooleanOption extends Option {
	static create(option) {
		if (typeof option.chance === "number") {
			var chance = option.chance;
			option.simulate = () => {
				return Math.random() < chance;
			}
		}
		else if (option.chance instanceof Function) {
			option.simulate = option.chance;
		}
		return new BooleanOption(option);
	}

	constructor(option) {
		super(option);
		if (option.simulate) {
			this.simulate = option.simulate;
		}
	}
}

class SelectOption extends Option  {
	static create(option) {
		if (option.values instanceof Array) {
			SelectOption.fixValues(option.values);

			var values = option.values;
			option.simulate = () => {
				var chance = 0;
				var random = Math.random();
				for (var i = 0; i < values.length; i++) {
					chance += values[i].chance;
					if (random < chance) {
						return values[i].name;
					}
				}
				return null;
			}
		}
		else {
			return null;
		}
		return new SelectOption(option);
	}

	static fixValues(values) {
		var totalChance = 0;
		var unsetValues = [];
		for (var i = 0; i < values.length; i++) {
			var value = values[i];
			if (value.chance) {
				totalChance += value.chance;
			}
			else {
				unsetValues.push(value);
			}
		}

		if (unsetValues.length !== 0) {
			var chance = (1 - totalChance) / unsetValues.length;
			for (var i = 0; i < unsetValues.length; i++) {
				var value = unsetValues[i];
				value.chance = chance;
			}
		}
	}

	constructor(option) {
		super(option);
		this.values = option.values;
		if (option.simulate) {
			this.simulate = option.simulate;
		}
		if (option.filterDisplay) {
			this.filterDisplay = option.filterDisplay;
		}
	}

	toUIDefinition(relevancy) {
		var definition = super.toUIDefinition();
		definition.values = [];
		for (var i = 0; i < this.values.length; i++) {
			var value = this.values[i];
			var displayValue = {
				value: value.value,
				display: value.display
			};
			if (this.filterDisplay === undefined || this.filterDisplay(displayValue, relevancy)) {
				definition.values.push(displayValue);
			}
		}
		return definition;
	}
}

export default class {
	constructor() {
		this.options = new Dictionary();
	}

	set(optionName, value) {
		var option = this.options.get(optionName, null);
		if (option !== null) {
			option.value = value;
		}
	}

	get(optionName, card, simulation) {
		var option = this.options.get(optionName, null);
		if (option !== null) {
			if (option.value === null) {
				option.simulate(card, simulation);
			}
			else {
				return option.value;
			}
		}
		return null;
	}

	add(option) {
		var existingOption = this.options.get(option.name, null);
		if (existingOption !== null) {
			existingOption.isActive = true;
			return existingOption;
		}

		var createdOption = Option.create(option);
		if (createdOption === null) {
			throw new Error(`Invalid option: ${option.name}`);
		}
		else {
			this.options.set(createdOption.name, createdOption);
			return createdOption;
		}
	}

	setAllInactive() {
		var options = this.options.values();
		for (var i = 0; i < options.length; i++) {
			var option = options[i];
			if (option.isActive) {
				option.isActive = false;
			}
		}
	}

	toUIDefinitions(relevancy=null) {
		var options = this.options.values();
		var definitions = [];
		for (var i = 0; i < options.length; i++) {
			var option = options[i];
			if (option.isActive
			&& (relevancy === null || option.isRelevantFor(relevancy))) {
				var definition = option.toUIDefinition(relevancy);
				definitions.push(definition);
			}
		}
		return definitions;
	}
}