import {Type} from "dccards/Types"
import Event from "./Event"
import Options from "./Options"
import Status from "./statuses/Status"
import OffensiveAbility from "./abilities/OffensiveAbility"

import AbilityData from "dccards/Ability"
import Events from "simulation/Events"

import Dictionary from "collections/dict"

class StatusCollection {
	constructor() {
		this.statuses = {};
	}

	add(status) {
		if (status instanceof Status && !this.has(status)) {
			if (this.hasExactClass(status.constructor)) {
				if (!status.allowStacking()) {
					return false;
				}
			}
			else {
				this.statuses[status.getName()] = [];
			}
			this.statuses[status.getName()].push(status);
			return true;
		}
		return false;
	}

	remove(status) {
		if (status instanceof Status && this.has(status)) {
			var statuses = this.statuses[status.getName()];
			for (var i = 0; i < statuses.length; i++) {
				if (statuses[i] === status) {
					statuses.splice(i, 1);
					break;
				}
			}
			if (statuses.length === 0) {
				delete this.statuses[status.getName()];
			}
			return true;
		}
		return false;
	}

	has(status) {
		if (this.statuses.hasOwnProperty(status.getName())) {
			var statuses = this.statuses[status.getName()];
			for (var i = 0; i < statuses.length; i++) {
				if (statuses[i] === status) {
					return true;
				}
			}
		}
		return false;
	}

	hasClass(tclass) {
		for (var status in this.statuses) {
			if (this.statuses.hasOwnProperty(status)) {
				var statuses = this.statuses[status];
				for (var i = 0; i < statuses.length; i++) {
					if (statuses[i] instanceof tclass) {
						return true;
					}
				}
			}
		}
		return false;
	}

	hasExactClass(tclass) {
		return this.statuses.hasOwnProperty(tclass.getName());
	}

	getAll() {
		var result = [];
		for (var status in this.statuses) {
			if (this.statuses.hasOwnProperty(status)) {
				var statuses = this.statuses[status];
				for (var i = 0; i < statuses.length; i++) {
					result.push(statuses[i]);
				}
			}
		}
		return result;
	}

	getAllOfClass(tclass) {
		var result = [];
		for (var status in this.statuses) {
			if (this.statuses.hasOwnProperty(status)) {
				var statuses = this.statuses[status];
				for (var i = 0; i < statuses.length; i++) {
					if (statuses[i] instanceof tclass) {
						result.push(statuses[i]);
					}
				}
			}
		}
		return result;
	}

	getAllOfExactClass(tclass) {
		if (this.hasExactClass(tclass.getName())) {
			return this.statuses[tclass.getName()].slice();
		}
		return [];
	}

	count(tclass=Status) {
		var result = 0;
		for (var status in this.statuses) {
			if (this.statuses.hasOwnProperty(status)) {
				var statuses = this.statuses[status];
				for (var i = 0; i < statuses.length; i++) {
					if (statuses[i] instanceof tclass) {
						result++;
					}
				}
			}
		}
		return result;
	}

	countExact(tclass) {
		if (this.hasExactClass(tclass)) {
			return this.statuses[tclass.getName()].length;
		}
		return 0;
	}
}

class Punch extends OffensiveAbility {
	logUse(card, simulation) {
		simulation.log(`${card.getName()} attacks!`);
	}

	getDamage(attacker, defender) {
		return OffensiveAbility.calculatePhysicalDamage(attacker, 0, defender);
	}
}

var punchInstance = new Punch(new AbilityData({
	name: "",
	effect: "",
	event: Events.attack,
	costStat: "psyche",
	cost: 0,
	priority: 0
}));

export default class {
	constructor(simulation, card, type, redeathed, level, boosts, build=card.getBuild(type, redeathed), options=new Options()) {
		this.simulation = simulation;

		this.card = card;
		this.redeathed = redeathed;
		this.level = level;

		this.build = build.slice();
		this.options = options;
		for (var i = 0; i < build.length; i++) {
			build[i].addOptions(options);
		}

		this.stats = card.getStats(type, redeathed, level, boosts);
		this.statuses = new StatusCollection();
		this.buffs = {
			attack: 0,
			defense: 0,
			speed: 0,
			intelligence: 0
		};
		this.debuffs = {
			attack: 0,
			defense: 0,
			speed: 0,
			intelligence: 0
		};
		this.health = null;
		this.psyche = null;
		this.maxHealth = null;
		this.maxPsyche = null;
		this.override = null;

		this.onEntryEvent = new Event();
		this.onBuffEvent = new Event();
		this.onClearBuffEvent = new Event();
		this.onInitialAttackEvent = new Event();
		this.onAttackEvent = new Event();
		this.onStruckEvent = new Event();
		this.onTakeDamageEvent = new Event();
		this.onDamageTakenEvent = new Event();
		this.onDeathEvent = new Event();
		this.onEnemyUsedAbilityEvent = new Event();
		this.onEndTurnEvent = new Event();

		this.events = new Dictionary();
		this.events.set(Events.entry, this.onEntryEvent);
		this.events.set(Events.buff, this.onBuffEvent);
		this.events.set(Events.clearBuff, this.onClearBuffEvent);
		this.events.set(Events.initialAttack, this.onInitialAttackEvent);
		this.events.set(Events.attack, this.onAttackEvent);
		this.events.set(Events.struck, this.onStruckEvent);
		this.events.set(Events.takeDamage, this.onTakeDamageEvent);
		this.events.set(Events.damageTaken, this.onDamageTakenEvent);
		this.events.set(Events.death, this.onDeathEvent);
		this.events.set(Events.enemyUsedAbility, this.onEnemyUsedAbilityEvent);
		this.events.set(Events.endTurn, this.onEndTurnEvent);
	}

	getCard() {
		return this.card;
	}

	getName() {
		if (this.override !== null) {
			return this.override.name;
		}
		return this.getCard().getName();
	}

	getStrain() {
		return this.getCard().getStrain();
	}

	getBuild() {
		return this.build.slice();
	}

	getOptions() {
		return this.options;
	}

	getOption(option) {
		return this.options.get(option, this, this.simulation);
	}

	reset() {
		this.clearStatuses(Status);

		this.health = this.stats.health;
		this.psyche = this.stats.psyche;
		this.maxHealth = this.stats.health;
		this.maxPsyche = this.stats.psyche;
		this.override = null;

		this.onEntryEvent.clear();
		this.onBuffEvent.clear();
		this.onClearBuffEvent.clear();
		this.onInitialAttackEvent.clear();
		this.onAttackEvent.clear();
		this.onStruckEvent.clear();
		this.onTakeDamageEvent.clear();
		this.onDamageTakenEvent.clear();
		this.onDeathEvent.clear();
		this.onEnemyUsedAbilityEvent.clear();
		this.onEndTurnEvent.clear();

		for (var i = 0; i < this.build.length; i++) {
			this.build[i].setup(this, this.simulation);
		}
	}

	// Health stuff
	willDie(damage) {
		return this.health <= damage;
	}

	isDead() {
		return this.health <= 0;
	}

	isAlive() {
		return this.health > 0;
	}

 	// Stat getters
 	getExpendableStat(stat) {
 		switch (stat) {
 			case "health": return this.getHealth();
 			case "psyche": return this.getPsyche();
 		}
 		return 0;
 	}

	getHealth() {
		if (this.isDead()) {
			return 0;
		}
		return this.health;
	}

	getHealthRatio() {
		if (this.isDead()) {
			return 0;
		}
		return this.health / this.maxHealth
	}

	getMaxHealth() {
		return this.maxHealth;
	}

	getPsyche() {
		return this.psyche;
	}

	getMaxPsyche() {
		return this.maxPsyche;
	}

	getAttack(modifier=0) {
		var attack = this.stats.attack;
		if (this.override !== null) {
			attack = this.override.stats.attack;
		}
		return Math.max(Math.floor(attack * (1 + this.buffs.attack - this.debuffs.attack + modifier)), 1);
	}

	getDefense() {
		var defense = this.stats.defense;
		if (this.override !== null) {
			defense = this.override.stats.defense;
		}
		return Math.max(Math.floor(defense * (1 + this.buffs.defense - this.debuffs.defense)), 1);
	}

	getSpeed() {
		var speed = this.stats.speed;
		if (this.override !== null) {
			speed = this.override.stats.speed;
		}
		return Math.max(Math.floor(speed * (1 + this.buffs.speed - this.debuffs.speed)), 1);
	}

	getIntelligence(modifier=0) {
		var intelligence = this.stats.intelligence;
		if (this.override !== null) {
			intelligence = this.override.stats.intelligence;
		}
		return Math.max(Math.floor(intelligence * (1 + this.buffs.intelligence - this.debuffs.intelligence + modifier)), 1);
	}

	// Actions
	enter() {
		this.onEntryEvent.notify();
	}

	buff() {
		this.onBuffEvent.notify();
	}

	clearBuff() {
		this.onClearBuffEvent.notify();
	}

	initialAttack() {
		this.onInitialAttackEvent.notify();
	}

	attack() {
		var event = {
			ability: null,
			damage: 0
		};
		this.onAttackEvent.notify(event);

		var ability = event.ability;
		if (ability === null) {
			ability = punchInstance;
		}
		ability.use(this, this.simulation);
	}

	enemyUsedAbility(ability) {
		var event = {
			ability: ability
		};
		this.onEnemyUsedAbilityEvent.notify(event);
	}

	endTurn() {
		this.onEndTurnEvent.notify();
	}

	strike(ability, strikeFunction=null) {
		var result = true;
		if (ability !== null) {
			var event = {
				ability: ability,
				prevent: () => {
					result = false;
				}
			};
			this.onStruckEvent.notify(event);
		}
		if (strikeFunction !== null) {
			if (result) {
				strikeFunction.apply(undefined);
				if (this.isDead()) {
					this.onDeathEvent.notify();
				}
			}
		}
		return result;
	}

	// Events
	on(eventName, callback, priority) {
		var event = this.events.get(eventName);
		if (event !== undefined) {
			return event.listen(callback, priority);
		}
		return null;
	}

	onEntry(callback, priority) {
		return this.onEntryEvent.listen(callback, priority);
	}

	onBuff(callback, priority) {
		return this.onBuffEvent.listen(callback, priority);
	}

	onClearBuff(callback, priority) {
		return this.onClearBuffEvent.listen(callback, priority);
	}

	onInitialAttack(callback, priority) {
		return this.onInitialAttackEvent.listen(callback, priority);
	}

	onAttack(callback, priority) {
		return this.onAttackEvent.listen(callback, priority);
	}

	onStruck(callback, priority) {
		return this.onStruckEvent.listen(callback, priority);
	}

	onTakeDamage(callback, priority) {
		return this.onTakeDamageEvent.listen(callback, priority);
	}

	onDamageTaken(callback, priority) {
		return this.onDamageTakenEvent.listen(callback, priority);
	}

	onDeath(callback, priority) {
		return this.onDeathEvent.listen(callback, priority);
	}

	onEnemyUsedAbility(callback, priority) {
		return this.onEnemyUsedAbilityEvent.listen(callback, priority);
	}

	onEndTurn(callback, priority) {
		return this.onEndTurnEvent.listen(callback, priority);
	}

	// Mutators
	kill(ignoreOnTakeDamage=false) {
		this.simulation.log(`Death comes for ${this.getName()}...`);
		this.damage(this.getHealth(), ignoreOnTakeDamage, true);
	}

	damage(amount, ignoreOnTakeDamage=false, ignoreDamageLog=false) {
		if (amount <= 0) {
			throw new Error(`Must apply 1 or more damage, attempted to damage: ${amount}`);
		}
		if (this.isDead()) {
			return;
		}

		if (!ignoreDamageLog) {
			this.simulation.log(`${this.getName()} takes ${amount} damage.`);
		}

		var dealDamage = true;
		if (!ignoreOnTakeDamage) {
			var event = {
				damage: amount,
				prevent: () => {
					dealDamage = false;
				}
			};
			this.onTakeDamageEvent.notify(event);
			amount = event.damage;
		}
		
		if (dealDamage) {
			this.health -= (amount > this.health ? this.health : amount);

			if (this.isAlive()) {
				this.onDamageTakenEvent.notify();
			} else {
				this.simulation.log(`${this.getName()} has been defeated.`);
			}
		}
	}

	heal(amount) {
		if (amount < 0) {
			throw new Error(`Must heal 0 or more health, attempted to heal: ${amount}`);
		}
		if (amount === 0 || this.health >= this.maxHealth) {
			return;
		}

		this.health += amount;
		if (this.health > this.maxHealth) {
			this.health = this.maxHealth;
		}
	}

	expend(stat, amount) {
		switch (stat) {
			case "health": this.expendHealth(amount); break;
			case "psyche": this.expendPsyche(amount); break;
		}
	}

	expendHealth(amount) {
		if (amount === -1) {
			amount = this.health;
		}
		this.health -= amount;
		if (this.health < 0) {
			this.health = 0;
		}
	}

	expendPsyche(amount) {
		if (amount === -1) {
			amount = this.psyche;
		}
		this.psyche -= amount;
		if (this.psyche < 0) {
			this.psyche = 0;
		}
	}

	damagePsyche(amount) {
		this.psyche -= amount;
		if (this.psyche < 0) {
			this.psyche = 0;
		}
		this.simulation.log(`${this.getName()} loses ${amount} MP.`);
	}

	swapHealthPsyche() {
		var health = this.health;
		this.health = this.psyche;
		this.psyche = health;

		var maxHealth = this.maxHealth;
		this.maxHealth = this.maxPsyche;
		this.maxPsyche = maxHealth;
	}

	setOverride(override) {
		this.override = override;
		this.maxHealth = this.override.stats.health;
		this.maxPsyche = this.override.stats.psyche;

		if (this.health > this.maxHealth) {
			this.health = this.maxHealth;
		}
		if (this.psyche > this.maxPsyche) {
			this.psyche = this.maxPsyche;
		}
	}

	buffAttack(amount) {
		this.buffs.attack += amount;
		return Math.floor(this.stats.attack * amount);
	}

	buffDefense(amount) {
		this.buffs.defense += amount;
		return Math.floor(this.stats.defense * amount);
	}

	buffSpeed(amount) {
		this.buffs.speed += amount;
		return Math.floor(this.stats.speed * amount);
	}

	buffIntelligence(amount) {
		this.buffs.intelligence += amount;
		return Math.floor(this.stats.intelligence * amount);
	}

	debuffAttack(amount) {
		this.debuffs.attack += amount;
		return Math.floor(this.stats.attack * amount);
	}

	debuffDefense(amount) {
		this.debuffs.defense += amount;
		return Math.floor(this.stats.defense * amount);
	}

	debuffSpeed(amount) {
		this.debuffs.speed += amount;
		return Math.floor(this.stats.speed * amount);
	}

	debuffIntelligence(amount) {
		this.debuffs.intelligence += amount;
		return Math.floor(this.stats.intelligence * amount);
	}

	addStatus(status) {
		if (this.statuses.add(status)) {
			status.apply(this, this.simulation);
		}
	}

	removeStatus(status) {
		if (this.statuses.remove(status)) {
			status.remove(this, this.simulation);
		}
	}

	statusCount(tclass=Status) {
		return this.statuses.count(tclass);
	}

	hasStatus(status) {
		return this.statuses.has(status);
	}

	hasStatusClass(tclass) {
		return this.statuses.hasClass(tclass);
	}

	hasExactStatusClass(tclass) {
		return this.statuses.hasExactClass(tclass);
	}

	clearStatuses(tclass) {
		var statuses = this.statuses.getAllOfClass(tclass);
		for (var i = 0; i < statuses.length; i++) {
			this.removeStatus(statuses[i]);
		}
	}
}