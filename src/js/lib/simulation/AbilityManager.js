import Ability from "./abilities/Ability"
import ATK20Attack from "./abilities/ATK20Attack"
import ATK30OnEnemyDeath from "./abilities/ATK30OnEnemyDeath"
import SPD30OnEnemyDeath from "./abilities/SPD30OnEnemyDeath"
import ATK50NearDeath from "./abilities/ATK50NearDeath"
import ClearsBuffsOnEnemy from "./abilities/ClearsBuffsOnEnemy"
import ClearsDebuffsOnSelf from "./abilities/ClearsDebuffsOnSelf"
import CounterAttack from "./abilities/CounterAttack"
import CriticalHit from "./abilities/CriticalHit"
import DoubleAttack from "./abilities/DoubleAttack"
import DoublesEnemyPsycheCost from "./abilities/DoublesEnemyPsycheCost"
import EvadesAttack from "./abilities/EvadesAttack"
import FullRecovery from "./abilities/FullRecovery"
import HealsOnAttack from "./abilities/HealsOnAttack"
import HPRecovery from "./abilities/HPRecovery"
import InitialBlock from "./abilities/InitialBlock"
import InitialINTAttack from "./abilities/InitialINTAttack"
import InitialPhysicalAttack from "./abilities/InitialPhysicalAttack"
import InitialPsychicAttack from "./abilities/InitialPsychicAttack"
import PoisonAttack from "./abilities/PoisonAttack"
import PsycheDamageOnDeath from "./abilities/PsycheDamageOnDeath"
import RandomStat30 from "./abilities/RandomStat30"
import RestoresHP from "./abilities/RestoresHP"
import SelfDestruct from "./abilities/SelfDestruct"
import StrainX2 from "./abilities/StrainX2"
import SwitchHealthAndPsyche from "./abilities/SwitchHealthAndPsyche"
import TurnIntoSheep from "./abilities/TurnIntoSheep"
import TurnsEnemyIntoChick from "./abilities/TurnsEnemyIntoChick"
import WithstandsAttack from "./abilities/WithstandsAttack"
import createBuffAbility from "./abilities/createBuffAbility"
import createDebuffAbility from "./abilities/createDebuffAbility"
import createCharmerAbility from "./abilities/createCharmerAbility"
import createElementalAbility from "./abilities/createElementalAbility"
import createPhysicalAbility from "./abilities/createPhysicalAbility"

import UnknownAbility from "./abilities/UnknownAbility"

import DataCollection from "collections/DataCollection"
import Dictionary from "collections/dict"
import Strains from "dccards/Strains"

import Options from "./Options"

var abilityClasses = (function() {
	var abilities = new Dictionary([], key => Ability);

	function addAbilities(abilityClasses) {
		for (var i = 0; i < abilityClasses.length; i++) {
			abilities.set(abilityClasses[i].getEffect(), abilityClasses[i]);
		}
	} 

	addAbilities([
		createPhysicalAbility(4, "Health for damage", false),

		createPhysicalAbility(1, "Strike"),
		createPhysicalAbility(2, "Strike"),
		createPhysicalAbility(3, "Strike"),
		createPhysicalAbility(4, "Strike"),

		createPhysicalAbility(1, "Scratch"),
		createPhysicalAbility(2, "Scratch"),
		createPhysicalAbility(3, "Scratch"),
		createPhysicalAbility(4, "Scratch"),

		createElementalAbility(1, Strains.burner),
		createElementalAbility(2, Strains.burner),
		createElementalAbility(3, Strains.burner),
		createElementalAbility(4, Strains.burner),

		createElementalAbility(1, Strains.chiller),
		createElementalAbility(2, Strains.chiller),
		createElementalAbility(3, Strains.chiller),
		createElementalAbility(4, Strains.chiller),

		createElementalAbility(1, Strains.slasher),
		createElementalAbility(2, Strains.slasher),
		createElementalAbility(3, Strains.slasher),
		createElementalAbility(4, Strains.slasher),

		createElementalAbility(1, Strains.shocker),
		createElementalAbility(2, Strains.shocker),
		createElementalAbility(3, Strains.shocker),
		createElementalAbility(4, Strains.shocker),

		createElementalAbility(1, Strains.screamer),
		createElementalAbility(2, Strains.screamer),
		createElementalAbility(3, Strains.screamer),
		createElementalAbility(4, Strains.screamer),

		createElementalAbility(1, Strains.spitter),
		createElementalAbility(2, Strains.spitter),
		createElementalAbility(3, Strains.spitter),
		createElementalAbility(4, Strains.spitter),

		createElementalAbility(1, Strains.leecher),
		createElementalAbility(2, Strains.leecher),
		createElementalAbility(3, Strains.leecher),
		createElementalAbility(4, Strains.leecher),

		createCharmerAbility(1),
		createCharmerAbility(2),
		createCharmerAbility(3),
		createCharmerAbility(4),

		createBuffAbility("ATK +10%", { attack: 0.10 }),
		createBuffAbility("ATK +20%", { attack: 0.20 }),
		createBuffAbility("ATK +25%", { attack: 0.25 }),

		createBuffAbility("DEF +10%", { defense: 0.10 }),
		createBuffAbility("DEF +20%", { defense: 0.20 }),
		createBuffAbility("DEF +25%", { defense: 0.25 }),

		createBuffAbility("SPD +10%", { speed: 0.10 }),
		createBuffAbility("SPD +20%", { speed: 0.20 }),
		createBuffAbility("SPD +25%", { speed: 0.25 }),

		createBuffAbility("INT +10%", { intelligence: 0.10 }),
		createBuffAbility("INT +20%", { intelligence: 0.20 }),
		createBuffAbility("INT +25%", { intelligence: 0.25 }),

		createDebuffAbility("ATK -10%", { attack: 0.10 }),
		createDebuffAbility("ATK -20%", { attack: 0.20 }),
		createDebuffAbility("ATK -25%", { attack: 0.25 }),

		createDebuffAbility("DEF -10%", { defense: 0.10 }),
		createDebuffAbility("DEF -20%", { defense: 0.20 }),
		createDebuffAbility("DEF -25%", { defense: 0.25 }),

		createDebuffAbility("SPD -10%", { speed: 0.10 }),
		createDebuffAbility("SPD -20%", { speed: 0.20 }),
		createDebuffAbility("SPD -25%", { speed: 0.25 }),

		createDebuffAbility("INT -10%", { intelligence: 0.10 }),
		createDebuffAbility("INT -20%", { intelligence: 0.20 }),
		createDebuffAbility("INT -25%", { intelligence: 0.25 }),

		createBuffAbility("ATK/DEF +10%", { attack: 0.1,  defense: 0.1 }),
		createBuffAbility("ATK/SPD +10%", { attack: 0.1,  speed: 0.1 }),
		createBuffAbility("ATK/INT +10%", { attack: 0.1,  intelligence: 0.1 }),
		createBuffAbility("DEF/INT +10%", { defense: 0.1, intelligence: 0.1 }),
		createBuffAbility("DEF/SPD +10%", { defense: 0.1, speed: 0.1 }),
		createBuffAbility("SPD/INT +10%", { speed: 0.1,   intelligence: 0.1 }),

		createDebuffAbility("ATK/DEF -10%", { attack: 0.1,  defense: 0.1 }),
		createDebuffAbility("ATK/SPD -10%", { attack: 0.1,  speed: 0.1 }),
		createDebuffAbility("ATK/INT -10%", { attack: 0.1,  intelligence: 0.1 }),
		createDebuffAbility("DEF/INT -10%", { defense: 0.1, intelligence: 0.1 }),
		createDebuffAbility("DEF/SPD -10%", { defense: 0.1, speed: 0.1 }),
		createDebuffAbility("SPD/INT -10%", { speed: 0.1,   intelligence: 0.1 }),

		createDebuffAbility("Attacks on death", { attack: 0.15, defense: 0.15, speed: 0.15, intelligence: 0.15 }),

		ATK20Attack,
		ATK30OnEnemyDeath,
		SPD30OnEnemyDeath,
		ATK50NearDeath,
		ClearsDebuffsOnSelf,
		ClearsBuffsOnEnemy,
		CounterAttack,
		CriticalHit,
		DoubleAttack,
		DoublesEnemyPsycheCost,
		EvadesAttack,
		FullRecovery,
		HealsOnAttack,
		HPRecovery,
		InitialBlock,
		InitialINTAttack,
		InitialPhysicalAttack,
		InitialPsychicAttack,
		PoisonAttack,
		PsycheDamageOnDeath,
		RandomStat30,
		RestoresHP,
		SelfDestruct,
		StrainX2,
		SwitchHealthAndPsyche,
		TurnIntoSheep,
		TurnsEnemyIntoChick,
		WithstandsAttack,
	]);

	return abilities;
})();

var unknownAbility = new UnknownAbility();

export default class extends DataCollection {
	static create(abilities) {
		for (var i = 0; i < abilities.length; i++) {
			var ability = abilities[i];
			var abilityClass = abilityClasses.get(ability.getEffect());
			abilities[i] = new abilityClass(ability);
		}
		return new this(abilities);
	}

	constructor(values) {
		super(values);
	}

	findByName(name) {
		if (name === null) {
			return unknownAbility;
		}
		return this.dictionary.get(name, unknownAbility);
	}

	getOptions() {
		var options = new Options();
		var abilities = this.toArray();
		for (var i = 0; i < abilities.length; i++) {
			abilities[i].addOptions(options);
		}
		return options;
	}
}