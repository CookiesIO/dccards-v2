import Card from "./Card"
import CardData from "dccards/Card"
import Types from "dccards/Types"
import Strains from "dccards/Strains"
import Rarities from "dccards/Rarities"

var ATBMax = 10000;
var dummyCard = new CardData({
	completed: true,
	name: "Joe",
	description: null,
	catalogNumber: null,
	cardNumber: null,
	spawnArea: null,
	shape: null,

	strain: Strains.voider, 
	rarity: Rarities.legendary, 

	maxLevel: 60, 
	redeathable: true, 
	tradeable: false, 
	stats: {
		healthMin: 9999,
		healthMax: 99999,
		psycheMin: 9999,
		psycheMax: 99999,
		attackMin: 1,
		attackMax: 1,
		defenseMin: 1,
		defenseMax: 1,
		speedMin: 1,
		speedMax: 1,
		intelligenceMin: 1,
		intelligenceMax: 1
	},
	types: [
		Types.average
	],
	skillset: {
		level01: null,
		level15: null,
		level30: null,
		level40: null,
		level50: null,
		redeath: null,
	},
	builds: {
		average: [],
		redeath: {
			average: []
		}
	}
});

export default class {
	constructor() {
		this.attackers = [];
		this.defenders = [];

		this.newAttacker = false;
		this.newDefender = false;
		this.attackerIndex = 0;
		this.defenderIndex = 0;
		this.attacker = null;
		this.defender = null;

		this.attackerATB = 0;
		this.defenderATB = 0;
		this.attackerSpeed = 0;
		this.defenderSpeed = 0;

		this.turnCount = 0;
		this.logMessages = [];
		this.inSimulation = false;
		this.winnerDecided = false;
		this.didWin = false;
	}

	isInSimulation() {
		return this.inSimulation;
	}

	didDecideWinner() {
		return this.winnerDecided;
	}

	didAttackerWin() {
		if (this.winnerDecided) {
			return this.didWin;
		}
		return false;
	}

	didDefenderWin() {
		if (this.winnerDecided) {
			return !this.didWin;
		}
		return false;
	}

	createAttackerCard(card, type, redeathed, level, boosts, build, options) {
		if (this.inSimulation) {
			return null;
		}
		var simulationCard = new Card(this, card, type, redeathed, level, boosts, build, options);
		this.attackers.push(simulationCard);
		return simulationCard;
	}

	createDefenderCard(card, type, redeathed, level, boosts, build, options) {
		if (this.inSimulation) {
			return null;
		}
		var simulationCard = new Card(this, card, type, redeathed, level, boosts, build, options);
		this.defenders.push(simulationCard);
		return simulationCard;
	}

	createAttackerDummyCard(build) {
		return this.createAttackerCard(dummyCard, Types.average, true, 70, true, build);
	}

	createDefenderDummyCard(build) {
		return this.createDefenderCard(dummyCard, Types.average, true, 70, true, build);
	}

	forceAttackerFirstAttack() {
		this.attackerATB = ATBMax;
	}

	forceDefenderFirstAttack() {
		this.defenderATB = ATBMax;
	}

	nextAttackerCard() {
		if (this.inSimulation) {
			this.attackerIndex++;
			if (this.attackerIndex < this.attackers.length) {
				this.attacker = this.attackers[this.attackerIndex];
				this.newAttacker = true;
				return true;
			}
			else {
				this.attacker = null;
			}
		}
		return false;
	}

	nextDefenderCard() {
		if (this.inSimulation) {
			this.defenderIndex++;
			if (this.defenderIndex < this.defenders.length) {
				this.defender = this.defenders[this.defenderIndex];
				this.newDefender = true;
				return true;
			}
			else {
				this.defender = null;
			}
		}
		return false;
	}

	removeAttacker(card) {
		if (!this.inSimulation) {
			for (var i = 0; i < this.attackers.length; i++) {
				if (this.attackers[i] === card) {
					this.attackers.splice(i, 1);
					return true;
				}
			}
		}
		return false;
	}

	removeDefender(card) {
		if (!this.inSimulation) {
			for (var i = 0; i < this.defenders.length; i++) {
				if (this.defenders[i] === card) {
					this.defenders.splice(i, 1);
					return true;
				}
			}
		}
		return false;
	}

	clearAttackers() {
		if (this.inSimulation) {
			return false;
		}
		this.attackers = [];
		return true;
	}

	clearDefenders() {
		if (this.inSimulation) {
			return false;
		}
		this.defenders = [];
		return true;
	}

	getAttacker() {
		return this.attacker;
	}

	getDefender() {
		return this.defender;
	}

	start() {
		this.winnerDecided = false;
		this.didWin = false;

		if (this.attackers.length === 0 || this.defenders.length === 0) {
			this.inSimulation = false;
			return false;
		}

		this.turnCount = 0;
		this.inSimulation = true;
		this.logMessages = [];

		for (var i = 0; i < this.attackers.length; i++) {
			this.attackers[i].reset();
		}
		for (var i = 0; i < this.defenders.length; i++) {
			this.defenders[i].reset();
		}

		this.attackerATB = 0;
		this.defenderATB = 0;
		this.attackerSpeed = 0;
		this.defenderSpeed = 0;

		this.attackerIndex = 0;
		this.defenderIndex = 0;

		this.attacker = this.attackers[0];
		this.defender = this.defenders[0];

		this.newAttacker = true;
		this.newDefender = true;
		return true;
	}

	simulateRound() {
		if (!this.inSimulation) {
			return false;
		}
		if (this.attacker === null || this.defender === null) {
			return false;
		}

		// Enter, Buff and Clear Buff happens before any ATB is calculated
		// It's the Attackers first, speed matters not 
		if (this.newAttacker) {
			this.attacker.enter();
		}
		if (this.newDefender) {
			this.defender.enter();
		}

		if (this.newAttacker) {
			this.attacker.buff();
		}
		if (this.newDefender) {
			this.defender.buff();
		}

		this.attacker.clearBuff();
		this.defender.clearBuff();

		// Fixes issue #1
		if (this.newAttacker) {
			this.attackerSpeed = this.attacker.getSpeed();
		}
		if (this.newDefender) {
			this.defenderSpeed = this.defender.getSpeed();
		}

		// Calculate ATB
		var attackerSpeed = this.attackerSpeed; //this.attacker.getSpeed(); // Fixes issue #1
		var defenderSpeed = this.defenderSpeed; //this.defender.getSpeed(); // Fixes issue #1

		var attackerInterval = (ATBMax - this.attackerATB) / attackerSpeed;
		var defenderInterval = (ATBMax - this.defenderATB) / defenderSpeed;

		var realAttacker;
		var realDefender;
		var newRealAttacker;
		var newRealDefender;
		
		// Lowest Interval is the real attacker, the attacker has precendence
		if (attackerInterval <= defenderInterval) {
			this.defenderATB += attackerInterval * defenderSpeed;
			this.attackerATB = 0;
			realAttacker = this.attacker;
			realDefender = this.defender;
			newRealAttacker = this.newAttacker;
			newRealDefender = this.newDefender;

			// Fixes issue #1
			this.defenderSpeed = this.defender.getSpeed();
		}
		else {
			this.attackerATB += defenderInterval * attackerSpeed;
			this.defenderATB = 0;
			realAttacker = this.defender;
			realDefender = this.attacker;
			newRealAttacker = this.newDefender;
			newRealDefender = this.newAttacker;

			// Fixes issue #1
			this.attackerSpeed = this.attacker.getSpeed();
		}

		if (newRealAttacker) {
			realAttacker.initialAttack();
		}

		// Only continue if realDefender is alive
		if (realDefender.isAlive()) {
			if (newRealDefender) {
				realDefender.initialAttack();
			}

			// And only continue if realAttacker is alive
			if (realAttacker.isAlive()) {
				realAttacker.attack();
				realAttacker.endTurn();
			}
		}

		// Neither Attacker nor Defender are new anymore
		this.newAttacker = false;
		this.newDefender = false;

		// Switch out cards if neccesary, also check if either side won
		if (this.attacker.isDead()) {
			this.nextAttackerCard();
			this.defenderATB = 0;
		}
		if (this.defender.isDead()) {
			this.nextDefenderCard();
			this.attackerATB = 0;
		}

		this.turnCount++;

		// Log whether you won or lost, if neccesary
		// null means there weren't cards to switch out with
		if (this.defender === null) {
			this.setWin(true);
		}
		else if (this.attacker === null) {
			this.setWin(false);
		}
		else if (this.turnCount === 200) {
			this.setWin(null)
		}
		return this.inSimulation;
	}

	simulate() {
		if (!this.inSimulation) {
			return false;
		}
		if (this.attacker === null || this.defender === null) {
			return false;
		}

		while (this.simulateRound()) {
		}

		return true;
	}

	setWin(win) {
		if (this.inSimulation) {
			if (win === true) {
				this.log("You win!");
			}
			else if (win === false) {
				this.log("You lost.");
			}
			else {
				this.log("You run for your life.");
			}
			this.inSimulation = false;
			this.winnerDecided = true;
			this.didWin = win === true;
		}
	}

	opponent(card) {
		if (this.inSimulation) {
			if (card !== null) {
				if (card === this.attacker) {
					return this.defender;
				}
				if (card === this.defender) {
					return this.attacker;
				}
			}
		}
		return null;
	}

	log(message) {
		if (this.isInSimulation) {
			this.logMessages.push(message);
		}
	}

	getLogText() {
		return this.logMessages.join("\n");
	}

	getLogArray() {
		return this.logMessages.slice();
	}
}