import Ability from "./Ability"
import Status from "simulation/statuses/Status"

export default class extends Ability {
	static getEffect() {
		return "Clears buffs on enemy";
	}

	canUse(card, simulation) {
		var opponent = simulation.opponent(card);
		return super.canUse(card, simulation)
			&& opponent.hasStatusClass(Status.buff());
	}

	apply(card, simulation, event) {
		var opponent = simulation.opponent(card);
		opponent.clearStatuses(Status.buff());
		event.unregister();
	}
}