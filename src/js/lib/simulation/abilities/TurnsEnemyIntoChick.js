import OffensiveAbility from "./OffensiveAbility"

export default class extends OffensiveAbility {
	static getEffect() {
		return "Turns enemy into chick";
	}

	damage(attacker, defender, simulation) {
		var morph = attacker.getOption("morph");
		if (morph) {
			simulation.log(`${defender.getName()} has morphed into a chick.`);
			defender.setOverride({
				name: "Chick",
				stats: {
					health: 460,
					psyche: 460,
					attack: 460,
					defense: 460,
					speed: 460,
					intelligence: 440,
				}
			});
		}
		else {
			simulation.log(`No effect.`);
		}
	}

	addOptions(options) {
		options.add({
			type: "Boolean",
			name: "morph",
			display: "Avian Flu/Sheepify",
			relevantFor: [
				"Attacker",
				"Defender"
			],
			defaultValue: false,
			chance: 0.3
		});
		super.addOptions(options);
	}
}