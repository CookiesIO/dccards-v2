import Ability from "./Ability"
import AttackBuffStatus from "simulation/statuses/AttackBuffStatus"
import DefenseBuffStatus from "simulation/statuses/DefenseBuffStatus"
import SpeedBuffStatus from "simulation/statuses/SpeedBuffStatus"
import IntelligenceBuffStatus from "simulation/statuses/IntelligenceBuffStatus"

export default class extends Ability {
	static getEffect() {
		return "Random stat +30%";
	}

	apply(card, simulation) {
		var stat = card.getOption("r30-stat");
		var buffStatusClass = this.getBuffStatusClass(stat);
		if (buffStatusClass !== null) {
			card.addStatus(new buffStatusClass(0.3));
		}
		else {
			//simulation.logError(this, `The stat '${stat}' does either not exist or does not have a buff`);
		}
	}

	getBuffStatusClass(stat) {
		switch (stat) {
			case "attack": return AttackBuffStatus;
			case "defense": return DefenseBuffStatus;
			case "speed": return SpeedBuffStatus;
			case "intelligence": return IntelligenceBuffStatus;
		}
		return null;
	}

	addOptions(options) {
		options.add({
			type: "Select",
			name: "r30-stat",
			display: "R30-Cell Metastatis",
			relevantFor: [
				"Attacker",
				"Defender"
			],
			defaultValue: "speed",
			values: [
				{
					value: "attack",
					display: "Attack"
				},
				{
					value: "defense",
					display: "Defense"
				},
				{
					value: "speed",
					display: "Speed"
				},
				{
					value: "intelligence",
					display: "Intelligence"
				}
			],
			filterDisplay: (displayValue, relevancy) => {
				if (relevancy !== null) {
					if (displayValue.value === "speed") {
						return false;
					}
					if (relevancy === "Attacker" && displayValue.value === "defense") {
						displayValue.display = "Speed/Defense (doesn't matter)";
						displayValue.value = "speed";
					}
					else if (relevancy === "Defender" && displayValue.value === "attack") {
						displayValue.display = "Speed/Attack (doesn't matter)";
						displayValue.value = "speed";
					}
				}
				return true;
			}
		});
		super.addOptions(options);
	}
}