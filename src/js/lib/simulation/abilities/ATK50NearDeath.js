import Ability from "./Ability"
import AttackBuffStatus from "simulation/statuses/AttackBuffStatus"

export default class extends Ability {
	static getEffect() {
		return "ATK +50% near death";
	}

	canUse(card, simulation) {
		return super.canUse(card, simulation)
			&& card.isAlive()
			&& card.getHealthRatio() <= 0.35;
	}

	apply(card, simulation, event) {
		card.addStatus(new AttackBuffStatus(0.5));
		event.unregister();
	}
}