import OffensiveAttackAbility from "./OffensiveAttackAbility"

export default class extends OffensiveAttackAbility {
	static getEffect() {
		return "Double attack";
	}

	apply(card, simulation) {
		super.apply(card, simulation);
		super.apply(card, simulation);
	}

	getPotentialDamage(attacker, defender) {
		return this.getDamage(attacker, defender) * 2;
	}

	getDamage(attacker, defender) {
		return OffensiveAttackAbility.calculatePhysicalDamage(attacker, 0, defender);
	}
}