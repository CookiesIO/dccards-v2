import Ability from "./Ability"

export default class extends Ability {
	static getEffect() {
		return "Psyche damage on death";
	}

	apply(card, simulation, event) {
		var opponent = simulation.opponent(card);
		var psycheDamage = card.getPsyche();
		opponent.damagePsyche(psycheDamage);
		event.unregister();
		event.stop();
	}
}