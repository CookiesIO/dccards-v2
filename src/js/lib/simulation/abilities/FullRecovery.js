import Ability from "./Ability"

export default class extends Ability {
	static getEffect() {
		return "Full recovery";
	}

	apply(card, simulation, event) {
		var revive = card.getOption("full recovery");
		if (revive) {
			card.heal(card.getMaxHealth());
			simulation.log(`${card.getName()} recovers.`);
			event.stop();
		}
		event.unregister();
	}

	addOptions(options) {
		options.add({
			type: "Boolean",
			name: "full recovery",
			display: "RE-Cell Revitalization",
			relevantFor: [
				"Defender"
			],
			defaultValue: false,
			chance: 0.5
		});
		super.addOptions(options);
	}
}