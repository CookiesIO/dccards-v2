import OffensiveAttackAbility from "./OffensiveAttackAbility"
import PoisonStatus from "simulation/statuses/PoisonStatus"

var modifier = OffensiveAttackAbility.getModifier(4);
export default class extends OffensiveAttackAbility {
	static getEffect() {
		return "Poison attack";
	}

	damage(attacker, defender, simulation) {
		super.damage(attacker, defender, simulation);
		if (defender.isAlive()) {
			var poison = attacker.getOption("poison");
			if (poison) {
				defender.addStatus(new PoisonStatus());
			}
		}
	}

	getDamage(attacker, defender) {
		return OffensiveAttackAbility.calculatePhysicalDamage(attacker, modifier, defender);
	}

	addOptions(options) {
		options.add({
			type: "Boolean",
			name: "poison",
			display: "Poison (Venom Strike)",
			relevantFor: [
				// Neither Attacker nor Defender
			],
			defaultValue: false,
			chance: 0.5
		});
		super.addOptions(options);
	}
}