import OffensiveAbility from "./OffensiveAbility"

export default class extends OffensiveAbility {
	static getEffect() {
		return "Initial INT attack";
	}

	getDamage(attacker, defender) {
		return OffensiveAbility.calculateElementalDamage(attacker, -0.2, 1, defender);
	}
}