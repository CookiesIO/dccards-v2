import Ability from "./Ability"
import Status from "simulation/statuses/Status"

export default class extends Ability {
	static getEffect() {
		return "Clears debuffs on self";
	}

	canUse(card, simulation) {
		return super.canUse(card, simulation)
			&& card.hasStatusClass(Status.debuff());
	}

	apply(card, simulation, event) {
		card.clearStatuses(Status.debuff());
		event.unregister();
	}
}