import Ability from "./Ability"

export default class extends Ability {
	static getEffect() {
		return "Self-destruct";
	}

	apply(card, simulation, event) {
		event.unregister();

		var selfDestruct = card.getOption("self-destruct");
		if (selfDestruct) {
			var opponent = simulation.opponent(card);
			var damage = card.getPsyche();
			opponent.damage(damage, true);
			event.stop();
			return true;
		}
		else {
			simulation.log(`No effect.`);
			return false;
		}
	}

	addOptions(options) {
		options.add({
			type: "Boolean",
			name: "self-destruct",
			display: "Self-destruct",
			relevantFor: [
			],
			defaultValue: false,
			chance: 0.5
		});
		super.addOptions(options);
	}
}