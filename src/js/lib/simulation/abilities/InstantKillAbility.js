import OffensiveAttackAbility from "./OffensiveAttackAbility"

export default class extends OffensiveAttackAbility {
	tryInstantlyKill(attacker, defender, simulation) {
		if (this.instantlyKills(attacker)) {
			defender.kill();
			return true;
		}
		return false;
	}

	instantlyKills(attacker) {
		return attacker.getOption("instant kill");
	}

	getInstantKillRate(attacker, defender) {
		return 0;
	}

	calculateInstantKillRate(attack, defense, maxChance) {
		// This is just a guess, don't think of it as being absolute.
		return Math.min((attack / defense), 1) * maxChance;
	}

	addOptions(options) {
		options.add({
			type: "Boolean",
			name: "instant kill",
			display: "Instant Kill",
			tooltip: "Turns on proc for Intant Kill abilities like Charmer and Critical Hit",
			relevantFor: [
				"Attacker"
			],
			defaultValue: false,
			chance: (card, simulation) => {
				var opponent = simulation.opponent(card);
				return this.getInstantKillRate(card, opponent);
			}
		});
		super.addOptions(options);
	}
}