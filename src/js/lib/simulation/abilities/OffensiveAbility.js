import Ability from "./Ability"

export default class extends Ability {
	static calculateDamage(attack, defense) {
		return Math.max(Math.floor(attack - (defense * 0.5)), 1);
	}

	static calculatePhysicalDamage(attacker, modifier, defender) {
		var attack = attacker.getAttack(modifier);
		var defense = defender.getDefense();

		return this.calculateDamage(attack, defense, 1);
	}

	static calculateElementalDamage(attacker, modifier, elementalModifier, defender) {
		var attack = attacker.getIntelligence(modifier) * elementalModifier;
		var defense = defender.getIntelligence();

		return this.calculateDamage(attack, defense);
	}

	static getModifier(level) {
		return (1 / Math.pow(2, (5 - level)));
	}

	apply(card, simulation) {
		var opponent = simulation.opponent(card);
		opponent.strike(this, () => {
			this.damage(card, opponent, simulation);
		});
	}

	damage(attacker, defender, simulation) {
		var damage = this.getDamage(attacker, defender);
		defender.damage(damage);
	}

	getPotentialDamage(attacker, defender) {
		return this.getDamage(attacker, defender);
	}

	getDamage(attacker, defender) {
		return 0;
	}
}