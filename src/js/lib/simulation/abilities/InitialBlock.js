import Ability from "./Ability"
import Status from "simulation/statuses/Status"
import Events from "simulation/Events"

export class InitialBlockStatus extends Status.buff() {
	static getName() {
		return "Initial Block";
	}

	constructor() {
		super();
		this.unregister = null;
	}

	apply(card, simulation) {
		this.unregister = card.onStruck((event) => this.onStruck(event, card, simulation), 1);
	}

	onStruck(event, card, simulation) {
		if (event.ability.getEvent() === Events.initialAttack) {
			simulation.log(`Initial attack blocked!`);
			event.preventStop();
		}
	}

	remove(card, simulation) {
		this.unregister();
	}
}

export default class extends Ability {
	static getEffect() {
		return "Initial block";
	}

	apply(card, simulation) {
		card.addStatus(new InitialBlockStatus());
	}
}