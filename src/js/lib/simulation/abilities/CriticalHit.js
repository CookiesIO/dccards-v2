import InstantKillAbility from "./InstantKillAbility"

var modifier = InstantKillAbility.getModifier(2);
export default class extends InstantKillAbility {
	static getEffect() {
		return "Critical hit";
	}

	damage(attacker, defender, simulation) {
		if (!this.tryInstantlyKill(attacker, defender, simulation)) {
			super.damage(attacker, defender, simulation);
		}
	}

	getInstantKillRate(attacker, defender) {
		var attack = attacker.getAttack();
		var defense = defender.getDefense();

		return this.calculateInstantKillRate(attack, defense, 0.85);
	}

	getPotentialDamage(attacker, defender) {
		return defender.getHealth();
	}

	getDamage(attacker, defender) {
		return InstantKillAbility.calculatePhysicalDamage(attacker, modifier, defender);
	}

	addNotices(notices) {
		notices.add(`${this.getFormalName()} can Instantly Kill, if it fails it will do the same damage as a Physical +2`);
		notices.addForSimulation(`${this.getFormalName()} the exact formula for the chance of this ability being successful is unknown, the current estimate is 'Min(Attacker Attack / Defender Defense, 1) * 0.85`);
		super.addNotices(notices);
	}
}