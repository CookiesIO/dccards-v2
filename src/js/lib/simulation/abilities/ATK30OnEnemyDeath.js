import Ability from "./Ability"
import Status from "simulation/statuses/Status"
import AttackBuffStatus from "simulation/statuses/AttackBuffStatus"

export class ATK30KillStatus extends Status {
	static getName() {
		return "ATK +30% Kill";
	}

	static allowStacking() {
		return true;
	}
}

export default class extends Ability {
	static getEffect() {
		return "ATK +30% on enemy death";
	}

	canUse(card, simulation) {
		var opponent = simulation.opponent(card);
		return super.canUse(card, simulation)
			&& opponent.isDead()
			&& card.isAlive();
	}

	apply(card, simulation, event) {
		card.addStatus(new ATK30KillStatus());
		card.addStatus(new AttackBuffStatus(0.3));
		if (card.statusCount(ATK30KillStatus) === 2) {
			event.unregister();
		}
	}
}