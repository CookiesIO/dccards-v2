import Ability from "./Ability"

export default class extends Ability {
	static getEffect() {
		return "Switch health & psyche";
	}

	apply(card, simulation) {
		var opponent = simulation.opponent(card);
		opponent.strike(this, () => {
			opponent.swapHealthPsyche();
			simulation.log(`${opponent.getName()}'s HP and MP have been reversed.`);
		});
	}
}