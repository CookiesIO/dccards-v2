import OffensiveAttackAbility from "./OffensiveAttackAbility"

export default function(level, effect, format=true) {
	var modifier = OffensiveAttackAbility.getModifier(level);

	return class extends OffensiveAttackAbility {
		static getEffect() {
			if (format) {
				return `${effect} +${level}`;
			}
			return effect;
		}

		getDamage(attacker, defender) {
			return OffensiveAttackAbility.calculatePhysicalDamage(attacker, modifier, defender);
		}
	};
}