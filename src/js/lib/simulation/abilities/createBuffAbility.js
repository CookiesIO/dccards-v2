import Ability from "./Ability"
import AttackBuffStatus from "simulation/statuses/AttackBuffStatus"
import DefenseBuffStatus from "simulation/statuses/DefenseBuffStatus"
import SpeedBuffStatus from "simulation/statuses/SpeedBuffStatus"
import IntelligenceBuffStatus from "simulation/statuses/IntelligenceBuffStatus"

export default function(effect, buff) {
	return class extends Ability {
		static getEffect() {
			return effect;
		}

		apply(card, simulation) {
			if (buff.attack) {
				var status = new AttackBuffStatus(buff.attack);
				card.addStatus(status);
			}
			if (buff.defense) {
				var status = new DefenseBuffStatus(buff.defense);
				card.addStatus(status);
			}
			if (buff.speed) {
				var status = new SpeedBuffStatus(buff.speed);
				card.addStatus(status);
			}
			if (buff.intelligence) {
				var status = new IntelligenceBuffStatus(buff.intelligence);
				card.addStatus(status);
			}
		}
	}
}