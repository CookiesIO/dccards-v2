import PressureStatus from "simulation/statuses/PressureStatus"
import Events from "simulation/Events"

export default class {
	static getEffect() {
		throw new Error("Implement static getEffectName damnit: " + this);
	}

	constructor(ability) {
		this.ability = ability;
	}

	getName(card=null) {
		return this.ability.getName();
	}

	getEffect(card=null) {
		return this.ability.getEffect();
	}

	getFormalName(card=null) {
		return `${this.getName(card)} (${this.getEffect(card)})`;
	}

	getEvent() {
		return this.ability.getEvent();
	}

	getPriority() {
		return this.ability.getPriority();
	}

	getCost() {
		return this.ability.getCost();
	}

	getCostStat() {
		return this.ability.getCostStat();
	}

	getMinimumCost(pressured=false) {
		var cost = this.getCost();
		if (cost === null) {
			return 0;
		}
		if (cost === -1) {
			return 1;
		}
		if (pressured) {
			return cost * 2;
		}
		return cost;
	}

	getCostText() {
		var cost = this.getCost();
		if (cost === null) {
			return `Unknown ${this.getCostStat()} usage`;
		}
		if (cost === -1) {
			return `Uses all ${this.getCostStat()}`;
		}
		return `Uses ${cost} ${this.getCostStat()}`;
	}

	eggnog(card) { // Courtesy of Joe, for lack of better name (at the moment of writing)
		var pressured = card.hasExactStatusClass(PressureStatus);
		return this.getMinimumCost(pressured) <= card.getExpendableStat(this.getCostStat());
	}

	setup(card, simulation) {
		if (this.getEvent() !== Events.attack) {
			card.on(this.getEvent(), (event) => {
				this.use(card, simulation, event);
			}, this.getPriority());
		}
	}

	use(card, simulation, ...params) {
		if (this.canUse(card, simulation, ...params)) {
			this.logUse(card, simulation, ...params);
			var result = this.apply(card, simulation, ...params);
			if (result === undefined || result) {
				this.expend(card);
			}
			var opponent = simulation.opponent(card);
			opponent.enemyUsedAbility(this);
		}
	}

	canUse(card, simulation) {
		return this.eggnog(card);
	}

	logUse(card, simulation) {
		simulation.log(`${card.getName()} uses ${this.getName(card)}.`);
	}

	apply(card, simulation) {
		simulation.log(`${this.getFormalName(card)} does nothing. At all. Except expending psyche if the psyche cost is know.`);
	}

	expend(card) {
		var cost = this.getCost();
		var stat = this.getCostStat();
		if (cost === -1) {
			card.expend(stat, -1);
		}
		else if (cost !== null) {
			var pressured = card.hasExactStatusClass(PressureStatus);
			if (pressured) {
				cost *= 2;
			}
			card.expend(stat, cost);
		}
	}

	addNotices(notices) {
		//notices.add(`${this.getFormalName()} has not been implemented`);
		if (this.getCost() === null) {
			notices.add(`The cost of ${this.getFormalName()} is unknown.`);
		}
		if (this.getEvent() === Events.attack) {
			notices.add(`${this.getFormalName()} is set to be an Attack ability, but isn't implemented as such.`);
		}
	}

	addOptions(options) {

	}
}