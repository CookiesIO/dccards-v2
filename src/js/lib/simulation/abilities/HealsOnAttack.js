import OffensiveAttackAbility from "./OffensiveAttackAbility"

var modifier = OffensiveAttackAbility.getModifier(4);
export default class extends OffensiveAttackAbility {
	static getEffect() {
		return "Heals on attack";
	}

	damage(attacker, defender, simulation) {
		var damage = this.getDamage(attacker, defender);
		defender.damage(damage);

		var maxHealing = attacker.getMaxHealth() - attacker.getHealth();
		var healing = Math.floor(damage * 0.3);
		if (healing > maxHealing) {
			healing = maxHealing;
		}
		attacker.heal(healing);
		simulation.log(`${healing} HP absorbed from ${defender.getName()}.`);
	}

	getDamage(attacker, defender) {
		return OffensiveAttackAbility.calculatePhysicalDamage(attacker, modifier, defender);
	}
}