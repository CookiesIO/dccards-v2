import Ability from "./Ability"
import PressureStatus from "simulation/statuses/PressureStatus"

export default class extends Ability {
	static getEffect() {
		return "Doubles enemy psyche cost";
	}

	canUse(card, simulation) {
		var opponent = simulation.opponent(card);
		return super.canUse(card, simulation)
			&& !opponent.hasExactStatusClass(PressureStatus);
	}

	apply(card, simulation) {
		var opponent = simulation.opponent(card);
		opponent.addStatus(new PressureStatus());
	}
}