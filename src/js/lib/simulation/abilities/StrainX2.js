import OffensiveAttackAbility from "./OffensiveAttackAbility"
import Strains from "dccards/Strains"

export default class extends OffensiveAttackAbility {
	static getEffect() {
		return "Strain x2";
	}

	getEffect(card=null) {
		var superResult = super.getEffect(card);
		if (card !== null) {
			superResult = superResult.replace("Strain", card.getStrain().getName())
		}
		return superResult;
	}

	apply(card, simulation) {
		super.apply(card, simulation);
		super.apply(card, simulation);
	}

	getPotentialDamage(attacker, defender) {
		return this.getDamage(attacker, defender) * 2;
	}

	getDamage(attacker, defender) {
		var defenderStrain = defender.getStrain();
		var elementalModifier = attacker.getStrain().getElementalModifier(defenderStrain);	
		return OffensiveAttackAbility.calculateElementalDamage(attacker, 0, elementalModifier, defender);
	}
}