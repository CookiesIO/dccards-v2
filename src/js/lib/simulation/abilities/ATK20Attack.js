import OffensiveAttackAbility from "./OffensiveAttackAbility"
import AttackDebuffStatus from "simulation/statuses/AttackDebuffStatus"

var modifier = OffensiveAttackAbility.getModifier(4);
export default class extends OffensiveAttackAbility {
	static getEffect() {
		return "ATK -20% attack";
	}

	damage(attacker, defender, simulation) {
		super.damage(attacker, defender, simulation);
		defender.addStatus(new AttackDebuffStatus(0.2));
	}

	getDamage(attacker, defender) {
		return OffensiveAttackAbility.calculatePhysicalDamage(attacker, modifier, defender);
	}
}