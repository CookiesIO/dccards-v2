import Ability from "./Ability"

export default class extends Ability {
	static getEffect() {
		return "Restores HP";
	}

	apply(card, simulation, event) {
		var revive = card.getOption("cell regeneration");
		if (revive === true) {
			card.heal(Math.floor(card.getMaxHealth() * 0.5));
			simulation.log(`${card.getName()} recovers.`);
			event.stop();
		}
		event.unregister();
	}

	addOptions(options) {
		options.add({
			type: "Boolean",
			name: "cell regeneration",
			display: "Cell Regeneration",
			relevantFor: [
				"Defender"
			],
			defaultValue: false,
			chance: 0.5
		});
		super.addOptions(options);
	}
}