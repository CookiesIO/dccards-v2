import Ability from "./Ability"
import AttackDebuffStatus from "simulation/statuses/AttackDebuffStatus"
import DefenseDebuffStatus from "simulation/statuses/DefenseDebuffStatus"
import SpeedDebuffStatus from "simulation/statuses/SpeedDebuffStatus"
import IntelligenceDebuffStatus from "simulation/statuses/IntelligenceDebuffStatus"

export default function(effect, debuff) {
	return class extends Ability {
		static getEffect() {
			return effect;
		}

		apply(card, simulation) {
			var opponent = simulation.opponent(card);
			if (debuff.attack) {
				var status = new AttackDebuffStatus(debuff.attack);
				opponent.addStatus(status);
			}
			if (debuff.defense) {
				var status = new DefenseDebuffStatus(debuff.defense);
				opponent.addStatus(status);
			}
			if (debuff.speed) {
				var status = new SpeedDebuffStatus(debuff.speed);
				opponent.addStatus(status);
			}
			if (debuff.intelligence) {
				var status = new IntelligenceDebuffStatus(debuff.intelligence);
				opponent.addStatus(status);
			}
		}
	}
}