import OffensiveAbility from "./OffensiveAbility"

export default class extends OffensiveAbility {
	static getEffect() {
		return "Turn into sheep";
	}

	damage(attacker, defender, simulation) {
		var morph = attacker.getOption("morph");
		if (morph) {
			simulation.log(`${defender.getName()} has morphed into a sheep.`);
			defender.setOverride({
				name: "Sheep",
				stats: {
					health: 462,
					psyche: 516,
					attack: 514,
					defense: 465,
					speed: 451,
					intelligence: 502,
				}
			});
		}
		else {
			simulation.log(`No effect.`);
		}
	}

	addOptions(options) {
		options.add({
			type: "Boolean",
			name: "morph",
			display: "Avian Flu/Sheepify",
			relevantFor: [
				"Attacker",
				"Defender"
			],
			defaultValue: false,
			chance: 0.3
		});
		super.addOptions(options);
	}
}