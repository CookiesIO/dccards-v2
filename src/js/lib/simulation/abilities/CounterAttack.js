import Ability from "./Ability"
import OffensiveAbility from "./OffensiveAbility"

export default class extends Ability {
	static getEffect() {
		return "Counter attack";
	}

	canUse(card, simulation, event) {
		return super.canUse(card, simulation)
			&& event.ability instanceof OffensiveAbility
			&& event.ability.getDamage(simulation.opponent(card), card) > 0;
	}

	apply(card, simulation, event) {
		var opponent = simulation.opponent(card);
		var damage = event.ability.getDamage(opponent, card);
		damage = Math.floor(damage * 1.2);
		if (opponent.strike(this)) {
			opponent.damage(damage);
		}
	}
}