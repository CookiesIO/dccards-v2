import OffensiveAbility from "./OffensiveAbility"

export default class extends OffensiveAbility {
	static getEffect() {
		return "Initial physical attack";
	}

	getDamage(attacker, defender) {
		return OffensiveAbility.calculatePhysicalDamage(attacker, -0.2, defender);
	}
}