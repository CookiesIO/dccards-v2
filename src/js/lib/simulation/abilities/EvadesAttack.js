import Ability from "./Ability"
import OffensiveAbility from "./OffensiveAbility"
import Status from "simulation/statuses/Status"

export class EvadesAttackStatus extends Status {
	static getName() {
		return "Evades attack";
	}

	constructor() {
		super();
		this.unregister = null;
	}

	apply(card, simulation) {
		this.unregister = card.onStruck((event) => this.onStruck(event, card, simulation), 1);
	}

	onStruck(event, card, simulation) {
		if (event.ability instanceof OffensiveAbility) {
			if (card.getOption("evades attack")) {
				var opponent = simulation.opponent(card);
				simulation.log(`${opponent.getName()}'s attack is evaded!`);
				event.preventStop();
			}
			card.removeStatus(this);
		}
	}

	remove(card, simulation) {
		this.unregister();
	}
}

export default class extends Ability {
	static getEffect() {
		return "Evades attack";
	}

	apply(card, simulation) {
		card.addStatus(new EvadesAttackStatus());
	}

	addOptions(options) {
		options.add({
			type: "Boolean",
			name: "evades attack",
			display: "Blurred Speed",
			relevantFor: [
				"Attacker",
				"Defender"
			],
			defaultValue: false,
			chance: 0.4
		});
		super.addOptions(options);
	}
}