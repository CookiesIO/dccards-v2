import OffensiveAbility from "./OffensiveAbility"

export default class extends OffensiveAbility {
	static getEffect() {
		return "Initial psychic attack";
	}

	canUse(card, simulation) {
		var opponent = simulation.opponent(card);
		return super.canUse(card, simulation)
			&& opponent.getPsyche() > 1;
	}

	damage(attacker, defender, simulation) {
		var mindfuck = attacker.getOption("mindfuck");
		if (mindfuck) {
			var psycheDamage = defender.getPsyche() - 1;
			defender.damagePsyche(psycheDamage);
		}
		else {
			simulation.log(`No effect.`);
		}
	}

	addOptions(options) {
		options.add({
			type: "Boolean",
			name: "mindfuck",
			display: "Mindfuck",
			relevantFor: [
				"Attacker",
				"Defender"
			],
			defaultValue: false,
			chance: 0.5
		});
		super.addOptions(options);
	}
}