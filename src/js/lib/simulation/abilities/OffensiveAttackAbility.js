import OffensiveAbility from "./OffensiveAbility"

export default class extends OffensiveAbility {
	setup(card, simulation) {
		card.onAttack((event) => {
			if (this.canUse(card, simulation)) {
				var opponent = simulation.opponent(card);
				var damage = this.getPotentialDamage(card, opponent);

				if (event.ability === null || event.damage < damage) {
					event.ability = this;
					event.damage = damage;
					if (opponent.willDie(damage)) {
						event.stop();
					}
				}
			}
		});
	}
}