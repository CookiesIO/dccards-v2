import AbilityData from "dccards/Ability"
import Ability from "./Ability"

export default class extends Ability {
	static getEffect() {
		return "{Unknown}";
	}

	constructor() {
		super(new AbilityData({
			name: "Unknown",
			effect: "Magic",
			costStat: "psyche",
			cost: null,
			priority: NaN,
			event: "unknown"
		}));
	}

	addNotices(notices) {

	}
}