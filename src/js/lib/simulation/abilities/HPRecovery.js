import Ability from "./Ability"
import Status from "simulation/statuses/Status"
import Events from "simulation/Events"

export class HPRecoveryStatus extends Status.buff() {
	static getName() {
		return "HP Recovery Status";
	}

	constructor() {
		super();
		this.unregister = null;
	}

	apply(card, simulation) {
		this.unregister = card.onEnemyUsedAbility((event) => this.onEnemyUsedAbility(event, card, simulation), 1);
	}

	onEnemyUsedAbility(event, card, simulation) {
		if (card.isDead()) {
			return;
		}
		if (event.ability.getEvent() === Events.attack) {
			var healing = Math.floor(card.getMaxHealth() * 0.3);
			simulation.log(`${card.getName()} regenerates ${healing} HP.`);
			card.heal(healing);
		}
	}

	remove(card, simulation) {
		this.unregister();
	}
}

export default class extends Ability {
	static getEffect() {
		return "HP recovery";
	}

	apply(card, simulation) {
		card.addStatus(new HPRecoveryStatus());
	}
}