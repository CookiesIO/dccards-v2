import InstantKillAbility from "./InstantKillAbility"
import Strains from "dccards/Strains"

export default function(level) {
	var chance = (5 + (level)) / 10;
	var modifier = InstantKillAbility.getModifier(level);

	return class extends InstantKillAbility {
		static getEffect() {
			return `${Strains.charmer.getName()} +${level}`;
		}

		damage(attacker, defender, simulation) {
			if (!this.tryInstantlyKill(attacker, defender, simulation)) {
				simulation.log(`${defender.getName()} escapes it!`)
			}
		}

		getInstantKillRate(attacker, defender) {
			var defenderStrain = defender.getStrain();
			var elementalModifier = Strains.charmer.getElementalModifier(defenderStrain);

			var attack = attacker.getIntelligence() * elementalModifier;
			var defense = defender.getIntelligence();

			return this.calculateInstantKillRate(attack, defense, chance);
		}

		getDamage(attacker, defender) {
			var defenderStrain = defender.getStrain();
			var elementalModifier = Strains.charmer.getElementalModifier(defenderStrain);
			return InstantKillAbility.calculateElementalDamage(attacker, modifier, elementalModifier, defender);
		}

		addNotices(notices) {
			notices.add(`${this.getFormalName()} can Instantly Kill, if it fails it will not deal damage`);
			notices.addForSimulation(`${this.getFormalName()} the exact formula for the chance of this ability being successful is unknown, the current estimate is 'Min(Attacker Intelligence / Defender Intelligence, 1) * ${chance}`);
			super.addNotices(notices);
		}
	};
}