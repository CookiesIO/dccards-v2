import OffensiveAttackAbility from "./OffensiveAttackAbility"

export default function(level, strain) {
	var modifier = OffensiveAttackAbility.getModifier(level);

	return class extends OffensiveAttackAbility {
		static getEffect() {
			return `${strain.getName()} +${level}`;
		}

		getDamage(attacker, defender) {
			var defenderStrain = defender.getStrain();
			var elementalModifier = strain.getElementalModifier(defenderStrain);
			return OffensiveAttackAbility.calculateElementalDamage(attacker, modifier, elementalModifier, defender);
		}
	};
}