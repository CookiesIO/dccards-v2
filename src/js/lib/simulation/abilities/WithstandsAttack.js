import Ability from "./Ability"

export default class extends Ability {
	static getEffect() {
		return "Withstands attack";
	}

	canUse(card, simulation, event) {
		return super.canUse(card, simulation)
			&& card.willDie(event.damage)
			&& card.getHealthRatio() >= 0.1;
	}

	apply(card, simulation, event) {
		var withstand = card.getOption("withstand");
		if (withstand === true) {
			var opponent = simulation.opponent(card);
			simulation.log(`${opponent.getName()} barely survives.`);
			event.damage = card.getHealth() - 1;
			event.stop();
		}
		event.unregister();
	}

	addOptions(options) {
		options.add({
			type: "Boolean",
			name: "withstand",
			display: "Survival Instinct",
			relevantFor: [
				"Defender"
			],
			defaultValue: false,
			chance: 0.7
		});
		super.addOptions(options);
	}
}