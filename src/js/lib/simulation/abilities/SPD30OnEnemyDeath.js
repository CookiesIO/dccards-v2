import Ability from "./Ability"
import Status from "simulation/statuses/Status"
import SpeedBuffStatus from "simulation/statuses/SpeedBuffStatus"

export class SPD30KillStatus extends Status {
	static getName() {
		return "SPD +30% Kill";
	}

	static allowStacking() {
		return true;
	}
}

export default class extends Ability {
	static getEffect() {
		return "SPD +30% on enemy death";
	}

	canUse(card, simulation) {
		var opponent = simulation.opponent(card);
		return super.canUse(card, simulation)
			&& opponent.isDead()
			&& card.isAlive();
	}

	apply(card, simulation, event) {
		card.addStatus(new SPD30KillStatus());
		card.addStatus(new SpeedBuffStatus(0.3));
		if (card.statusCount(SPD30KillStatus) === 2) {
			event.unregister();
		}
	}
}