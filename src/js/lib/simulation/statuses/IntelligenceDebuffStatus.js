import Status from "./Status"

export default class extends Status.debuff() {
	static allowStacking() {
		return true;
	}

	static getName() {
		return "Intelligence Debuff";
	}

	constructor(amount) {
		super();
		this.amount = amount;
	}

	apply(card, simulation) {
		var amount = card.debuffIntelligence(this.amount);
		simulation.log(`${card.getName()}'s Intelligence decreases by ${amount}.`);
	}

	remove(card, simulation) {
		card.debuffIntelligence(-this.amount);
	}
}