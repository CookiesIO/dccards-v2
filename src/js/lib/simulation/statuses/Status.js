export default class Status {
	static buff() {
		return BuffStatus;
	}

	static debuff() {
		return DebuffStatus;
	}

	static allowStacking() {
		return false;
	}

	static getName() {
		throw new Error(`Implement static Status.getName in ${this}`);
	}

	constructor() {
	}

	getName() {
		return this.constructor.getName();
	}

	allowStacking() {
		return this.constructor.allowStacking();
	}

	apply(card, simulation) {
	}

	remove(card, simulation) {
	}
}

export class BuffStatus extends Status {

}

export class DebuffStatus extends Status {

}
