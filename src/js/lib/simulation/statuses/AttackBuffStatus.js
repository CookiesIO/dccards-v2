import Status from "./Status"

export default class extends Status.buff() {
	static allowStacking() {
		return true;
	}

	static getName() {
		return "Attack Buff";
	}

	constructor(amount) {
		super();
		this.amount = amount;
	}

	apply(card, simulation) {
		var amount = card.buffAttack(this.amount);
		simulation.log(`${card.getName()}'s Attack increases by ${amount}.`);
	}

	remove(card, simulation) {
		card.buffAttack(-this.amount);
	}
}