import Status from "./Status"

export default class extends Status {
	static getName() {
		return "Poison";
	}

	constructor() {
		super();
		this.unregister = null;
	}

	apply(card, simulation) {
		simulation.log(`${card.getName()} has been poisoned!`);
		this.unregister = card.onEndTurn(() => this.endTurn(card, simulation), 1);
	}

	endTurn(card, simulation) {
		card.strike(null, () => {
			var damage = Math.floor(card.getMaxHealth() * 0.15);
			card.damage(damage, true);
		});
	}

	remove(card, simulation) {
		this.unregister();
	}
}