import Status from "./Status"

export default class extends Status.debuff() {
	static getName() {
		return "Pressure";
	}

	constructor() {
		super(); 
	}

	apply(card, simulation) {
		simulation.log(`PSY costs are doubled.`);
	}
}