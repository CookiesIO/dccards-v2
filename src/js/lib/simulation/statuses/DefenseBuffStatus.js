import Status from "./Status"

export default class extends Status.buff() {
	static allowStacking() {
		return true;
	}

	static getName() {
		return "Defense Buff";
	}

	constructor(amount) {
		super();
		this.amount = amount;
	}

	apply(card, simulation) {
		var amount = card.buffDefense(this.amount);
		simulation.log(`${card.getName()}'s Defense increases by ${amount}.`);
	}

	remove(card, simulation) {
		card.buffDefense(-this.amount);
	}
}