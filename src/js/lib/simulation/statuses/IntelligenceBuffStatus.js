import Status from "./Status"

export default class extends Status.buff() {
	static allowStacking() {
		return true;
	}

	static getName() {
		return "Intelligence Buff";
	}

	constructor(amount) {
		super();
		this.amount = amount;
	}

	apply(card, simulation) {
		var amount = card.buffIntelligence(this.amount);
		simulation.log(`${card.getName()}'s Intelligence increases by ${amount}.`);
	}

	remove(card, simulation) {
		card.buffIntelligence(-this.amount);
	}
}