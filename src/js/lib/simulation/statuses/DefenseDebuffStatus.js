import Status from "./Status"

export default class extends Status.debuff() {
	static allowStacking() {
		return true;
	}

	static getName() {
		return "Defense Debuff";
	}

	constructor(amount) {
		super();
		this.amount = amount;
	}

	apply(card, simulation) {
		var amount = card.debuffDefense(this.amount);
		simulation.log(`${card.getName()}'s Defense decreases by ${amount}.`);
	}

	remove(card, simulation) {
		card.debuffDefense(-this.amount);
	}
}