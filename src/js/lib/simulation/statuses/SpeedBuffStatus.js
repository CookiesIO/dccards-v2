import Status from "./Status"

export default class extends Status.buff() {
	static allowStacking() {
		return true;
	}

	static getName() {
		return "Speed Buff";
	}

	constructor(amount) {
		super();
		this.amount = amount;
	}

	apply(card, simulation) {
		var amount = card.buffSpeed(this.amount);
		simulation.log(`${card.getName()}'s Speed increases by ${amount}.`);
	}

	remove(card, simulation) {
		card.buffSpeed(-this.amount);
	}
}