import Status from "./Status"

export default class extends Status.debuff() {
	static allowStacking() {
		return true;
	}

	static getName() {
		return "Attack Debuff";
	}

	constructor(amount) {
		super();
		this.amount = amount;
	}

	apply(card, simulation) {
		var amount = card.debuffAttack(this.amount);
		simulation.log(`${card.getName()}'s Attack decreases by ${amount}.`);
	}

	remove(card, simulation) {
		card.debuffAttack(-this.amount);
	}
}