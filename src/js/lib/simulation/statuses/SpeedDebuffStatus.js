import Status from "./Status"

export default class extends Status.debuff() {
	static allowStacking() {
		return true;
	}

	static getName() {
		return "Speed Debuff";
	}

	constructor(amount) {
		super();
		this.amount = amount;
	}

	apply(card, simulation) {
		var amount = card.debuffSpeed(this.amount);
		simulation.log(`${card.getName()}'s Speed decreases by ${amount}.`);
	}

	remove(card, simulation) {
		card.debuffSpeed(-this.amount);
	}
}