export default class {
	constructor() {
		this.callbacks = [];
	}

	clear() {
		this.callbacks = [];
	}

	listen(callback, priority) {
		var callbackItem = {
			priority: priority,
			callback: callback
		};

		var inserted = false;

		// Try insert, highest priority at the end of the array
		// Priority is like: 1st, 2nd, 3rd, etc. so lower number = higher priority
		for (var i = 0; i < this.callbacks.length; i++) {
			if (this.callbacks[i].priority <= priority) {
				this.callbacks.splice(i, 0, callbackItem);
				inserted = true;
				break;
			}
		}

		if (!inserted) {
			this.callbacks.unshift(callbackItem);
		}

		return () => {
			// Find and remove the callback
			for (var i = 0; i < this.callbacks.length; i++) {
				if (this.callbacks[i] === callbackItem) {
					this.callbacks.splice(i, 1);
					break;
				}
			}
		}
	}

	notify(event={}, ...params) {
		if (this.callbacks.length === 0) {
			return;
		}

		var i = 0;
		event.unregister = () => {
			this.callbacks.splice(i, 1);
		};

		var stop = false;
		event.stop = () => {
			stop = true;
		};

		if (event.prevent !== null) {
			event.preventStop = () => {
				event.prevent();
				event.stop();
			}
		}

		// Loop backwards, since highest priority items are at the end
		// This makes it possible to easily support removing callbacks
		// While it's notifying
		for (i = this.callbacks.length-1; i >= 0; i--) {
			var result = this.callbacks[i].callback(event, ...params);
			if (result === false || stop) {
				break;
			}
		}

		delete event.unregister;
		delete event.stop;
		delete event.preventStop;
	}
}
