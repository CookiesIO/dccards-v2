import Card from "dccards/Card"
import {Strain} from "dccards/Strains"
import {Rarity} from "dccards/Rarities"
import DataCollection from "./DataCollection"

export default class CardCollection extends DataCollection {
	getCards(filter=null) {
		if (filter === null) {
			return this;
		}

		var cards = this.toArray();
		if (filter instanceof Strain) {
			var result = [];
			for (var i = 0; i < cards.length; i++) {
				if (cards[i].getStrain().compareTo(filter) === 0) {
					result.push(cards[i]);
				}
			}
			return new CardCollection(result);
		}
		else if (filter instanceof Rarity) {
			var result = [];
			for (var i = 0; i < cards.length; i++) {
				if (cards[i].getRarity().compareTo(filter) === 0) {
					result.push(cards[i]);
				}
			}
			return new CardCollection(result);
		}
		else if (typeof filter === "function") {
			var result = [];
			for (var i = 0; i < cards.length; i++) {
				if (filter(cards[i])) {
					result.push(cards[i]);
				}
			}
			return new CardCollection(result);
		}
		throw new Error("getCards() can only filter by Strain, Rarity or a function");
	}
}