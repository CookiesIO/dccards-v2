export default class {
	constructor() {
		this.map = {};
		this.keys = [];
		this.values = [];
	}

	get(key, defaultValue=undefined) {
		if (this.has(key)) {
			return this.map[key].value;
		}
		return defaultValue;
	}

	set(key, value) {
		var index = this.keys.length;
		this.map[key] = {
			value: value,
			index: index
		};
		this.keys.push(key);
		this.values.push(value);
	}

	remove(key) {
		if (this.has(key)) {
			var index = this.map[key].index;
			this.keys.splice(index, 1);
			this.values.splice(index, 1);
			delete this.map[key]

			// Make sure the indexes matches
			for (var i = index; i < this.values.length; i++) {
				this.values[i].index = i;
			}
		}
	}

	has(key) {
		return Object.prototype.hasOwnProperty.call(this.map, key);
	}

	getValues() {
		return this.values.slice();
	}

	getKeys() {
		return this.keys.slice();
	}

	getCount() {
		return this.values.length;
	}

	clear() {
		this.map = {};
		this.keys = [];
		this.values = [];
	}

	foreach(callback) {
		for (var i = this.keys.length-1; i > 0; i--) {
			callback(this.keys[i], this.values[i]);
		}
	}
}