import Dictionary from "collections/dict"

export default class {
	constructor(values) {
		this.dictionary = new Dictionary();
		for (var i = 0; i < values.length; ++i) {
			var value = values[i];
			this.dictionary.set(value.getName(), value);
		}
	}

	findByName(name) {
		return this.dictionary.get(name, null);
	}

	toArray() {
		return this.dictionary.values();
	}

	getCount() {
		return this.dictionary.length;
	}
}