import Promise from "bluebird"
import Request from "superagent-bluebird-promise"

export default function(abilityName) {
	return Request.get(`/api/abilities/${encodeURIComponent(abilityName)}`).then((res) => {
		return res.body;
	});
}