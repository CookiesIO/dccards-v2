import cardFixtures from "./cards"
import abilityFixtures from "./abilities"

import Request from "superagent-bluebird-promise"
import config from "./dccards-mock-config"
import requestMock from "superagent-mock"

requestMock(Request, [
	{
		pattern: '/api/cards',
		fixtures: cardFixtures,
		callback: function (match, data) {
			return {
				body: data
			};
		}
	},
	{
		pattern: '/api/abilities',
		fixtures: abilityFixtures,
		callback: function (match, data) {
			return {
				body: data
			};
		}
	},
]);