export default function() {
	return [
		{
			id: 1,
			completed: false,
			name: "Serpent Lady",
			description: null,
			catalogNumber: null,
			cardNumber: null,
			spawnArea: "Beastling event",
			shape: "Female (Beastling)",

			strain: "Spitter", 
			rarity: "Legendary", 

			maxLevel: 60, 
			redeathable: true, 
			tradeable: true, 
			stats: {
				healthMin: 1200,
				healthMax: 3000,
				psycheMin: 1050,
				psycheMax: 2625,
				attackMin: 1103,
				attackMax: 2757,
				defenseMin: 1150,
				defenseMax: 2875,
				speedMin: 1000,
				speedMax: 2500,
				intelligenceMin: 1150,
				intelligenceMax: 2875
			},
			types: [
				"Average",
				"Fresh",
				"Mental",
				"Strong",
				"Tough",
				"Quick",
				"Smart",
				"Perfect"
			],
			skillset: {
				level01: "Survival Instinct",
				level15: "Pressure",
				level30: "Gore Frenzy",
				level40: "AI10-Cell Decomposition",
				level50: "Death's Fists",
				redeath: "A20-Cell Mutation",
			},
			builds: {
				average: ["Survival Instinct", "Pressure", "Death's Fists"],
				redeath: {
					average: ["Pressure", "Death's Fists", "A20-Cell Mutation"]
				}
			}
		}
	];
}