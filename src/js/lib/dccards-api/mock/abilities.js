export default function() {
	return [
		{
			name: "Survival Instinct",
			effect: "Withstands attack",
			cost: -1,
			costStat: "psyche",
			event: "Take Damage",
			priority: 1
		},
		{
			name: "Pressure",
			effect: "Doubles enemy psyche cost",
			cost: 400,
			costStat: "psyche",
			event: "Entry",
			priority: 1
		},
		{
			name: "Gore Frenzy",
			effect: "Strike +3",
			cost: 600,
			costStat: "psyche",
			event: "Attack",
			priority: 2
		},
		{
			name: "AI10-Cell Decomposition",
			effect: "ATK/INT -10%",
			cost: 400,
			costStat: "psyche",
			event: "Buff",
			priority: 1
		},
		{
			name: "Death's Fists",
			effect: "Strike +4",
			cost: 1200,
			costStat: "psyche",
			event: "Attack",
			priority: 1
		},
		{
			name: "A20-Cell Mutation",
			effect: "ATK +20%",
			cost: 200,
			costStat: "psyche",
			event: "Buff",
			priority: 2
		},
	];
}