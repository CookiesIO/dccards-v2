import Promise from "bluebird"
import Request from "superagent-bluebird-promise"

export default function() {
	return Request.get('/api/abilities').then((res) => {
		return res.body;
	});
}