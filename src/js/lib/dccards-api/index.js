import loadAbilities from "./loadAbilities"
import loadAbility from "./loadAbility"
import loadCard from "./loadCard"
import loadCards from "./loadCards"

export default {
	loadAbilities,
	loadAbility,
	loadCard,
	loadCards
}