import Promise from "bluebird"
import Request from "superagent-bluebird-promise"

export default function(cardName) {
	return Request.get(`/api/cards/${encodeURIComponent(cardName)}`).then((res) => {
		return res.body;
	});
}