import React, {PropTypes} from "react/addons"
import {Row, Col, Glyphicon, Panel, ListGroup, ListGroupItem} from "react-bootstrap"

import CardImage from "./CardImage"
import CardLink from "./CardLink"
import {Card} from "dccards"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		cards: PropTypes.arrayOf(PropTypes.instanceOf(Card)).isRequired,
		header: PropTypes.node.isRequired,
		imageType: PropTypes.oneOf(["none", "icon", "banner", "card"]),
	},

	getDefaultProps() {
		return {
			imageType: "none"
		}
	},

	renderLink(card) {
		switch (this.props.imageType) {
			case "none":
				return (
					<CardLink card={card}>
						{card.getName()}
					</CardLink>
				);
			case "icon":
				return (
					<CardLink card={card}>
						<CardImage card={card} imageType={this.props.imageType} />
						{card.getName()}
					</CardLink>
				);
			case "banner":
				return (
					<CardLink card={card}>
						{card.getName()}
						<CardImage className="img-responsive center-block" card={card} imageType={this.props.imageType} width="120" height="28" />
					</CardLink>
				);
			case "card":
				return (
					<CardLink card={card}>
						<CardImage className="img-responsive center-block" card={card} imageType={this.props.imageType} />
					</CardLink>
				);
		}
		return null;
	},

	render() {
		var {
			cards,
			imageType,
			...rest
		} = this.props;

		return (
			<Panel {...rest}>
				<ListGroup fill>
					{cards.map((card) => {
						return (
							<ListGroupItem key={card.getID()} className="text-center">{this.renderLink(card)}</ListGroupItem>
						);
					})}
				</ListGroup>
			</Panel>
		);
	}
});