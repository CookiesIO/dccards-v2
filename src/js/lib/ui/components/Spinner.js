import React, {PropTypes} from "react/addons"
import {Glyphicon} from "react-bootstrap"
import classnames from "classnames"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		glyph: PropTypes.string
	},
	
	getDefaultProps() {
		return {
			glyph: "refresh"
		}
	},

	render() {
		var {className, ...rest} = this.props;
		return (
			<Glyphicon {...rest} className={classnames(className, "glyphicon-spinning")} />
		);
	}
});