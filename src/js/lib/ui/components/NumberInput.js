import React, {PropTypes} from "react/addons"
import {Input} from "react-bootstrap"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		onChange: PropTypes.func,
		onKeyDown: PropTypes.func,
		onBlur: PropTypes.func,

		value: PropTypes.number,
		min: PropTypes.number,
		max: PropTypes.number
	},

	getDefaultProps() {
		return {
			value: 0,
			min: -Infinity,
			max: Infinity
		};
	},

	getInitialState() {
		return {
			valueOverride: null
		};
	},

	handleKeyDown(event) {
		// Prevent all but numeric and delete buttons
		if (!event.ctrlKey) {
			if ((event.altKey || event.shiftKey)
				|| ((event.keyCode < 48 || event.keyCode > 57) // outside 0-9
					&& (event.keyCode < 96 || event.keyCode > 105) // outside numpad 0-9
					&& event.keyCode !== 37 && event.keyCode !== 39 // not left/right arrows
					&& event.keyCode !== 46 && event.keyCode !== 8 // not delete/backspace
					&& (this.props.min >= 0 || (event.keyCode !== 189 && event.keyCode !== 109)) // not - if min is smaller than 0
					&& event.keyCode !== 9) // Tabs are allowed as well
				) {
				event.preventDefault();
			}

			// Up arrow
			if (event.keyCode === 38) {
				this.updateValue(this.props.value + (event.shiftKey ? 5 : 1));
			}

			// Down arrow
			if (event.keyCode === 40) {
				this.updateValue(this.props.value - (event.shiftKey ? 5 : 1));
			}

			// The "-" buttons, make the number negative
			if (event.keyCode === 189 || event.keyCode === 109) {
				if (this.props.min < 0 && this.props.value > 0) {
					this.updateValue(this.props.value * -1);
				}
			}

			// The "+" buttons, make the number positive
			if (event.keyCode === 187 || event.keyCode === 107) {
				if (this.props.max > 0 && this.props.value < 0) {
					this.updateValue(this.props.value * -1);
				}
			}

			if (event.keyCode === 13) {
				event.target.blur();
			}
		}

		if (this.props.onKeyDown) {
			this.props.onKeyDown(event);
		}
	},

	handleChange() {
		var value = this.refs.input.getValue();
		var valueAsInt = parseInt(value);
		if (!isNaN(valueAsInt)) {
			this.setState({
				valueOverride: null
			});
			this.updateValue(valueAsInt);
		}
		else {
			if (this.state.valueOverride === null && this.props.value !== 0) {
				this.updateValue(0);
			}
			this.setState({
				valueOverride: value
			});
		}
	},

	handleBlur(event) {
		this.setState({
			valueOverride: null
		});

		if (this.props.onBlur) {
			this.props.onBlur(event);
		}
	},

	updateValue(value) {
		if (value < this.props.min) {
			value = this.props.min;
		}
		else if (value > this.props.max) {
			value = this.props.max;
		}
		if (value !== this.props.value) {
			this.props.onChange(value);
		}
	},

	getStyle() {
		var style = undefined;
		if (this.state.valueOverride !== null) {
			if (/^-?$/.test(this.state.valueOverride)) {
				style = "warning";
			}
			else {
				style = "error";
			}
		}
		return style;
	},

	getDisplayValue() {
		if (this.state.valueOverride !== null) {
			return this.state.valueOverride;
		}
		return this.props.value;
	},

	render() {
		var { value, onChange, onKeyDown, onBlur, ...rest } = this.props;
		return (
			<Input
				{...rest}
				type="number"
				value={this.getDisplayValue()}
				bsStyle={this.getStyle()}
				ref="input"
				onChange={this.handleChange}
				onKeyDown={this.handleKeyDown}
				onBlur={this.handleBlur}
			/>
		);
	}
});
