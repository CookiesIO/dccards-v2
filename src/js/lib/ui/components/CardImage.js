import React, {PropTypes} from "react/addons"

import {Card} from "dccards"
import Image from "./Image"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		card: PropTypes.instanceOf(Card).isRequired,
		imageType: PropTypes.oneOf(["card", "icon", "banner"])
	},

	getDefaultProps() {
		return {
			imageType: "card"
		};
	},
	
	getImageSrc() {
		return `/assets/images/cards/${this.props.imageType}/${this.props.card.getName()}.png`;
	},
	
	getImageErrorSrc() {
		return `/assets/images/cards/${this.props.imageType}/image_not_found.png`;
	},

	render() {
		var {card, imageType, ...rest} = this.props;
		return (
			<Image {...rest} src={this.getImageSrc()} errorSrc={this.getImageErrorSrc()} alt={card.getName()} />
		);
	}
});