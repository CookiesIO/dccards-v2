import React, {PropTypes} from "react/addons"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		src: PropTypes.string.isRequired,
		errorSrc: PropTypes.string,
		onError: PropTypes.func
	},

	getInitialState() {
		return {
			imageError: false
		};
	},

	getDefaultProps() {
		return {
			errorSrc: null
		};
	},

	componentWillReceiveProps() {
		this.setState({
			imageError: false
		});
	},

	handleError() {
		if (this.state.imageError === false) {
			this.setState({
				imageError: true
			});
			if (this.props.onError) {
				this.props.onError();
			}
		}
	},

	getImageUrl() {
		if (this.state.imageError) {
			return this.props.errorSrc;
		}
		return this.props.src;
	},

	render() {
		var {src, errorSrc, onError, ...rest} = this.props;
		return (
			<img {...rest} src={this.getImageUrl()} onError={this.handleError} />
		);
	}
});