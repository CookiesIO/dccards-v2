import React, {PropTypes} from "react/addons"
import Reflux from "reflux"
import {Button} from "react-bootstrap"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		data: PropTypes.any,
		selected: PropTypes.bool,
		disabled: PropTypes.bool,
		onClick: PropTypes.func,
		defaultStyle: PropTypes.string,
		selectedStyle: PropTypes.string,
	},
	
	getDefaultProps() {
		return {
			data: undefined,
			selected: false,
			disabled: false,
			onClick: () => {},
			defaultStyle: "default",
			selectedStyle: "primary"
		}
	},

	handleClick() {
		this.props.onClick(this.props.data);
	},

	render() {
		var {
			selected,
			disabled,
			children,
			onClick,
			defaultStyle,
			selectedStyle,
			...rest
		} = this.props;
		
		return (
			<Button
				{...rest}
				bsStyle={selected ? selectedStyle : defaultStyle}
				disabled={disabled}
				onClick={this.handleClick}>
				{children}
			</Button>
		);
	}
});