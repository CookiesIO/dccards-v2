import React, {PropTypes} from "react/addons"

import {Strains} from "dccards"
import Image from "./Image"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		strain: PropTypes.oneOf(Strains.getAll()).isRequired
	},

	getImageUrl() {
		return `/assets/images/strains/${this.props.strain.getName()}.png`;
	},

	render() {
		var {strain, ...rest} = this.props;
		return (
			<Image {...rest} src={this.getImageUrl()} alt={strain.getName()} />
		);
	}
});