import React, {PropTypes} from "react/addons"
import {Glyphicon} from "react-bootstrap"

import {Rarities} from "dccards"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		rarity: PropTypes.oneOf(Rarities.getAll()).isRequired
	},
	
	render() {
		var {rarity, ...rest} = this.props;

		var stars = [];
		if (rarity.index >= 0) {
			stars.push((
				<Glyphicon key="1" glyph="star" />
			));
		}
		if (rarity.index >= 1) {
			stars.push((
				<Glyphicon key="2" glyph="star" />
			));
		}
		if (rarity.index >= 2) {
			stars.push((
				<Glyphicon key="3" glyph="star" />
			));
		}
		if (rarity.index >= 3) {
			stars.push((
				<Glyphicon key="4" glyph="star" />
			));
		}
		if (rarity.index == 4) {
			stars.push((
				"+"
			));
		}
		if (rarity.index >= 5) {
			stars.push((
				<Glyphicon key="5" glyph="star" />
			));
		}

		return (
			<span {...rest}>
				{stars}
			</span>
		);
	}
});