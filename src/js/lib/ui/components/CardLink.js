import React, {PropTypes} from "react/addons"
import {Link} from "react-router"
import {Card} from "dccards"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		card: PropTypes.instanceOf(Card).isRequired
	},
	
	render() {
		var {card, ...rest} = this.props;
		return (
			<Link {...rest} to="calculatorCard" params={{cardName: card.getName()}} >
				{this.props.children}
			</Link>
		);
	}
});