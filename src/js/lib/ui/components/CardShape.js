import React, {PropTypes} from "react/addons"

import Image from "./Image"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		shape: PropTypes.string.isRequired
	},

	getImageSrc() {
		if (this.props.shape === null) {
			return this.getImageErrorSrc();
		}
		var shape = this.props.shape.replace(/(\(|\))/g, "").replace(" ", "-").toLowerCase();
		return `/assets/images/shapes/${shape}.png`;
	},

	getImageErrorSrc() {
		return `/assets/images/shapes/image_not_found.png`;
	},

	render() {
		var {shape, ...rest} = this.props;
		return (
			<Image {...rest} src={this.getImageSrc()} errorSrc={this.getImageErrorSrc()} alt={shape} />
		);
	}
});