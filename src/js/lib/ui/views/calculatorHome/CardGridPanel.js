import React, {PropTypes} from "react/addons"
import {Row, Col, Panel} from "react-bootstrap"

import {Card, Rarities} from "dccards"
import {CardImage, CardLink} from "ui/components"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		cards: PropTypes.arrayOf(PropTypes.instanceOf(Card)).isRequired,
		eventKey: PropTypes.string.isRequired,
		activeKey: PropTypes.string
	},

	getInitialState() {
		return {
			sortedCards: null,
		};
	},

	cardComparer(cardA, cardB) {
		return cardA.compareTo(cardB, true, true);
	},

	sortIfNeccesary() {
		if (this.props.eventKey === this.props.eventKey) {
			if (this.state.sortedCards === null) {
				this.setState({
					sortedCards: this.props.cards.sort(this.cardComparer)
				});
			}
		}
	},

	componentWillMount() {
		this.sortIfNeccesary();
	},

	componentWillReceiveProps(nextProps) {
		if (this.props.cards !== nextProps.cards) {
			this.setState({
				sortedCards: null
			});
		}
		this.sortIfNeccesary();
	},

	render() {
		var {
			cards,
			eventKey,
			activeKey,
			...rest
		} = this.props;

		var {
			sortedCards
		} = this.state;

		cards = null;
		if (eventKey === activeKey && sortedCards !== null) {
			var colSizes = {
				xs: 12,
				sm: 6,
				md: 4
			};

			cards = [];
			var lastCharCategory = null;
			for (var i = 0; i < sortedCards.length; i++) {
				var card = sortedCards[i];

				var cardName = card.getName(true);
				var charCategory = cardName.charAt(0).toUpperCase();

				if (charCategory !== lastCharCategory) {
					cards.push((
						<Col xs={12} className="category-row">
							{charCategory}
						</Col>
					));
					lastCharCategory = charCategory;
				}

				cards.push((
					<Col
						{...colSizes}
						key={i}
						className="text-center"
						style={{height: 48}}
					>
						<CardLink card={card}>
							{card.getName()}
							<CardImage
								className="img-responsive center-block"
								card={card}
								imageType={"banner"}
								width="120"
								height="28"
								style={{width: 120, height: 28}}
							/>
						</CardLink>
					</Col>
				));
			}
		}

		return (
			<Panel {...rest} eventKey={eventKey}>
				<Row>
					{cards}
				</Row>
			</Panel>
		);
	}
});