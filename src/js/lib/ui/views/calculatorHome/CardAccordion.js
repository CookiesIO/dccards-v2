import React, {PropTypes} from "react/addons"
import {Accordion} from "react-bootstrap"

import {Card} from "dccards"
import CardGridPanel from "./CardGridPanel"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		pestilents: PropTypes.arrayOf(PropTypes.instanceOf(Card)).isRequired,
		virulents: PropTypes.arrayOf(PropTypes.instanceOf(Card)).isRequired,
		legendaries: PropTypes.arrayOf(PropTypes.instanceOf(Card)).isRequired,
		epicPlus: PropTypes.arrayOf(PropTypes.instanceOf(Card)).isRequired,
		epics: PropTypes.arrayOf(PropTypes.instanceOf(Card)).isRequired,
		rares: PropTypes.arrayOf(PropTypes.instanceOf(Card)).isRequired,
		uncommons: PropTypes.arrayOf(PropTypes.instanceOf(Card)).isRequired,
		commons: PropTypes.arrayOf(PropTypes.instanceOf(Card)).isRequired
	},

	getInitialState() {
		return {
			activeKey: null,
		};
	},

	handleSelect(key) {
		if (key === this.state.activeKey) {
			key = null;
		}
		this.setState({
			activeKey: key
		});
	},

	render() {
		var {
			activeKey,
			pestilents,
			virulents, 
			legendaries,
			epicPlus,
			epics,
			rares,
			uncommons, 
			commons,
			...rest
		} = this.props;

		var {
			activeKey 
		} = this.state;

		return (
			<Accordion
				activeKey={activeKey}
				onSelect={this.handleSelect}
			>
				<CardGridPanel
					header="Pestilents"
					cards={pestilents}
					eventKey="pestilents"
					activeKey={activeKey}
				/>
				<CardGridPanel
					header="Virulents"
					cards={virulents}
					eventKey="virulents"
					activeKey={activeKey}
				/>
				<CardGridPanel
					header="Legendaries"
					cards={legendaries}
					eventKey="legendaries"
					activeKey={activeKey}
				/>
				<CardGridPanel
					header="Epic Plus"
					cards={epicPlus}
					eventKey="epicPlus"
					activeKey={activeKey}
				/>
				<CardGridPanel
					header="Epics"
					cards={epics}
					eventKey="epics"
					activeKey={activeKey}
				/>
				<CardGridPanel
					header="Rares"
					cards={rares}
					eventKey="rares"
					activeKey={activeKey}
				/>
				<CardGridPanel
					header="Uncommons"
					cards={uncommons}
					eventKey="uncommons"
					activeKey={activeKey}
				/>
				<CardGridPanel
					header="Commons"
					cards={commons}
					eventKey="commons"
					activeKey={activeKey}
				/>
			</Accordion>
		);
	}
});