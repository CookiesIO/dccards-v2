import React from "react"
import Reflux from "reflux"

import {RouteHandler} from "react-router"
import {Grid, Row, Col, Alert, Glyphicon} from "react-bootstrap"
import {Spinner} from "ui/components"

import Header from "./app/Header"
import Footer from "./app/Footer"

import StateStore from "stores/dccards/StateStore"

export default React.createClass({
	mixins: [Reflux.connect(StateStore, "currentState")],
	getLoadingText() {
		if (this.state.currentState.loaded) {
			return "Loading data, currently displaying cached data...";
		}
		return "Loading data...";
	},
	getLoadingFailedText() {
		if (this.state.currentState.loaded) {
			return "Loading failed, currently using cached data.";
		}
		return "Loading failed.";
	},
	render() {
		var children = [];
		if (this.state.currentState.loading) {
			children.push((
				<Row key="loading">
					<Col xs={12} md={6} mdPush={3}>
						<Alert bsStyle="info">
							<Spinner /> {this.getLoadingText()}
						</Alert>
					</Col>
				</Row>
			));
		}
		if (this.state.currentState.loadingFailed) {
			children.push((
				<Row key="failed">
					<Col xs={12} md={6} mdPush={3}>
						<Alert bsStyle="warning">
							<Glyphicon glyph="remove-sign" /> {this.getLoadingFailedText()}
						</Alert>
					</Col>
				</Row>
			));
		}
		if (this.state.currentState.loaded) {
			children.push((
				<RouteHandler key="render"/>
			));
		}
		return (
			<div>
				<Header fluid={true} />
				<Grid componentClass="section" fluid={true}>
					{children}
				</Grid>
				<Footer fluid={true} />
			</div>
		);
	}
});