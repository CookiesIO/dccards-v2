import React from "react"
import {Navbar, Nav, DropdownButton, MenuItem} from "react-bootstrap"
import {NavItemLink, MenuItemLink} from "react-router-bootstrap"

export default React.createClass({
	propTypes: {
		fluid: React.PropTypes.bool
	},
	getDefaultProps() {
		return {
			fluid: false
		};
	},
	render() {
		return (
			<header>
				<Navbar fluid={this.props.fluid} brand="Deadman Cards">
					<Nav>
						<NavItemLink to="home">Home</NavItemLink>
						<NavItemLink to="calculator">Calculator</NavItemLink>
						{/*<NavItemLink to="contribute">Contribute</NavItemLink>*/}
						<DropdownButton title="Community">
							<MenuItem href="http://deadmanscrossforum.com/">{"Deadman's Cross Forum"}</MenuItem>
							<MenuItem href="http://deadmans-cross.wikia.com/wiki/Deadman%27s_Cross_Wiki">{"Deadman's Cross Wiki"}</MenuItem>
						</DropdownButton>
						{/*<NavItemLink to="about">About</NavItemLink>*/}
						{/*<NavItemLink to="app">Home</NavItemLink>*/}
					</Nav>
				</Navbar>
			</header>
		);
	}
});