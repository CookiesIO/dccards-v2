import React from "react"
import {Grid, Row, Col} from "react-bootstrap"

export default React.createClass({
	propTypes: {
		fluid: React.PropTypes.bool
	},
	getDefaultProps() {
		return {
			fluid: false
		};
	},
	render() {
		return (
			<Grid fluid={this.props.fluid} componentClass="footer">
				<Row>
					<Col xs={4} className="text-left"><a href="/humans.txt"><img src="/assets/images/humans_logo_small.png" alt="Humans.txt" /></a></Col>
					<Col xs={4} className="text-center">All images are owned by <a href="http://www.jp.square-enix.com/dc/" rel="nofollow" target="_blank">Square Enix</a> Co., Ltd.</Col>
				</Row>
			</Grid>
		);
	}
});