import React from "react/addons"
import Reflux from "reflux"
import {Row, Col} from "react-bootstrap"
import DocumentTitle from "react-document-title"

import {CardList} from "ui/components"
import {Rarities} from "dccards"
import CalculatorCardsStore from "stores/ui/CalculatorCardsStore"

import CardAccordion from "./calculatorHome/CardAccordion"

export default React.createClass({
	mixins: [Reflux.connect(CalculatorCardsStore, "cards"), React.addons.PureRenderMixin],
	render() {
		var {
			cards: {
				pestilents,
				virulents,
				legendaries,
				epicPlus,
				epics,
				rares,
				uncommons,
				commons
			}
		} = this.state;

		pestilents  = pestilents.toArray();
		virulents   = virulents.toArray();
		legendaries = legendaries.toArray();
		epicPlus    = epicPlus.toArray();
		epics       = epics.toArray();
		rares       = rares.toArray();
		uncommons   = uncommons.toArray();
		commons     = commons.toArray();

		var colSizes = {
			xs: 12,
			lg: 8,
			lgPush: 2
		};

		return (
			<DocumentTitle title="Deadmans Cross Cards - Calculator">
				<Row>
					<Col {...colSizes}>
						<CardAccordion
							pestilents={pestilents}
							virulents={virulents}
							legendaries={legendaries}
							epicPlus={epicPlus}
							epics={epics}
							rares={rares}
							uncommons={uncommons}
							commons={commons}
						/>
					</Col>
				</Row>
			</DocumentTitle>
		);
	}
});