import React from "react"
import Reflux from "reflux"
import DocumentTitle from "react-document-title"

import {RouteHandler} from "react-router"
import {Row, Col, Well} from "react-bootstrap"
import {Spinner} from "ui/components"

export default React.createClass({
	render() {
		return (
			<DocumentTitle title="Deadmans Cross Cards - Home">
				<Row>
					<Col xs={12}>
						<Well bsSize="small">
							<h4>Welcome to DCCards!</h4>
							<p>This page will maybe, hopefully, at one point have news about the site.</p>
						</Well>
					</Col>
				</Row>
			</DocumentTitle>
		);
	}
});