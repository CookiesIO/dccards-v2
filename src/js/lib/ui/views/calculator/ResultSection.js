import React, {PropTypes} from "react"
import {Panel, Row, Col} from "react-bootstrap"

import ResultRow from "./ResultRow"

export default React.createClass({
	propTypes: {
		resultSection: PropTypes.shape({
			section: PropTypes.string.isRequired,
			typeCount: PropTypes.number.isRequired,
			cards: PropTypes.array.isRequired
		})
	},

	shouldComponentUpdate(nextProps, nextState) {
		if (nextProps.resultSection.section !== this.props.section) {
			return true;
		}

		if (nextProps.typeCount !== this.props.typeCount) {
			return true;
		}

		var nextCards = nextProps.resultSection.cards;
		var currentCards = this.props.resultSection.cards;

		if (nextCards.length !== currentCards.length) {
			return true;
		}

		for (var i = 0; i < nextCards.length; i++) {
			var nextCard = nextCards[i];
			var currentCard = currentCards[i];
			if (nextCard.card !== currentCard.card) {
				return true;
			}
			else {
				if (nextCard.types.length !== currentCard.types.length) {
					return true;
				}
				else {
					for (var j = 0; j < nextCard.types.length; j++) {
						if (nextCard.types[i] !== currentCard.types[i]) {
							return true;
						}
					}
				}
			}
		}

		return false;
	},

	render() {
		var {resultSection: {
			section,
			typeCount,
			cards
		}} = this.props;

		return (
			<Panel
				header={(
					<span>
						<strong>{section}</strong>
						{`: ${cards.length} (${typeCount})`}
					</span>
				)}
				collapsable
				defaultExpanded
			>
				<div className="grid-table grid-striped grid-hover">
					{cards.map((cardResult, i) => {
						return (
							<ResultRow
								key={i}
								card={cardResult.card}
								types={cardResult.types}
							/>
						);
					})}
				</div>
			</Panel>
		);
	}
});