import React, {PropTypes} from "react/addons"

import ResultSection from "./ResultSection"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		results: PropTypes.array
	},

	render() {
		var { results } = this.props;

		if (results === null) {
			return (
				<div>
				</div>
			);
		}

		return (
			<div>
				{results.map((resultSection, i) => {
					return (
						<ResultSection key={i}
							resultSection={resultSection}
						/>
					);
				})}
			</div>
		);
	}
});