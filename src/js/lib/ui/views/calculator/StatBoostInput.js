import React, {PropTypes} from "react/addons"
import {Input, Row, Col, OverlayTrigger, Tooltip} from "react-bootstrap"

import OptionSection from "./OptionSection"
import {NumberInput} from "ui/components"

import {Card, Types} from "dccards"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		stat: PropTypes.string.isRequired,
		boost: PropTypes.number.isRequired,
		maxBoost: PropTypes.number.isRequired,
		baseStat: PropTypes.number.isRequired,
		onChange: PropTypes.func.isRequired,
	},

	getInitialState() {
		return {
			isEditting: false
		};
	},

	handleChange(value) {
		if (!this.state.isEditting) {
			value -= this.props.baseStat;
		}
		this.props.onChange(value);
	},

	handleFocus() {
		this.refs.overlay.show();
		this.setState({
			isEditting: true
		});
	},

	handleBlur() {
		this.refs.overlay.hide();
		this.setState({
			isEditting: false
		});
	},

	render() {
		var {
			stat,
			boost,
			maxBoost,
			baseStat,
			onChange,
			...rest
		} = this.props;

		var { isEditting } = this.state;

		return (
			<OverlayTrigger
				ref="overlay"
				trigger="manual"
				placement="top"
				overlay={<Tooltip>{`${baseStat+boost} (${baseStat}+${boost})`}</Tooltip>}
			>
				<NumberInput
					{...rest}
					value={isEditting ? boost : baseStat+boost}
					label={stat}
					min={isEditting ?        0 : baseStat}
					max={isEditting ? maxBoost : baseStat+maxBoost}
					onChange={this.handleChange}
					onFocus={this.handleFocus}
					onBlur={this.handleBlur}
				/>
			</OverlayTrigger>
		);
	}
});
