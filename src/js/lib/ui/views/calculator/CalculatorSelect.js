import React, {PropTypes} from "react/addons"
import {Input} from "react-bootstrap"

import Calculator from "calculators/Calculator"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		calculators: PropTypes.arrayOf(PropTypes.instanceOf(Calculator)).isRequired,
		calculator: PropTypes.instanceOf(Calculator).isRequired,
		onCalculatorChange: PropTypes.func.isRequired
	},

	handleChange() {
		var choice = this.refs.input.getValue();
		var calculators = this.props.calculators;
		for (var i = 0; i < calculators.length; i++) {
			if (choice === calculators[i].getName()) {
				this.props.onCalculatorChange(calculators[i]);
				break;
			}
		}
	},

	render() {
		var {
			calculator,
			calculators
		} = this.props;

		return (
			<Input
				type="select"
				label="Calculator"
				value={calculator.getName()}
				onChange={this.handleChange}
				ref="input"
			>
				{calculators.map((calculator) => {
					return (
						<option
							key={calculator.getName()}
							value={calculator.getName()}
						>
							{calculator.getName()}
						</option>
					);
				})}
			</Input>
		);
	}
});