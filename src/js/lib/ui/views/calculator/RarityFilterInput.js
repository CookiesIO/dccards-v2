import React, {PropTypes} from "react/addons"
import {ButtonGroup} from "react-bootstrap"
import {ToggleButton} from "ui/components"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		filter: PropTypes.shape({
			pestilents:  PropTypes.bool.isRequired,
			virulents:   PropTypes.bool.isRequired,
			legendaries: PropTypes.bool.isRequired,
			epicPlus:    PropTypes.bool.isRequired,
			epics:       PropTypes.bool.isRequired,
			rares:       PropTypes.bool.isRequired,
			uncommons:   PropTypes.bool.isRequired,
			commons:     PropTypes.bool.isRequired,
		}).isRequired,
		onFilterChange: PropTypes.func.isRequired
	},

	handleChange(rarity) {
		var {
			pestilents,
			virulents,
			legendaries,
			epicPlus,
			epics,
			rares,
			uncommons,
			commons
		} = this.props.filter;

		this.props.onFilterChange({
			pestilents,
			virulents,
			legendaries,
			epicPlus,
			epics,
			rares,
			uncommons,
			commons,
			[rarity]: !this.props.filter[rarity]
		});
	},

	renderMobileButton(display, selected, rarity) {
		return (
			<ButtonGroup bsSize="small">
				{this.renderButton(display, selected, rarity, true)}
			</ButtonGroup>
		);
	},

	renderButton(display, selected, rarity, noPadding=false) {
		return (
			<ToggleButton
				data={rarity}
				selected={selected}
				selectedStyle="warning"
				onClick={this.handleChange}
				className="overflow-ellipsis"
				style={noPadding ? {paddingLeft:0, paddingRight:0} : null}
			>
				{display}
			</ToggleButton>
		);
	},

	render() {
		var {
			pestilents,
			virulents,
			legendaries,
			epicPlus,
			epics,
			rares,
			uncommons,
			commons
		} = this.props.filter;

		return (
			<ButtonGroup style={{marginBottom: 10}}>
				<ButtonGroup justified>
					{/* Pestilents */}
					{this.renderMobileButton("Pestilents", pestilents, "pestilents")}

					{/* Virulents */}
					{this.renderMobileButton("Virulents", virulents, "virulents")}

					{/* Legendaries */}
					{this.renderMobileButton("Legendaries", legendaries, "legendaries")}
				</ButtonGroup>
				<div /> {/* Keep the div, it fixes incorrect styling */}
				<ButtonGroup justified>
					{/* Epic Plus */}
					{this.renderMobileButton("Epic Plus", epicPlus, "epicPlus")}

					{/* Epics */}
					{this.renderMobileButton("Epics", epics, "epics")}
				</ButtonGroup>
				<div /> {/* Keep the div, it fixes incorrect styling */}
				<ButtonGroup justified>

					{/* Rares */}
					{this.renderMobileButton("Rares", rares, "rares")}

					{/* Uncommons */}
					{this.renderMobileButton("Uncommons", uncommons, "uncommons")}

					{/* Commons */}
					{this.renderMobileButton("Commons", commons, "commons")}
				</ButtonGroup>
			</ButtonGroup>
		);	/*<div className="hidden-xs">
					<ButtonGroup bsSize="xsmall">
						{/* Pestilents *}
						{this.renderButton("Pestilents", pestilents, "pestilents")}

						{/* Virulents *}
						{this.renderButton("Virulents", virulents, "virulents")}

						{/* Legendaries *}
						{this.renderButton("Legendaries", legendaries, "legendaries")}

						{/* Epic Plus *}
						{this.renderButton("Epic Plus", epicPlus, "epicPlus")}

						{/* Epics *}
						{this.renderButton("Epics", epics, "epics")}

						{/* Rares *}
						{this.renderButton("Rares", rares, "rares")}

						{/* Uncommons *}
						{this.renderButton("Uncommons", uncommons, "uncommons")}

						{/* Commons *}
						{this.renderButton("Commons", commons, "commons")}
					</ButtonGroup>
				</div>*/
	}
});