import React, {PropTypes} from "react/addons"

import DynamicOptionGroup from "./DynamicOptionGroup"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		onOptionChange: PropTypes.func.isRequired,
		options: PropTypes.array.isRequired
	},

	render() {
		var optionGroups = this.props.options.map((group) => {
			return (
				<DynamicOptionGroup
					key={group.group}
					group={group}
					onOptionChange={this.props.onOptionChange}
				/>
			);
		});
		return (
			<div>
				{optionGroups}
			</div>
		);
	}
});
