import React, {PropTypes} from "react/addons"

import {NumberInput} from "ui/components"
import OptionSection from "./OptionSection"
import BoostsInput from "./BoostsInput"
import BuildInput from "./BuildInput"

import {Card, Types} from "dccards"
import Ability from "simulation/abilities/Ability"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		onLevelChange: PropTypes.func.isRequired, 
		onBuildChange: PropTypes.func.isRequired, 
		onBoostsChange: PropTypes.func.isRequired, 
		card: PropTypes.instanceOf(Card).isRequired,
		type: PropTypes.oneOf(Types.getAll()).isRequired,
		level: PropTypes.number.isRequired,
		build: PropTypes.arrayOf(PropTypes.instanceOf(Ability)).isRequired,
		boosts: PropTypes.shape({
			health: PropTypes.number,
			psyche: PropTypes.number,
			attack: PropTypes.number,
			defense: PropTypes.number,
			speed: PropTypes.number,
			intelligence: PropTypes.number,
		}).isRequired,
		redeathed: PropTypes.bool.isRequired
	},

	handleLevelChange(level) {
		this.props.onLevelChange(level);
	},

	handleBuildChange(build) {
		this.props.onBuildChange(build);
	},

	handleBoostsChange(boosts) {
		this.props.onBoostsChange(boosts);
	},

	render() {
		var {
			card,
			type,
			level,
			build,
			boosts,
			redeathed,
			onLevelChange,
			onBuildChange,
			onBoostsChange
		} = this.props;

		var maxLevel = card.getMaxLevel(redeathed);

		return (
			<OptionSection
				header={`Selected Card`}
				style="info"
			>
				<NumberInput
					value={level}
					label="Level"
					min={1}
					max={maxLevel}
					onChange={this.handleLevelChange}
				/>
				<BoostsInput
					card={card}
					type={type}
					level={level}
					boosts={boosts}
					redeathed={redeathed}
					onBoostsChange={this.handleBoostsChange}
				/>
				<BuildInput
					card={card}
					level={level}
					build={build}
					redeathed={redeathed}
					onBuildChange={this.handleBuildChange}
				/>
			</OptionSection>
		);
	}
});
