import React, {PropTypes} from "react/addons"
import {Row, Col} from "react-bootstrap"

import {ToggleButton} from "ui/components"

import {Card, Rarities} from "dccards"
import Ability from "simulation/abilities/Ability"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		onBuildChange: PropTypes.func.isRequired, 
		card: PropTypes.instanceOf(Card).isRequired,
		level: PropTypes.number.isRequired,
		build: PropTypes.arrayOf(PropTypes.instanceOf(Ability)).isRequired,
		redeathed: PropTypes.bool.isRequired
	},

	handleToggleAbility(ability) {
		var build = this.props.build;
		var index = build.indexOf(ability);
		if (index === -1) {
			this.props.onBuildChange(build.concat([ability]));
		}
		else {
			build = build.slice();
			build.splice(index, 1);
			this.props.onBuildChange(build);
		}
	},

	renderButton(key, ability) {
		var { build, card } = this.props;
		var hasAbility = build.indexOf(ability) !== -1;

		return (
			<ToggleButton
				key={key}
				data={ability}
				selected={hasAbility}
				disabled={!hasAbility && build.length === 3}
				onClick={this.handleToggleAbility}
				bsSize="small"
				block

			>
				<p className="text-left no-margin overflow-ellipsis">
					{ability.getName(card)}
				</p>
				<p className="text-right no-margin overflow-ellipsis">
					<span className="pull-left ability-cost-margin">
						{ability.getCostText()}
					</span>
					<span>
						{ability.getEffect(card)}
					</span>
				</p>
			</ToggleButton>
		);
	},

	render() {
		var {
			card,
			level,
			redeathed,
		} = this.props;

		var redeathMaxLevel = card.getMaxLevel(true);
		var skillset = card.getSkillset();

		var buttons = [
			this.renderButton("01", skillset.level01)
		];
		if (level >= 15) {
			buttons.push(this.renderButton("15", skillset.level15));
		}
		if (level >= 30) {
			buttons.push(this.renderButton("30", skillset.level30));
		}
		if (level >= 40 && card.getRarity() !== Rarities.common) {
			buttons.push(this.renderButton("40", skillset.level40));
		}
		if (level >= 50) {
			buttons.push(this.renderButton("50", skillset.level50));
		}
		if (card.getRedeathable() && level === redeathMaxLevel) {
			buttons.push(this.renderButton("rd", skillset.redeath));
		}

		return (
			<div style={{marginBottom: "15px"}}>
				{buttons}
			</div>
		);
	}
});
