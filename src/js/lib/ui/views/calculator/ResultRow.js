import React, {PropTypes} from "react/addons"
import {Row, Col, Glyphicon} from "react-bootstrap"

import {Rarities, Types, Card} from "dccards"
import {CardImage, CardLink, StrainImage} from "ui/components"

export default React.createClass({
	propTypes: {
		card: PropTypes.instanceOf(Card),
		types: PropTypes.arrayOf(PropTypes.oneOf(Types.getAll()))
	},

	shouldComponentUpdate(nextProps, nextState) {
		if (nextProps.card !== this.props.card) {
			return true;
		}
		if (nextProps.types.length !== this.props.types.length) {
			return true;
		}
		for (var i = 0; i < nextProps.types.length; i++) {
			if (nextProps.types[i] !== this.props.types.length) {
				return true;
			}
		}
		return false;
	},

	render() {
		var { card, types } = this.props;

		var leftCol = {xs:12, sm:5, md:4};
		var rightCol = {xs:12, sm:7, md:8};
		return (
			<Row>
				<Col {...leftCol}>
					<CardImage
						card={card}
						imageType="icon"
						className="pull-left"
						width="18"
						height="18"
					/>
					<StrainImage
						strain={card.getStrain()}
						className="pull-left"
						width="18"
						height="18"
					/>{' '}
					<CardLink card={card}>
						{card.getName()} <Glyphicon glyph="link" />
					</CardLink>
				</Col>
				<Col {...rightCol}>
					{types.map((type) => {
						return type.getName();;
					}).join(', ')}
				</Col>
			</Row>
		);
	}
});