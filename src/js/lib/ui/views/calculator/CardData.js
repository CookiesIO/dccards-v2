import React, {PropTypes} from "react/addons"
import Reflux from "reflux"
import {Link} from "react-router"
import {Row, Col, Alert, Label, Glyphicon, ButtonToolbar} from "react-bootstrap"
import {CardImage, CardShape, CardStars, NumberInput} from "ui/components"

import CardDataHeader from "./CardDataHeader"
import StatTable from "./StatTable"
import TypeSelect from "./TypeSelect"
import RedeathButton from "./RedeathButton"
import BoostsInput from "./BoostsInput"

import {Card, Types} from "dccards"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		card: PropTypes.instanceOf(Card).isRequired,
		type: PropTypes.oneOf(Types.getAll()).isRequired,
		redeathed: PropTypes.bool.isRequired,
		onTypeChange: PropTypes.func.isRequired,
		onRedeathChange: PropTypes.func.isRequired
	},

	handleTypeChange(type) {
		this.props.onTypeChange(type);
	},

	handleRedeathClick() {
		this.props.onRedeathChange(!this.props.redeathed);
	},
	
	render() {
		var {
			card,
			type,
			redeathed,
		} = this.props;

		return (
			<div>
				<CardDataHeader cardName={card.getName()} />
				<CardStars rarity={card.getRarity()} />

				<div>
					<CardShape shape={card.getShape()} />
					<span>{card.getSpawnArea()}</span>
				</div>

				<p style={{color:"#777", fontSize:"85%"}}>
					{card.getDescription()}
				</p>
				<ButtonToolbar>
					<RedeathButton
						selectedStyle="danger"
						selected={redeathed}
						disabled={!card.getRedeathable()}
						onClick={this.handleRedeathClick}
					/>
					<TypeSelect
						types={card.getTypes()}
						type={type}
						onTypeChange={this.handleTypeChange}
					/>
				</ButtonToolbar>
				<StatTable
					card={card}
					type={type}
					redeathed={redeathed}
				/>
			</div>
		);
	}
});
