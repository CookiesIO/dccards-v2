import React, {PropTypes} from "react/addons"
import {ListGroupItem} from "react-bootstrap"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		header: PropTypes.node.isRequired,
		style: PropTypes.oneOf(["primary", "success", "info", "warning", "danger"])
	},
	getDefaultProps() {
		return {
			style: "primary"
		};
	},
	render() {
		return (
			<div>
				<ListGroupItem bsStyle={this.props.style}>
					{this.props.header}
				</ListGroupItem>
				<ListGroupItem>
					{this.props.children}
				</ListGroupItem>
			</div>
		);
	}
});
