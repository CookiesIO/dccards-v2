import React, {PropTypes} from "react/addons"
import Reflux from "reflux"
import {ButtonGroup, Button} from "react-bootstrap"

import {ToggleButton} from "ui/components"
import {Types} from "dccards"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		types: PropTypes.arrayOf(PropTypes.oneOf(Types.getAll())).isRequired,
		type: PropTypes.oneOf(Types.getAll()).isRequired,
		onTypeChange: PropTypes.func.isRequired
	},

	renderMobileButton(type) {
		return (
			<ButtonGroup bsSize="small">
				{this.renderButton(type)}
			</ButtonGroup>
		);
	},

	renderButton(type) {
		return (
			<ToggleButton
				data={type}
				selected={this.props.type === type} 
				disabled={this.props.types.indexOf(type) === -1}
				onClick={this.props.onTypeChange}
			>
				{type.getName()}
			</ToggleButton>
		);
	},

	render() {
		return (
			<ButtonGroup>
				<div className="visible-xs-block">
					<ButtonGroup justified>
						{/* Average */}
						{this.renderMobileButton(Types.average)}

						{/* Fresh */}
						{this.renderMobileButton(Types.fresh)}

						{/* Mental */}
						{this.renderMobileButton(Types.mental)}

						{/* Strong */}
						{this.renderMobileButton(Types.strong)}
					</ButtonGroup>
					<div /> {/* Keep the div, it fixes incorrect styling */}
					<ButtonGroup justified>
						{/* Tough */}
						{this.renderMobileButton(Types.tough)}

						{/* Quick */}
						{this.renderMobileButton(Types.quick)}

						{/* Smart */}
						{this.renderMobileButton(Types.smart)}

						{/* Perfect */}
						{this.renderMobileButton(Types.perfect)}
					</ButtonGroup>
				</div>
				<div className="hidden-xs">
					<ButtonGroup bsSize="xsmall">
						{/* Average */}
						{this.renderButton(Types.average)}

						{/* Fresh */}
						{this.renderButton(Types.fresh)}

						{/* Mental */}
						{this.renderButton(Types.mental)}

						{/* Strong */}
						{this.renderButton(Types.strong)}

						{/* Tough */}
						{this.renderButton(Types.tough)}

						{/* Quick */}
						{this.renderButton(Types.quick)}

						{/* Smart */}
						{this.renderButton(Types.smart)}

						{/* Perfect */}
						{this.renderButton(Types.perfect)}
					</ButtonGroup>
				</div>
			</ButtonGroup>
		);
	}
});