import React, {PropTypes} from "react/addons"
import Reflux from "reflux"
import {Table, Col} from "react-bootstrap"

import {Rarities, Types, Card} from "dccards"

var StatRow = React.createClass({
	mixins: [React.addons.PureRenderMixin],
	render() {
		var title = this.props.title;
		var health = this.props.health;
		var psyche = this.props.psyche;
		var attack = this.props.attack;
		var defense = this.props.defense;
		var speed = this.props.speed;
		var intelligence = this.props.intelligence;

		var total = health + psyche + attack + defense + speed + intelligence;

		return (
			<tr>
				<td>{title}</td>
				<td className="text-center">{health}</td>
				<td className="text-center">{psyche}</td>
				<td className="text-center">{attack}</td>
				<td className="text-center">{defense}</td>
				<td className="text-center">{speed}</td>
				<td className="text-center">{intelligence}</td>
				<td className="text-center">{total}</td>
			</tr>
		);
	}
});

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		card: PropTypes.instanceOf(Card).isRequired,
		type: PropTypes.oneOf(Types.getAll()),
		redeathed: PropTypes.bool
	},
	getDefaultProps() {
		return {
			type: Types.average,
			redeathed: false
		};
	},
	render() {
		var card = this.props.card;
		var type = this.props.type;
		var redeathed = this.props.redeathed;
		var level = card.getMaxLevel(redeathed);

		var level1Stats = card.getStats(type, redeathed, 1, false);
		var levelMaxStats = card.getStats(type, redeathed, level, false);
		var levelMaxStatsBoosted = card.getStats(type, redeathed, level, true);
		var boostPoints = card.getBoosts(redeathed);

		return (
			<Table striped condensed hover responsive>
				<thead>
					<tr>
						<Col sm={2} className="text-center" componentClass="th"></Col>
						<Col sm={1} className="text-center" componentClass="th">Health</Col>
						<Col sm={1} className="text-center" componentClass="th">Psyche</Col>
						<Col sm={1} className="text-center" componentClass="th">Attack</Col>
						<Col sm={1} className="text-center" componentClass="th">Defense</Col>
						<Col sm={1} className="text-center" componentClass="th">Speed</Col>
						<Col sm={1} className="text-center" componentClass="th">Intelligence</Col>
						<Col sm={1} className="text-center" componentClass="th">Total</Col>
					</tr>
				</thead>
				<tbody>
					<StatRow title="Level 1" {...level1Stats} />
					<StatRow title="Level Max" {...levelMaxStats} />
					<StatRow title="Level Max Boosted" {...levelMaxStatsBoosted} />
					<StatRow title="Max Boost Points" {...boostPoints} />
				</tbody>
			</Table>
		);
	}
});