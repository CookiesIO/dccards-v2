import React, {PropTypes} from "react/addons"
import {Input} from "react-bootstrap"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		onOptionChange: PropTypes.func.isRequired,
		option: PropTypes.oneOfType([
			PropTypes.shape({
				type: PropTypes.oneOf(["Boolean"]).isRequired,
				name: PropTypes.string.isRequired,
				display: PropTypes.string.isRequired,
				value: PropTypes.bool.isRequired
			}),
			PropTypes.shape({
				type: PropTypes.oneOf(["Select"]).isRequired,
				name: PropTypes.string.isRequired,
				display: PropTypes.string.isRequired,
				value: PropTypes.string.isRequired,
				values: PropTypes.arrayOf(
					PropTypes.shape({
						display: PropTypes.string.isRequired,
						value: PropTypes.string.isRequired,
					})
				).isRequired
			})
		]).isRequired
	},

	handleCheckboxChange() {
		this.props.onOptionChange(this.props.option.name, this.refs.input.getChecked());
	},

	handleSelectChange() {
		this.props.onOptionChange(this.props.option.name, this.refs.input.getValue());
	},

	render() {
		var option = this.props.option;
		switch (option.type) {
			case "Boolean": return this.renderCheckbox();
			case "Select": return this.renderSelect();
		}
	},

	renderCheckbox() {
		var {
			display,
			value
		} = this.props.option;

		return (
			<Input
				type="checkbox"
				label={display}
				checked={value}
				ref="input"
				onChange={this.handleCheckboxChange}
			/>
		);
	},

	renderSelect() {
		var {
			display,
			value,
			values
		} = this.props.option;

		return (
			<Input
				type="select"
				label={display}
				value={value}
				onChange={this.handleSelectChange}
				ref="input"
			>
				{values.map((value) => {
					return (
						<option
							key={value.value}
							value={value.value}
						>
							{value.display}
						</option>
					);
				})}
			</Input>
		);
	}
});
