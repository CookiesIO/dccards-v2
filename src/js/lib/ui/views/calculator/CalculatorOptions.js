import React, {PropTypes} from "react/addons"

import {Panel, ListGroup} from "react-bootstrap"

import OptionSection from "./OptionSection"
import CalculatorSelect from "./CalculatorSelect"
import CardOptionsSection from "./CardOptionsSection"
import CalculatorOptionsSection from "./CalculatorOptionsSection"
import RarityFilterInput from "./RarityFilterInput"

import {Card, Types} from "dccards"
import Ability from "simulation/abilities/Ability"
import Calculator from "calculators/Calculator"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		card: PropTypes.instanceOf(Card).isRequired,
		type: PropTypes.oneOf(Types.getAll()).isRequired,
		level: PropTypes.number.isRequired,
		build: PropTypes.arrayOf(PropTypes.instanceOf(Ability)).isRequired,
		boosts: PropTypes.shape({
			health: PropTypes.number,
			psyche: PropTypes.number,
			attack: PropTypes.number,
			defense: PropTypes.number,
			speed: PropTypes.number,
			intelligence: PropTypes.number,
		}).isRequired,
		redeathed: PropTypes.bool.isRequired,
		options: PropTypes.array.isRequired,
		calculator: PropTypes.instanceOf(Calculator),
		calculators: PropTypes.arrayOf(PropTypes.instanceOf(Calculator)),

		onCalculatorChange: PropTypes.func.isRequired,
		onLevelChange: PropTypes.func.isRequired,
		onBuildChange: PropTypes.func.isRequired,
		onBoostsChange: PropTypes.func.isRequired,
		onOptionChange: PropTypes.func.isRequired,
	},

	render() {
		var {
			card,
			type,
			level,
			build,
			boosts,
			redeathed,

			options,
			calculator,
			calculators,
			rarityFilter,

			onLevelChange,
			onBuildChange,
			onBoostsChange,
			onOptionChange,
			onCalculatorChange,
			onFilterChange
		} = this.props;

		return (
			<form>
				<Panel
					header="Options"
				>
					<CalculatorSelect
						onCalculatorChange={onCalculatorChange}
						calculators={calculators}
						calculator={calculator}
					/>
					<p>Rarity filters</p>
					<RarityFilterInput
						filter={rarityFilter}
						onFilterChange={onFilterChange}
					/>
					<ListGroup fill>
						<CardOptionsSection
							onLevelChange={onLevelChange}
							onBuildChange={onBuildChange}
							onBoostsChange={onBoostsChange}
							card={card}
							type={type}
							level={level}
							build={build}
							boosts={boosts}
							redeathed={redeathed}
						 />
						 <CalculatorOptionsSection
						 	onOptionChange={onOptionChange}
						 	options={options}
						 />
					</ListGroup>
				 </Panel>
			</form>
		);
	}
});
