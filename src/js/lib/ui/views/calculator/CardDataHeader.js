import React, {PropTypes} from "react/addons"
import Reflux from "reflux"
import {Link} from "react-router"
import {Label, Glyphicon} from "react-bootstrap"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		cardName: PropTypes.string.isRequired
	},
	render() {
		return (
			<div>
				<Label>{this.props.cardName}</Label>{' '}
				<a href={`http://deadmans-cross.wikia.com/wiki/${encodeURIComponent(this.props.cardName)}`} rel="nofollow" target="_blank">
					<Label bsStyle="primary">Wiki <Glyphicon glyph="link" /></Label>
				</a>
			</div>
		);
	}
});
