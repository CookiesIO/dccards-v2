import React, {PropTypes} from "react/addons"

import {Spinner} from "ui/components"
import CalculatorResultsBody from "./CalculatorResultsBody"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		results: PropTypes.array,
		calculating: PropTypes.bool.isRequired
	},

	render() {
		var { results, calculating } = this.props;

		var message = null;
		if (calculating) {
			message = (
				<div key="calculatingMessage" className="text-center" style={{height: "100%"}}>
					<h2><Spinner /> Calculating Calculations...</h2>
					<p>*magic*</p>
				</div>
			);
		}

		return (
			<div>
				{message}
				<div key="calculatorResults" className={calculating ? "hidden" : "show"}>
					<CalculatorResultsBody
						results={results}
					/>
				</div>
			</div>
		);
	}
});