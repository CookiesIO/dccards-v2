import React, {PropTypes} from "react/addons"
import {Row, Col} from "react-bootstrap"

import {Card, Types} from "dccards"
import StatBoostInput from "./StatBoostInput"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		onBoostsChange: PropTypes.func.isRequired, 
		card: PropTypes.instanceOf(Card).isRequired,
		type: PropTypes.oneOf(Types.getAll()).isRequired,
		level: PropTypes.number.isRequired,
		boosts: PropTypes.shape({
			health: PropTypes.number,
			psyche: PropTypes.number,
			attack: PropTypes.number,
			defense: PropTypes.number,
			speed: PropTypes.number,
			intelligence: PropTypes.number,
		}).isRequired,
		redeathed: PropTypes.bool.isRequired
	},

	handleHealthChange(health) {
		this.updateBoosts({health});
	},

	handlePsycheChange(psyche) {
		this.updateBoosts({psyche});
	},

	handleAttackChange(attack) {
		this.updateBoosts({attack});
	},

	handleDefenseChange(defense) {
		this.updateBoosts({defense});
	},

	handleSpeedChange(speed) {
		this.updateBoosts({speed});
	},

	handleIntelligenceChange(intelligence) {
		this.updateBoosts({intelligence});
	},

	updateBoosts(
		{
			health:health=this.props.boosts.health,
			psyche:psyche=this.props.boosts.psyche,
			attack:attack=this.props.boosts.attack,
			defense:defense=this.props.boosts.defense,
			speed:speed=this.props.boosts.speed,
			intelligence:intelligence=this.props.boosts.intelligence,
		}) {
		this.props.onBoostsChange({
			health,
			psyche,
			attack,
			defense,
			speed,
			intelligence
		});
	},

	render() {
		var {
			card,
			type,
			level,
			boosts: {
				health,
				psyche,
				attack,
				defense,
				speed,
				intelligence
			},
			redeathed,
		} = this.props;

		var stats = card.getStats(type, redeathed, level, false);
		var maxBoosts = card.getBoosts(redeathed);

		return (
			<div>
				<Row>
					<Col xs={6}>
						<StatBoostInput
							stat="Health"
							boost={health}
							maxBoost={maxBoosts.health}
							baseStat={stats.health}
							onChange={this.handleHealthChange}
						/>
					</Col>
					<Col xs={6}>
						<StatBoostInput
							stat="Psyche"
							boost={psyche}
							maxBoost={maxBoosts.psyche}
							baseStat={stats.psyche}
							onChange={this.handlePsycheChange}
						/>
					</Col>
				</Row>
				<Row>
					<Col xs={6}>
						<StatBoostInput
							stat="Attack"
							boost={attack}
							maxBoost={maxBoosts.attack}
							baseStat={stats.attack}
							onChange={this.handleAttackChange}
						/>
					</Col>
					<Col xs={6}>
						<StatBoostInput
							stat="Defense"
							boost={defense}
							maxBoost={maxBoosts.defense}
							baseStat={stats.defense}
							onChange={this.handleDefenseChange}
						/>
					</Col>
				</Row>
				<Row>
					<Col xs={6}>
						<StatBoostInput
							stat="Speed"
							boost={speed}
							maxBoost={maxBoosts.speed}
							baseStat={stats.speed}
							onChange={this.handleSpeedChange}
						/>
					</Col>
					<Col xs={6}>
						<StatBoostInput
							stat="Intelligence"
							boost={intelligence}
							maxBoost={maxBoosts.intelligence}
							baseStat={stats.intelligence}
							onChange={this.handleIntelligenceChange}
						/>
					</Col>
				</Row>
			</div>
		);
	}
});
