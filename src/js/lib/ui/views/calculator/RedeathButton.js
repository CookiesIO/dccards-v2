import React, {PropTypes} from "react/addons"
import {ButtonGroup} from "react-bootstrap"

import {ToggleButton} from "ui/components"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		selected: PropTypes.bool.isRequired,
		disabled: PropTypes.bool.isRequired,
		onClick: PropTypes.func.isRequired
	},
	render() {
		return (
			<ButtonGroup>
				<div className="visible-xs-block">
					<ButtonGroup justified>
						<ButtonGroup bsSize="small">
							<ToggleButton
								{...this.props}
							>
								Redeath
							</ToggleButton>
						</ButtonGroup>
					</ButtonGroup>
				</div>
				<div className="hidden-xs">
					<ButtonGroup bsSize="xsmall">
						<ToggleButton
							{...this.props}
						>
							Redeath
						</ToggleButton>
					</ButtonGroup>
				</div>
			</ButtonGroup>
		);
	}
});