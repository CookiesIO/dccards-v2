import React, {PropTypes} from "react/addons"

import OptionSection from "./OptionSection"
import DynamicOption from "./DynamicOption"

export default React.createClass({
	mixins: [React.addons.PureRenderMixin],
	propTypes: {
		onOptionChange: PropTypes.func.isRequired,
		group: PropTypes.object.isRequired
	},

	handleOptionChange(option, value) {
		var group = this.props.group;
		this.props.onOptionChange(group.group, option, value);
	},

	render() {
		var group = this.props.group;
		var options = group.options.map((option) => {
			return (
				<DynamicOption
					key={option.name}
					option={option}
					onOptionChange={this.handleOptionChange}
				/>
			);
		});
		return (
			<OptionSection
				header={group.group}
				style="info"
			>
				{options}
			</OptionSection>
		)
	},
});
