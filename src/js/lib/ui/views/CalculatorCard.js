import React, {PropTypes} from "react/addons"
import Reflux from "reflux"
import {Row, Col, Alert} from "react-bootstrap"
import DocumentTitle from "react-document-title"

import CalculatorActions from "actions/ui/CalculatorActions"
import CalculatorStateStore from "stores/ui/CalculatorStateStore"
import CalculatorCardsStore from "stores/ui/CalculatorCardsStore"
import CalculatorStore from "stores/calculator/CalculatorStore"

import CardData from "./calculator/CardData"
import CalculatorOptions from "./calculator/CalculatorOptions"
import CalculatorResults from "./calculator/CalculatorResults"
import {CardImage, Spinner} from "ui/components"

import CalculatorHome from "./CalculatorHome"

export default React.createClass({
	mixins: [Reflux.connect(CalculatorStateStore, "calculatorState")
			,Reflux.connect(CalculatorStore, "calculators")
			,React.addons.PureRenderMixin],

	statics: {
		willTransitionTo(transition, params, query, callback) {
			var setCard = () => {
				CalculatorActions.setCard(params.cardName);
				setTimeout(() => {
					callback();
				}, 0);
			}

			if (CalculatorCardsStore.state.cards !== null) {
				setCard();
			}
			else {
				var unlisten = CalculatorCardsStore.listen(() => {
					setCard();
					unlisten();
				});
			}
		}
	},

	contextTypes: {
		router: PropTypes.func
	},

	getCardName() {
		var {router} = this.context;
		return router.getCurrentParams().cardName;
	},

	handleTypeChange(type) {
		CalculatorActions.setType(type);
	},

	handleRedeathChange(redeathed) {
		CalculatorActions.setRedeathed(redeathed);
	},

	handleLevelChange(level) {
		CalculatorActions.setLevel(level);
	},

	handleBuildChange(build) {
		CalculatorActions.setBuild(build);
	},

	handleBoostsChange(boosts) {
		CalculatorActions.setBoosts(boosts);
	},

	handleCalculatorChange(calculator) {
		CalculatorActions.setCalculator(calculator);
	},

	handleOptionChange(section, option, value) {
		CalculatorActions.setOption(section, option, value);
	},

	handleFilterChange(filter) {
		CalculatorActions.setRarityFilter(filter);
	},

	render() {
		var {
			card,
			type,
			level,
			build,
			boosts,
			redeathed,
			rarityFilter,
			calculator,
			options,
			calculator,
			calculating,
			results
		} = this.state.calculatorState;

		var calculators = this.state.calculators;

		if (card === null) {
			return (
				<div>
					<Row>
						<Col xs={12} lg={6} lgPush={3}>
							<Alert bsStyle="warning">
								<strong>{this.getCardName()}</strong> does not exist.
							</Alert>
						</Col>
					</Row>
					<CalculatorHome />
				</div>
			);
		}

		var leftCol = {xs:12, sm:3, md:2};
		var rightCol = {xs:12, sm:9, md:10};
		return (
			<DocumentTitle title={`Deadmans Cross Cards - Calculator - ${this.getCardName()}`}>
				<div>
					<Row>
						<Col {...leftCol}>
							<CardImage className="img-responsive center-block" card={card} imageType="card" />
						</Col>
						<Col {...rightCol}>
							<CardData
								card={card}
								type={type}
								redeathed={redeathed}
								onTypeChange={this.handleTypeChange}
								onRedeathChange={this.handleRedeathChange}
							/>
						</Col>
					</Row>
					<Row>
						<Col {...leftCol}>
							<CalculatorOptions
								card={card}
								type={type}
								level={level}
								build={build}
								boosts={boosts}
								redeathed={redeathed}

								options={options}
								calculator={calculator}
								calculators={calculators}
								rarityFilter={rarityFilter}

								onCalculatorChange={this.handleCalculatorChange}
								onLevelChange={this.handleLevelChange}
								onBuildChange={this.handleBuildChange}
								onBoostsChange={this.handleBoostsChange} 
								onOptionChange={this.handleOptionChange} 
								onFilterChange={this.handleFilterChange}
							/> 
						</Col>
						<Col {...rightCol}>
							<CalculatorResults
								results={results}
								calculating={calculating}
							/>
						</Col>
					</Row>
				</div>
			</DocumentTitle>
		);
	}
});