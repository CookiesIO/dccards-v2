import Reflux from "reflux"

export default Reflux.createActions([
	"setRarityFilter",
	"setCalculator",
	"setOption",

	"setCard",
	"setType",
	"setLevel",
	"setRedeathed",
	"setBoosts",
	"setBuild"
]);

