import Reflux from "reflux"
import Promise from "bluebird"

import Card from "dccards/Card"
import Ability from "dccards/Ability"

import CardCollection from "collections/CardCollection"
import AbilityManager from "simulation/AbilityManager"

import DCCardsAPI from "dccards-api"

var loading = false;
var load = Reflux.createAction({
	children: ["abilitiesCompleted", "cardsCompleted", "cacheCompleted", "completed", "failure"],
	shouldEmit() {
		return !loading;
	}
});

load.listen(function() {
	loading = true;
	Promise.join(DCCardsAPI.loadAbilities(), DCCardsAPI.loadCards(),
	(abilities, cards) => {
		abilities = parseAbilities(abilities);
		abilities = AbilityManager.create(abilities);

		cards = parseCards(cards, abilities);
		cards = new CardCollection(cards);

		this.abilitiesCompleted(abilities);
		this.cardsCompleted(cards);

		loading = false;
		this.completed();
	})
	.caught(err => {
		loading = false;
		this.failure();
	});
});

export default load;

function parseAbilities(rawAbilities) {
	var abilities = [];
	for (var i = 0; i < rawAbilities.length; i++) {
		abilities.push(Ability.parse(rawAbilities[i]));
	}
	return abilities;
}

function parseCards(rawCards, abilityManager) {
	var cards = [];
	for (var i = 0; i < rawCards.length; i++) {
		cards.push(Card.parse(rawCards[i], abilityManager));
	}
	return cards;
}