import Reflux from "reflux"
import Actions from "actions/calculator"
import DataCollection from "collections/DataCollection"

export default Reflux.createStore({
	init() {
		this.listenToMany(Actions);
		this.calculators = [];
	},

	getInitialState() {
		return this.calculators;
	},

	addCalculator(calculator) {
		this.updateCalculators(this.calculators.concat([calculator]));
	},

	updateCalculators(calculators) {
		this.calculators = calculators;
		this.trigger(this.calculators);
	}
});