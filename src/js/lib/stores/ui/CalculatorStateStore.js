import Reflux from "reflux"

import CalculatorActions from "actions/ui/CalculatorActions"
import CalculatorCardsStore from "stores/ui/CalculatorCardsStore"
import CalculatorStore from "stores/calculator/CalculatorStore"
import {Types, Rarities} from "dccards"

export default Reflux.createStore({
	init() {
		this.filteredCards = null;
		this.state = {
			card: null,
			type: null,
			level: null,
			build: null,
			boosts: null,
			redeathed: false,
			rarityFilter: {
				pestilents: true,
				virulents: true,
				legendaries: true,
				epicPlus: true,
				epics: true,
				rares: false,
				uncommons: false,
				commons: false
			},
			calculator: null,
			options: null,
			calculating: false,
			results: []
		};

		this.listenToMany(CalculatorActions);
		this.listenTo(CalculatorCardsStore, this.cardsUpdated, this.cardsUpdated);
		this.listenTo(CalculatorStore, this.calculatorsUpdated, this.calculatorsUpdated);
	},

	getInitialState() {
		return this.state;
	},

	cardsUpdated(state) {
		if (this.state.card !== null) {
			CalculatorActions.setCard(this.state.card.getName());
		}
		this.updateCalculatorCards(this.state.rarityFilter);
	},

	calculatorsUpdated(calculators) {
		if (calculators.length > 0) {
			CalculatorActions.setCalculator(calculators[0]);
		}
	},

	setCalculator(calculator) {
		this.updateState({
			calculator
		});
		CalculatorActions.setOption("Opponent Card", "redeathed", this.state.redeathed);
	},

	setOption(section, option, value) {
		var { calculator } = this.state;
		if (calculator !== null) {
			calculator.setOption(section, option, value);
			this.updateState({
				options: null
			});
		}
	},

	setRarityFilter(rarityFilter) {
		this.updateCalculatorCards(rarityFilter);
		this.updateState({
			rarityFilter
		});
	},

	setCard(cardName) {
		var cards = CalculatorCardsStore.state.cards;
		if (cards !== null) {
			this.updateCard(cards.findByName(cardName));
		}
	},

	setType(type) {
		var { card, level, redeathed } = this.state;
		if (card !== null) {
			this.updateState({
				type,
				build: this.state.card.getBuild(type, redeathed, level)
			});
		}
	},

	setLevel(level) {
		var { card, type, redeathed } = this.state;
		if (card !== null) {
			this.updateState({
				level,
				build: this.state.card.getBuild(type, redeathed, level)
			});
		}
	},

	setBuild(build) {
		if (this.state.card !== null) {
			this.updateState({
				build
			});
		}
	},

	setBoosts(boosts) {
		if (this.state.card !== null) {
			this.updateState({
				boosts
			});
		}
	},

	setRedeathed(redeathed) {
		var { card, type } = this.state;
		if (card !== null && this.state.redeathed != (redeathed && card.getRedeathable())) {
			this.updateState({
				level: card.getMaxLevel(redeathed),
				build: card.getBuild(type, redeathed),
				boosts: card.getBoosts(redeathed),
				redeathed: redeathed && card.getRedeathable()
			});
			CalculatorActions.setOption("Opponent Card", "redeathed", this.state.redeathed);
		}
	},

	updateCard(card) {
		var type = null;
		var level = null;
		var build = null;
		var boosts = null;

		if (card !== null) {
			type = card.getTypes()[0];
			level = card.getMaxLevel();
			build = card.getBuild(type, false);
			boosts = card.getBoosts(false);
		}

		this.updateState({
			card,
			type,
			level,
			build,
			boosts,
			redeathed: false
		});
	},

	updateCalculatorCards(rarityFilter) {
		var state = CalculatorCardsStore.state;
		if (state.cards !== null) {
			this.filteredCards = [];

			if (rarityFilter.pestilents) {
				this.filteredCards = this.filteredCards.concat(state.pestilents.toArray());
			}
			if (rarityFilter.virulents) {
				this.filteredCards = this.filteredCards.concat(state.virulents.toArray());
			}
			if (rarityFilter.legendaries) {
				this.filteredCards = this.filteredCards.concat(state.legendaries.toArray());
			}
			if (rarityFilter.epicPlus) {
				this.filteredCards = this.filteredCards.concat(state.epicPlus.toArray());
			}
			if (rarityFilter.epics) {
				this.filteredCards = this.filteredCards.concat(state.epics.toArray());
			}
			if (rarityFilter.rares) {
				this.filteredCards = this.filteredCards.concat(state.rares.toArray());
			}
			if (rarityFilter.uncommons) {
				this.filteredCards = this.filteredCards.concat(state.uncommons.toArray());
			}
			if (rarityFilter.commons) {
				this.filteredCards = this.filteredCards.concat(state.commons.toArray());
			}

			this.updateState({});
		}
	},

	updateState({
		card: card=this.state.card,
		type: type=this.state.type,
		level: level=this.state.level,
		build: build=this.state.build,
		boosts: boosts=this.state.boosts,
		redeathed: redeathed=this.state.redeathed,
		rarityFilter: rarityFilter=this.state.rarityFilter,
		calculator: calculator=this.state.calculator,
		options: options=this.state.options,
		results: results=this.state.results,
		calculating: calculating=this.state.calculating,
		finishedCalculating: finishedCalculating=false
	}) {
		if (!finishedCalculating && card !== null && calculator !== null) {
			if (card !== this.state.card
				|| type !== this.state.type
				|| level !== this.state.level
				|| build !== this.state.build
				|| boosts !== this.state.boosts
				|| redeathed !== this.state.redeathed
				|| calculator !== this.state.calculator
				) {
				calculator.setCard(card, type, redeathed, level, boosts, build);
			}
			if (options === null
				|| card !== this.state.card
				|| build !== this.state.build
				|| calculator !== this.state.calculator) {
				options = calculator.getOptions();
			}

			if (this.filteredCards !== null) {
				calculating = true;
				calculator.calculate(this.filteredCards).then((results) => {
					this.updateState({
						results,
						calculating: false,
						finishedCalculating: true
					});
				})
				.caught(() => {
					
				});
			}
		}

		this.state = {
			card,
			type,
			level,
			build,
			boosts,
			redeathed,
			rarityFilter,
			calculator,
			options,
			calculating,
			results
		};
		this.trigger(this.state);
	}
});