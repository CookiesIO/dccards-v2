import Reflux from "reflux"

import CardCollection from "collections/CardCollection"
import {Rarities} from "dccards"
import CardStore from "stores/dccards/CardStore"

export default Reflux.createStore({
	init() {
		this.state = {
			cards: null,
			pestilents: null,
			virulents: null,
			legendaries: null,
			epicPlus: null,
			epics: null,
			rares: null,
			uncommons: null,
			commons: null
		};
		this.listenTo(CardStore, this.cardsUpdated, this.cardsUpdated);
	},

	getInitialState() {
		return this.state;
	},

	cardsUpdated(cards) {
		if (cards !== null) {
			var cards = cards.getCards((card) => {
				return card.isSimulatable();
			}).toArray();

			cards.sort((cardA, cardB) => {
				return cardA.compareTo(cardB);
			});

			cards = new CardCollection(cards);

			this.state = {
				cards,
				pestilents: cards.getCards(Rarities.pestilent),
				virulents: cards.getCards(Rarities.virulent),
				legendaries: cards.getCards(Rarities.legendary),
				epicPlus: cards.getCards(Rarities.epicPlus),
				epics: cards.getCards(Rarities.epic),
				rares: cards.getCards(Rarities.rare),
				uncommons: cards.getCards(Rarities.uncommon),
				commons: cards.getCards(Rarities.common)
			};

			this.trigger(this.state);
		}
	},
});