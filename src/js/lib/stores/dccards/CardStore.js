import Reflux from "reflux"
import Actions from "actions/dccards"

export default Reflux.createStore({
	init() {
		this.listenToMany(Actions);
		this.cards = null;
	},

	getInitialState() {
		return this.cards;
	},

	loadCardsCompleted(cards) {
		this.updateCards(cards);
	},

	updateCards(cards) {
		this.cards = cards;
		this.trigger(this.cards);
	}
});