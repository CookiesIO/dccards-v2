import Reflux from "reflux"
import Actions from "actions/dccards"

export default Reflux.createStore({
	init() {
		this.listenToMany(Actions);
		this.abilities = null;
	},

	getInitialState() {
		return this.abilities;
	},

	loadAbilitiesCompleted(abilities) {
		this.updateAbilities(abilities);
	},

	updateAbilities(abilities) {
		this.abilities = abilities;
		this.trigger(this.abilities);
	}
});