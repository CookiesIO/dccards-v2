import Reflux from "reflux"
import Actions from "actions/dccards"

export default Reflux.createStore({
	init() {
		this.listenToMany(Actions);
		this.state = {
			loading: false,
			loaded: false,
			loadingFailed: false
		}
	},

	getInitialState() {
		return this.state;
	},

	load() {
		this.updateState({
			loading: true,
			loaded: this.state.loaded,
			loadingFailed: false
		});
	},

	loadCompleted() {
		this.updateState({
			loading: false,
			loaded: true,
			loadingFailed: false
		});
	},

	loadCacheCompleted() {
		this.updateState({
			loading: true,
			loaded: true,
			loadingFailed: false
		});
	},

	loadFailure() {
		this.updateState({
			loading: false,
			loaded: this.state.loaded,
			loadingFailed: true
		});
	},

	updateState(state) {
		this.state = state;
		this.trigger(this.state);
	}
});